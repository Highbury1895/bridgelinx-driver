import 'package:animated_splash/animated_splash.dart';
import 'package:bridgelinx_driver/root.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:easy_localization/easy_localization.dart';
// import 'package:camera/camera.dart';
import 'package:flutter_better_camera/camera.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_foreground_plugin/flutter_foreground_plugin.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dashboard/flow/camera_page.dart';
import 'utils/fcm_subscription.dart';
import 'utils/globals.dart';
import 'package:flutter/foundation.dart' show kDebugMode;

// Toggle this to cause an async error to be thrown during initialization
// and to test that runZonedGuarded() catches the error
final _kShouldTestAsyncErrorOnInit = false;

// Toggle this for testing Crashlytics in your app locally.
final _kTestingCrashlytics = true;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();

  // Fetch the available cameras before initializing the app.
  try {
    WidgetsFlutterBinding.ensureInitialized();
    cameras = await availableCameras();
  } on CameraException catch (e) {
    logError(e.code, e.description);
  }

  prefs = await SharedPreferences.getInstance();

  runApp(EasyLocalization(
      supportedLocales: [Locale('en'), Locale('ur')],
      path: 'assets/translations',
      fallbackLocale: Locale('ur'),
      saveLocale: true,
      // startLocale: Locale('ur'),
      useOnlyLangCode: true,
      child: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  // Future<void> _initializeFlutterFireFuture;

  Future<void> _testAsyncErrorOnInit() async {
    Future<void>.delayed(const Duration(seconds: 2), () {
      final List<int> list = <int>[];
      print(list[100]);
    });
  }

// Define an async function to initialize FlutterFire
  Future<void> _initializeFlutterFire() async {
    // Wait for Firebase to initialize
    await Firebase.initializeApp();

    if (_kTestingCrashlytics) {
      // Force disable Crashlytics collection while doing every day development.
      // Temporarily toggle this to true if you want to test crash reporting in your app.
      await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
    } else {
      // Else only enable it in non-debug builds.
      // You could additionally extend this to allow users to opt-in.
      await FirebaseCrashlytics.instance
          .setCrashlyticsCollectionEnabled(!kDebugMode);
    }

    // Pass all uncaught errors to Crashlytics.
    Function originalOnError = FlutterError.onError;
    FlutterError.onError = (FlutterErrorDetails errorDetails) async {
      await FirebaseCrashlytics.instance.recordFlutterError(errorDetails);
      // Forward to original handler.
      originalOnError(errorDetails);
    };

    if (_kShouldTestAsyncErrorOnInit) {
      await _testAsyncErrorOnInit();
    }
    FirebaseCrashlytics.instance.setUserIdentifier("0");

    // Initialize Notifications
    initNotifs();
  }

  @override
  didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        // FlutterForegroundPlugin?.stopForegroundService();
        // if (_showMap) _setListener();
        // print("app in resumed");
        break;
      case AppLifecycleState.inactive:
        // print("app in inactive");
        break;
      case AppLifecycleState.paused:
        // _getPositionSubscription?.cancel();
        // if (_showMap) startForegroundService();
        // print("app in paused");
        break;
      case AppLifecycleState.detached:
        await FlutterForegroundPlugin?.stopForegroundService();
        // await ForegroundService?.stopForegroundService();
        print("app in detached");
        break;
    }
  }

  @override
  void initState() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      appVersion = packageInfo.version;
      appVersionCode = int.tryParse(packageInfo.buildNumber);
      
      switch (packageInfo.packageName) {
        case "com.weoveri.bridgelinx_driver":
          appFlavor = FlavorType.Prod;
          break;
        default:
          appFlavor = FlavorType.Dev;
          break;
      }
      // print('App Flavor: $appFlavor');
    });

    WidgetsBinding.instance.addObserver(this);

    // _initializeFlutterFireFuture = _initializeFlutterFire();
    _initializeFlutterFire();

    initializeNotifications();

    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      // EasyLocalization Start
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: Locale(getLocale()),
      // EasyLocalization End
      title: 'BridgeLinx Driver',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Color(0xff1C75BC),
          accentColor: Color(0xff00C2CB),
          primaryColorDark: Color(0xFF009298),
          primarySwatch: Colors.blue,
          canvasColor: Colors.white,
          backgroundColor: Colors.white,
          bottomAppBarColor: Colors.blue[400],
          scaffoldBackgroundColor: Colors.white,
          buttonColor: Color(0xff1C75BC),
          accentTextTheme: TextTheme(
            bodyText1: TextStyle(
                fontSize: 14,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.normal,
                fontFamily: 'Avenir',
                color: Colors.black54),
            bodyText2: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                // color: Color(0xff898989),
                color: Colors.black54,
                fontFamily: 'Avenir'),
            button: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Color(0xffffffff),
                fontFamily: 'Avenir'),
            // body heading
            headline1: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: Colors.black54,
                fontFamily: 'Avenir'),
            headline2: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
                // color: Colors.grey,
                color: Colors.black54,
                fontFamily: 'Avenir'),
            headline4: TextStyle(
                color: Colors.black54,
                fontSize: 34,
                fontWeight: FontWeight.w600,
                fontFamily: 'Avenir'),
            headline5: TextStyle(
                color: Colors.black54,
                fontSize: 20,
                fontWeight: FontWeight.w600,
                fontFamily: 'Avenir'),
          ),
          textTheme: TextTheme(
            headline4: TextStyle(),
            headline3: TextStyle(),
            headline2: TextStyle(),
            headline1: TextStyle(),
            bodyText2: TextStyle(),
            bodyText1: TextStyle(),
            headline5: TextStyle(),
            headline6: TextStyle(),
            subtitle1: TextStyle(),
            subtitle2: TextStyle(),
            caption: TextStyle(),
            overline: TextStyle(),
          ),
          primaryTextTheme: TextTheme(
            headline4: TextStyle(),
            headline3: TextStyle(),
            headline2: TextStyle(),
            headline1: TextStyle(),
            bodyText2: TextStyle(),
            bodyText1: TextStyle(),
            headline5: TextStyle(),
            headline6: TextStyle(),
            subtitle1: TextStyle(),
            subtitle2: TextStyle(),
            caption: TextStyle(),
            overline: TextStyle(),
          )),
      home: AnimatedSplash(
        imagePath: 'assets/logo.png',
        home: RootPage(),
        duration: 2500,
        type: AnimatedSplashType.StaticDuration,
      ),
    );
  }
}
