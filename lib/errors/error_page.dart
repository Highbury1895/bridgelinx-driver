import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:easy_localization/easy_localization.dart';

class ErrorPage extends StatefulWidget {
  final Job job;
  final String title;
  final String desc;
  final String imagePath;
  final Journey journey;

  const ErrorPage({Key key, this.job, this.title, this.desc, this.imagePath, this.journey})
      : super(key: key);
  @override
  _ErrorPageState createState() => _ErrorPageState();
}

class _ErrorPageState extends State<ErrorPage> {
  Job _job;
  String _title;
  String _desc;
  String _imagePath;
  Journey _journey;

  void initVars() {
    _job = widget.job;
    _title = widget.title;
    _desc = widget.desc;
    _imagePath = widget.imagePath;
    _journey = widget.journey;
  }

  @override
  void initState() {
    initVars();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'error'.tr(),
            style: Theme.of(context)
                .accentTextTheme
                .headline1
                .copyWith(color: Colors.white),
          ),
          centerTitle: true,
          automaticallyImplyLeading: true,
          actions: [
            IconButton(
              icon: Icon(FontAwesomeIcons.headset),
              onPressed: () => setState(() {
                makePhoneCall(helpline);
              }),
            ),
          ],
        ),
        body: Container(
            padding: EdgeInsets.all(30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(
                      _title,
                      style: Theme.of(context).accentTextTheme.headline4,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Image.asset(
                          _imagePath,
                          height: 180,
                          width: 180,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Text(
                      _desc,
                      style: Theme.of(context).accentTextTheme.headline5,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    _job == null
                        ? loginCubit.retryAfterError(_title)
                        : jobCubit.retryAfterError(_job, _title, _journey);
                  },
                  child: Center(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        'retry'.tr(),
                        style: Theme.of(context)
                            .accentTextTheme
                            .headline1
                            .copyWith(
                              color: Colors.white,
                            ),
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
