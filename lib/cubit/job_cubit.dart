import 'package:bloc/bloc.dart';
import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:bridgelinx_driver/DTO/stops.dart';
import 'package:bridgelinx_driver/Repos/job_repo.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meta/meta.dart';
import 'package:easy_localization/easy_localization.dart';
import '../utils/shared_preferences.dart';

part 'job_state.dart';

class JobCubit extends Cubit<JobState> {
  final JobRepo _jobRepo;
  JobCubit(this._jobRepo)
      : super(JobInitial(null, currentDriver.currentJob ?? currentJobId));

  // Update Coordinates

  updateJobCo(String jobId, Coordinates coordinates) {
    _jobRepo.updateJobCo(jobId ?? currentJob.jobId, coordinates);
  }

  updateDriverCo(String driverId, Coordinates coordinates) {
    _jobRepo.updateDriverCo(driverId ?? currentDriver.driverId, coordinates);
  }

  updateCoordinates(String jobId, String driverId, Coordinates coordinates) {
    _jobRepo.updateCoordinates(jobId ?? currentJob.jobId,
        driverId ?? currentDriver.driverId, coordinates);
  }

  // Times

  updateTimes(Job job) async {
    await _jobRepo.updateTimes(job);
  }

  updateDistancesAndTimes(Job job) async {
    await _jobRepo.updateDistancesAndTimes(job);
  }

  // Image

  Future<Job> uploadPic(
      Job job, Journey journey, String imagePath, String imageName) async {
    emit(JobLoadingPage());

    Job _job = await _jobRepo.uploadPic(job, journey, imagePath, imageName);
    return _job;
  }

  // Job

  Future<Price> getPrice(Job job) async {
    Price price = await _jobRepo.getPrice(job);
    return price;
  }

  getCurrentJob(String jobId) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    // print('${_connected.toString()} ${isCurrentJob()} ${isCurrentDriver()}');
    if (_connected == 0 || (_connected == 1 && !isCurrentJob())) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          null);
      return;
    }

    final Job result = jobId != null || isCurrentJob()
        ? _connected == 2
            ? await _jobRepo.getCurrentJob(jobId)
            : getCurrentJobSP()
        : null;

    // print(result?.toJson());
    currentJob = result;
    currentJobId = null;
    Future.delayed(Duration(microseconds: 100), () {
      emit(JobInitial(currentJob, currentJobId));
    });
  }

  // Notification

  sendFCM(Job job, String title, String notBody) {
    _jobRepo.sendFCM(job, title, notBody);
  }

  // Realtime Database

  jobStaticCritical(bool flag, Timestamp timestamp, String jobId,
      String driverId, String driverPhone) {
    _jobRepo.jobStaticCritical(flag, timestamp, jobId, driverId, driverPhone);
  }

  jobETA130(Job job, Journey journey, Driver driver, Timestamp timestamp) {
    _jobRepo.jobETA130Journey(job, journey, driver, timestamp);
  }

  // Job Flow

  continueJob(Job job, Journey journey) async {
    emit(JobLoadingPage());
    if (job == null) {
      emit(JobInitial(null, currentJobId));
      return;
    }

    if (journey == null && job.driverStatus.status != DriverStatus.driverInit) {
      // Get Driver Coordinates
      Position _position = await getGeoCurrentLocation(null);
      if (_position == null) return;
      Coordinates _coordinates =
          Coordinates(lat: _position.latitude, lng: _position.longitude);
      journey = await _jobRepo.getJourney(
          job, job.driverStatus.journey, _coordinates);
    }

    switch (job.driverStatus.status) {
      case DriverStatus.driverInit:
        emit(JobLoadingPage());
        Future.delayed(Duration(milliseconds: 100), () {
          emit(JobInitial(job, null));
        });
        break;
      case DriverStatus.driverAccepted:
        emit(JobExitBase(job, journey));
        break;
      case DriverStatus.driverMovingToPickup:
        emit(JobMovePickup(job, journey));
        break;
      case DriverStatus.driverArrived:
        emit(JobStartLoading(job, journey));
        break;
      case DriverStatus.driverLoading:
        emit(JobTakePicPickupCheckIn(job, journey));
        break;
      case DriverStatus.driverSetLoaders:
        emit(JobSetLoaders(job, journey));
        break;
      case DriverStatus.driverLoadingCompleted:
        emit(JobTakePicPickup(job, journey));
        break;
      case DriverStatus.driverExitPickup:
        emit(JobExitPickup(job, journey));
        break;
      case DriverStatus.driverLeft:
        emit(JobTakePicPickupGatePass(job, journey));
        break;
      case DriverStatus.driverMovingToDropOff:
        emit(JobMoveDrop(job, journey));
        break;
      case DriverStatus.driverReachedDropOff:
        emit(JobUnloading(job, journey));
        break;
      case DriverStatus.driverUnloading:
        emit(JobTakePicDropoffCheckIn(job, journey));
        break;
      case DriverStatus.driverSetUnloaders:
        emit(JobSetUnloaders(job, journey));
        break;
      case DriverStatus.driverUnloadingCompleted:
        emit(JobTakePicDropoff(job, journey));
        break;
      case DriverStatus.driverExitDropoff:
        emit(JobExitDropOff(job, journey));
        break;
      case DriverStatus.driverProofOfDelivery:
        emit(JobTakePicDropoffCertificate(job, journey));
        break;
      case DriverStatus.driverMovingToStop:
        emit(JobMoveStop(job, journey));
        break;
      case DriverStatus.driverReachedStop:
        emit(JobReachedStop(job, journey));
        break;
      case DriverStatus.driverExitStop:
        emit(JobExitStop(job, journey));
        break;
      case DriverStatus.driverCheckout:
        emit(JobComplete(job));
        break;
      default:
        emit(JobInitial(job, ''));
        break;
    }
  }

  applyForJob(Job job, BuildContext context) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0 || _connected == 1) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          null);
      return;
    }

    final jobId = await _jobRepo.applyForJob(job);
    if (jobId == null) {
      await saveCurrentJob(null);
      jobNotifTime = null;
      emit(JobInitial(currentJob, ''));
    } else {
      exitBase(job);
    }
  }

  nextJourney(Job job, Journey journey) async {
    emit(JobLoadingPage());

    if (job.jobType == JobType.interCity) {
      // Check Internet
      int _connected = await checkInternet();
      if (_connected == 0 || _connected == 1) {
        showError(
            currentJob,
            errors[1].tr(),
            'please_turn_on_internet_services'.tr(),
            'assets/no_internet.png',
            null);
        return;
      }

      Job _job = await _jobRepo.updateJourey(job);
      job.scheduledStops = _job.scheduledStops;
      journey = Journey.fromJson(job.journey[journey.id]);
    }

    String nextJourney = journey.nextRef;

    if (nextJourney == null) {
      jobCubit.completeJob(job, journey);
    } else {
      Journey _journey =
          await _jobRepo.getJourney(job, nextJourney, currentCoordinates);

      if (_journey.journeyType == JourneyType.Pickup) {
        jobCubit.moveToPickup(job, _journey);
      } else if (_journey.journeyType == JourneyType.Dropoff) {
        jobCubit.moveToDropoff(job, _journey);
      } else if (_journey.journeyType == JourneyType.Stop) {
        jobCubit.moveToStop(job, _journey);
      }
    }
  }

  // Start Job
  exitBase(Job job) async {
    if (currentDriver.driverType != DriverType.dynamic) emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0 || _connected == 1) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          null);
      return;
    }

    // Set Distances and Times
    job = await _jobRepo.getDistanceMatrixSep(job);

    // Get Driver Coordinates
    Position _position = await getGeoCurrentLocation(null);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    // Get Journey
    final journey =
        await _jobRepo.getJourney(job, job.startJourney, _coordinates);

    // Set Job & Driver Status
    job.driverStatus = DriverStatusTS(DriverStatus.driverAccepted,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);

    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney = {
            journey.id: List.from([job.driverStatus.toJson()])
          }
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    job.jobStatus =
        JobStatusTS(JobStatus.jobOngoing, Timestamp.fromDate(DateTime.now()));
    job.jobStatusHistory.add(job.jobStatus.toJson());

    _jobRepo.setDriverJobStatus(job.jobId, job.driverStatus, job.jobStatus,
        job.driverStatusHistoryJourney);

    // Save Last Job
    await saveLastJob(job);
    await saveCurrentJob(job);

    _jobRepo.leftBase();

    if (currentDriver.driverType != DriverType.dynamic)
      _jobRepo.jobStaticCritical(
          false,
          jobNotifTime != null ? Timestamp.fromDate(jobNotifTime) : null,
          currentJob.jobId,
          currentDriver.driverId,
          currentDriver.primaryPhoneNumber);

    emit(JobExitBase(job, journey));
  }

  // Pickup Start
  moveToPickup(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverMovingToPickup,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobMovePickup(job, journey));
    });
  }

  startLoading(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          journey);
      return;
    }

    // Send Notification
    sendFCM(
        job, 'Arrived at Pickup', 'Driver has arrived at the pickup location');

    // Add Distances
    job = await _jobRepo.setTotalDistance(job, journey.id);

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverArrived,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobStartLoading(job, journey));
    });
  }

  takePicPickupCheckIn(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverLoading,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobTakePicPickupCheckIn(job, journey));
    });
  }

  setLoaders(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverSetLoaders,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobSetLoaders(job, journey));
    });
  }

  completeLoading(Job job, Journey journey) {
    emit(JobLoadingPage());

    _jobRepo.updateJobLoaders(
        job.jobId, job.numberOfLoadersJourney, job.numberOfUnloadersJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobLoadingComplete(job, journey));
    });
  }

  takePicPickup(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverLoadingCompleted,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobTakePicPickup(job, journey));
    });
  }

  // Notify Cash Page

  exitPickup(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverExitPickup,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobExitPickup(job, journey));
    });
  }

  takePicPickupGatePass(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverLeft,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobTakePicPickupGatePass(job, journey));
    });
  }
  // Pickup End

  // Cash Collect Page

  // Dropoff Start
  moveToDropoff(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverMovingToDropOff,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () async {
      emit(JobMoveDrop(job, journey));
    });
  }

  startUnloading(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          journey);
      return;
    }

    // Send Notification
    sendFCM(
        job, 'Arrived Dropoff', 'Driver has arrived at the dropoff location');

    // Add Distances
    job = await _jobRepo.setTotalDistance(job, journey.id);

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverReachedDropOff,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobUnloading(job, journey));
    });
  }

  takePicDropoffCheckIn(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverUnloading,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobTakePicDropoffCheckIn(job, journey));
    });
  }

  setUnloaders(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverSetUnloaders,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobSetUnloaders(job, journey));
    });
  }

  completeUnloading(Job job, Journey journey) async {
    emit(JobLoadingPage());

    _jobRepo.updateJobLoaders(
        job.jobId, job.numberOfLoadersJourney, job.numberOfUnloadersJourney);
    await saveCurrentJob(job);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobUnloadingComplete(job, journey));
    });
  }

  takePicDropoff(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverUnloadingCompleted,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobTakePicDropoff(job, journey));
    });
  }

  // takePicDropoffGatePass(Job job) {
  //   emit(JobLoadingPage());
  //   Future.delayed(Duration(milliseconds: 100), () {
  //     emit(JobTakePicDropoffGatePass(job));
  //   });
  // }

  exitDropoff(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverExitDropoff,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobExitDropOff(job, journey));
    });
  }

  // Stop Start
  moveToStop(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverMovingToStop,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobMoveStop(job, journey));
    });
  }

  reachedStop(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Update Stop Time
    job.scheduledStops[journey.toStop.id]['reachTime']['actual'] =
        Timestamp.now().millisecondsSinceEpoch;
    _jobRepo.updateStopTime(job);

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverReachedStop,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobReachedStop(job, journey));
    });
  }

  exitStop(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Update Stop Time
    DateTime reachTime = Timestamp.fromMillisecondsSinceEpoch(int.parse(job
            .scheduledStops[journey.toStop.id]['reachTime']['actual']
            .toString()))
        .toDate();
    DateTime leaveTime = DateTime.now();
    int duration = leaveTime.difference(reachTime).inSeconds;
    job.scheduledStops[journey.toStop.id]['leaveTime']['actual'] =
        Timestamp.fromDate(leaveTime).millisecondsSinceEpoch;
    job.scheduledStops[journey.toStop.id]['stopOverDuration']['actual'] =
        duration;
    _jobRepo.updateStopTime(job);

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverExitStop,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobExitStop(job, journey));
    });
  }

  notifyCash(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0 || _connected == 1) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          journey);
      return job;
    }

    // if (job.priceInfo.paymentType == PaymentType.atDrop) {
    if (journey.nextRef == null) {
      Price price = await getPrice(job);
      final _price = Price(
          estimatedAmount: job.priceInfo.estimatedAmount,
          paymentStatus: PaymentStatus.unpaid,
          paymentType: job.priceInfo.paymentType,
          discount: price.discount,
          finalAmount: double.parse(price.finalAmount.toStringAsFixed(0)),
          flatRate: price.flatRate,
          surcharge: price.surcharge,
          variableRate: price.variableRate,
          paymentTypeJourney: job.priceInfo.paymentTypeJourney);
      job.priceInfo = _price;
      await saveCurrentJob(job);
      _jobRepo.updateJobPrice(job.jobId, _price);
    }

    Future.delayed(Duration(milliseconds: 100), () async {
      emit(JobCollectCash(job, 'cash_notified'.tr(), journey));
    });
  }

  collectCash(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          journey);
      return;
    }

    job.priceInfo.paymentStatus = PaymentStatus.paid;
    await saveCurrentJob(job);
    // To be changed to batch
    _jobRepo.updateJobPrice(job.jobId, job.priceInfo);
    _jobRepo.updateCashInHand(
        (job.priceInfo.finalAmount ?? job.priceInfo.estimatedAmount) +
            currentDriver.cashInHand);
    Future.delayed(Duration(milliseconds: 100), () async {
      emit(JobCollectCash(job, 'cash_collected'.tr(), journey));
    });
  }

  takePicDropoffCertificate(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Set Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverProofOfDelivery,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    await saveCurrentJob(job);

    _jobRepo.setDriverStatus(
        job.jobId, job.driverStatus, job.driverStatusHistoryJourney);

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobTakePicDropoffCertificate(job, journey));
    });
  }
  // Dropoff End

  completeJob(Job job, Journey journey) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0 || _connected == 1) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          journey);
      return;
    }

    Journey paymentJourney;
    if (job.priceInfo.paymentTypeJourney != null) {
      job.journey.forEach((k, v) {
        if (v['to'] == job.priceInfo.paymentTypeJourney)
          paymentJourney = Journey.fromJson(v);
      });
    }

    // if ((job.priceInfo.paymentType == PaymentType.atPick ||
    //         job.priceInfo.paymentType == PaymentType.other) &&
    if ((job.priceInfo.paymentTypeJourney == null ||
            (paymentJourney != null && paymentJourney.nextRef != null)) &&
        job.vehicleType != VehicleType.superx) {
      Price price = await getPrice(job);
      final _price = Price(
          estimatedAmount: job.priceInfo.estimatedAmount,
          paymentStatus: PaymentStatus.paid,
          paymentType: job.priceInfo.paymentType,
          discount: price.discount,
          finalAmount: double.parse(price.finalAmount.toStringAsFixed(0)),
          flatRate: price.flatRate,
          surcharge: price.surcharge,
          variableRate: price.variableRate,
          paymentTypeJourney: job.priceInfo.paymentTypeJourney);
      job.priceInfo = _price;
      _jobRepo.updateJobPrice(job.jobId, _price);
    }

    // Set Job & Driver Status
    Position _position = await getGeoCurrentLocation(journey);
    if (_position == null) return;
    Coordinates _coordinates =
        Coordinates(lat: _position.latitude, lng: _position.longitude);

    job.driverStatus = DriverStatusTS(DriverStatus.driverCheckout,
        Timestamp.fromDate(DateTime.now()), _coordinates, journey.id);
    job.driverStatusHistoryJourney == null ||
            !job.driverStatusHistoryJourney.containsKey(journey.id)
        ? job.driverStatusHistoryJourney[journey.id] = [
            job.driverStatus.toJson()
          ]
        : job.driverStatusHistoryJourney[journey.id]
            .add(job.driverStatus.toJson());
    job.jobStatus =
        JobStatusTS(JobStatus.jobFinished, Timestamp.fromDate(DateTime.now()));
    job.jobStatusHistory.add(job.jobStatus.toJson());

    _jobRepo.setDriverJobStatus(job.jobId, job.driverStatus, job.jobStatus,
        job.driverStatusHistoryJourney);

    // Set Job Times
    _jobRepo.setJobTimes(
        job.jobId,
        Timestamp.fromMillisecondsSinceEpoch(int.parse(job
            .driverStatusHistoryJourney[job.startJourney].first['timestamp'])),
        job.jobStatus.timestamp);

    // Update Distances
    /*
    if (currentDriver.driverType == DriverType.dynamic) {
      job = await _jobRepo.setTotalDistance(job, 'DropoffToBase');
    }
    */

    // Check ETA > 130%
    // jobETA130(job,journey, currentDriver, Timestamp.now());

    // Save Last Job
    await saveLastJob(job);
    await saveCurrentJob(job);

    // Send Notification
    sendFCM(job, 'Order Complete', 'Your order has been completed');

    Future.delayed(Duration(milliseconds: 100), () {
      emit(JobComplete(job));
    });
  }

  moveToBase(Job job) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0 || _connected == 1) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          null);
      return;
    }

    bool appRestart = currentJob == null;

    final base = await _jobRepo.getBase();
    if (!appRestart) {
      await _jobRepo.setCurrentJob(null, currentJob.jobId);
      await saveCurrentJob(null);
    }

    if (appRestart)
      Future.delayed(Duration(milliseconds: 100), () {
        emit(JobMoveBase(base, job));
      });
  }

  setCurrentJob(String jobId, String jobHistory) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0 || _connected == 1) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          null);
      return;
    }

    await _jobRepo.setCurrentJob(jobId, jobHistory);
    await saveCurrentJob(null);

    emit(JobInitial(currentJob, null));
  }

  cancelJob(String jobId, String driverId) async {
    return await _jobRepo.cancelJob(jobId, driverId);
  }

  reachedBase(Job job) async {
    emit(JobLoadingPage());

    // Check Internet
    int _connected = await checkInternet();
    if (_connected == 0) {
      showError(
          currentJob,
          errors[1].tr(),
          'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png',
          null);
      return;
    }

    // Update Distances
    // job = await _jobRepo.setTotalDistance(job, 'DropoffToBase');

    await _jobRepo.reachedBase(job);
    emit(JobInitial(currentJob, null));
  }

  // Stops

  updateStops(DriverStop stop, Job job, Journey journey, bool end) {
    emit(JobLoadingPage());
    final _job = _jobRepo.updateStops(stop, job, end);

    continueJob(_job, journey);
    return _job;
  }

  // Job Flow End

  // Job History

  jobListPage(List<String> jobs) {
    emit(JobList(jobs));
  }

  jobDetailsPage(Job job) {
    emit(JobDetails(job));
  }

  dashboardPage() {
    emit(JobInitial(null, currentDriver?.currentJob ?? currentJobId));
  }

  // Job History End

  // Error Check

  showError(
      Job job, String title, String desc, String imagePath, Journey journey) {
    if (!jobDialog && !dashDialog)
      emit(JobError(job, title, desc, imagePath, journey));
  }

  retryAfterError(Job job, String error, Journey journey) async {
    emit(JobLoadingPage());

    Future.delayed(Duration(milliseconds: 200), () async {
      if (error == errors[0]) {
        bool _serviceEnabled = await Geolocator.isLocationServiceEnabled();
        _serviceEnabled
            ? continueJob(job, journey)
            : showError(
                currentJob,
                errors[1].tr(),
                'please_turn_on_internet_services'.tr(),
                'assets/no_location.png',
                journey);
      } else {
        int _connected = await checkInternet();
        _connected == 2 ||
                (_connected == 1 && isCurrentDriver() && isCurrentJob())
            ? continueJob(job, journey)
            : showError(
                currentJob,
                errors[1].tr(),
                'please_turn_on_internet_services'.tr(),
                'assets/no_internet.png',
                journey);
      }
    });
  }

  // Error Check End

  // Create Job Start

  createJob() async {
    return await _jobRepo.createJob();
  }

  // Create Job End
}
