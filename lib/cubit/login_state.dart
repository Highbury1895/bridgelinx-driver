part of 'login_cubit.dart';

@immutable
abstract class LoginState {
  const LoginState();
}

class LoginInitial extends LoginState {
  const LoginInitial();
}

class LoginLoading extends LoginState {
  const LoginLoading();
}

class LoginNumber extends LoginState {
  const LoginNumber();
}

class LoginLoaded extends LoginState {
  final User firebaseUser;
  final Driver driverInfo;
  const LoginLoaded(this.firebaseUser, this.driverInfo);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is LoginLoaded &&
        o.firebaseUser == firebaseUser &&
        o.driverInfo == driverInfo;
  }

  @override
  int get hashCode => firebaseUser.hashCode + driverInfo.hashCode;
}

class LoginCode extends LoginState {
  final int number;
  const LoginCode(this.number);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is LoginCode && o.number == number;
  }

  @override
  int get hashCode => number.hashCode;
}

class LoginError extends LoginState {
  final String title;
  final String desc;
  final String imagePath;
  const LoginError(this.title, this.desc, this.imagePath);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is LoginError &&  o.title == title && o.desc == desc && o.imagePath == imagePath;
  }

  @override
  int get hashCode => title.hashCode + desc.hashCode + imagePath.hashCode;
}
