import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/Repos/driver_repo.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:meta/meta.dart';
import 'package:easy_localization/easy_localization.dart';
part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final DriverRepo _driverRepo;
  final FirebaseAuth _firebaseAuth;

  LoginCubit(this._driverRepo, this._firebaseAuth) : super(LoginInitial());

  checkLogin() async {
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
    _firebaseAuth.currentUser != null
        ? goToDashboard(_firebaseAuth.currentUser)
        : goToLoginInit();
  }

  blockDriver(Driver driver, bool _bool) async {
    final result = await _driverRepo.blockDriver(driver, _bool);
    return result;
  }

  goToLoginInit() {
    emit(LoginInitial());
  }

  goToLogin() {
    emit(LoginNumber());
  }

  goToLoginCode(int number) async {
    int _connected = await checkInternet();
    _connected == 2
        ? emit(LoginCode(number))
        : showError(errors[1].tr(), 'please_turn_on_internet_services'.tr(),
            'assets/no_internet.png');
  }

  goToDashboard(User firebaseUser) async {
    emit(LoginLoading());

    // Check Internet
    int _connected = await checkInternet();
    // print('Printing ${_connected.toString()} ${isCurrentJob()} ${isCurrentDriver()}');
    if (_connected == 0 ||
        (_connected == 1 && !isCurrentJob() && !isCurrentDriver())) {
      showError(errors[1].tr(), 'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png');
      return;
    }

    Driver result;
    if (currentDriver == null) {
      result = _connected == 2
          ? await _driverRepo.getDriver(firebaseUser)
          : getCurrentDriverSP();
      await saveCurrentDriver(result);
      // print('Driver ${result.profile.toJson()} ${result.toJsonSP()} ');
    } else {
      result = currentDriver;
    }

    if (currentDriver == null) {
      Fluttertoast.showToast(msg: 'client_user_exists'.tr());
      logout();
    } else {
      emit(LoginLoaded(firebaseUser, result));
    }
  }

  logout() async {
    emit(LoginLoading());
    if (currentDriver != null) {
      await setDriverOnline(false);
      await setFCMToken(null);
    }
    await saveCurrentDriver(null);
    _firebaseAuth.signOut().then((value) => emit(LoginInitial()));
  }

  Future<void> setDriverOnline(bool _bool) async {
    await _driverRepo.setDriverOnline(_bool);
  }

  Future<void> setFCMToken(String token) async {
    await _driverRepo.setFCMToken(token);
  }

  Stream driverListener() {
    return _driverRepo.driverStream();
  }
  // Error Check

  showError(String title, String desc, String imagePath) {
    if (!jobDialog && !dashDialog) emit(LoginError(title, desc, imagePath));
  }

  retryAfterError(String error) async {
    emit(LoginLoading());

    Future.delayed(Duration(milliseconds: 200), () async {
      int _connected = await checkInternet();
      _connected != 0
          ? FirebaseAuth.instance.currentUser != null
              ? goToDashboard(FirebaseAuth.instance.currentUser)
              : goToLogin()
          : showError(errors[1].tr(), 'please_turn_on_internet_services'.tr(),
              'assets/no_internet.png');
    });
  }
}
