part of 'job_cubit.dart';

@immutable
abstract class JobState {
  const JobState();
}

class JobLoadingPage extends JobState {
  const JobLoadingPage();
}

class JobInitial extends JobState {
  final Job job;
  final String jobId;

  const JobInitial(this.job, this.jobId);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobInitial && o.job == job && o.jobId == jobId;
  }

  @override
  int get hashCode => job.hashCode + jobId.hashCode;
}

class JobExitBase extends JobState {
  final Job job;
  final Journey journey;
  const JobExitBase(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobExitBase && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobMovePickup extends JobState {
  final Job job;
  final Journey journey;
  const JobMovePickup(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobMovePickup && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobTakePicPickupCheckIn extends JobState {
  final Job job;
  final Journey journey;
  const JobTakePicPickupCheckIn(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobTakePicPickupCheckIn && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobStartLoading extends JobState {
  final Job job;
  final Journey journey;
  const JobStartLoading(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobStartLoading && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobSetLoaders extends JobState {
  final Job job;
  final Journey journey;
  const JobSetLoaders(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobSetLoaders && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobLoadingComplete extends JobState {
  final Job job;
  final Journey journey;
  const JobLoadingComplete(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobLoadingComplete && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobTakePicPickup extends JobState {
  final Job job;
  final Journey journey;
  const JobTakePicPickup(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobTakePicPickup && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobTakePicPickupGatePass extends JobState {
  final Job job;
  final Journey journey;
  const JobTakePicPickupGatePass(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobTakePicPickupGatePass && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobExitPickup extends JobState {
  final Job job;
  final Journey journey;
  const JobExitPickup(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobExitPickup && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobMoveDrop extends JobState {
  final Job job;
  final Journey journey;
  const JobMoveDrop(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobMoveDrop && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobTakePicDropoffCheckIn extends JobState {
  final Job job;
  final Journey journey;
  const JobTakePicDropoffCheckIn(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobTakePicDropoffCheckIn && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobUnloading extends JobState {
  final Job job;
  final Journey journey;
  const JobUnloading(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobUnloading && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobSetUnloaders extends JobState {
  final Job job;
  final Journey journey;
  const JobSetUnloaders(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobSetUnloaders && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobUnloadingComplete extends JobState {
  final Job job;
  final Journey journey;
  const JobUnloadingComplete(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobUnloadingComplete && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobTakePicDropoff extends JobState {
  final Job job;
  final Journey journey;
  const JobTakePicDropoff(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobTakePicDropoff && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobTakePicDropoffGatePass extends JobState {
  final Job job;
  final Journey journey;
  const JobTakePicDropoffGatePass(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobTakePicDropoffGatePass && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobExitDropOff extends JobState {
  final Job job;
  final Journey journey;
  const JobExitDropOff(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobExitDropOff && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobMoveStop extends JobState {
  final Job job;
  final Journey journey;
  const JobMoveStop(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobMoveStop && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobReachedStop extends JobState {
  final Job job;
  final Journey journey;
  const JobReachedStop(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobReachedStop && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobExitStop extends JobState {
  final Job job;
  final Journey journey;
  const JobExitStop(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobExitStop && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobCollectCash extends JobState {
  final Job job;
  final Journey journey;
  final String buttonText;
  const JobCollectCash(this.job, this.buttonText, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobCollectCash && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobTakePicDropoffCertificate extends JobState {
  final Job job;
  final Journey journey;
  const JobTakePicDropoffCertificate(this.job, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobTakePicDropoffCertificate && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobComplete extends JobState {
  final Job job;
  const JobComplete(this.job);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobComplete && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}

class JobMoveBase extends JobState {
  final Base base;
  final Job job;
  const JobMoveBase(this.base, this.job);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobMoveBase && o.base == base && o.job == job;
  }

  @override
  int get hashCode => base.hashCode + job.hashCode;
}

class JobError extends JobState {
  final Job job;
  final Journey journey;
  final String title;
  final String desc;
  final String imagePath;

  const JobError(this.job, this.title, this.desc, this.imagePath, this.journey);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobError &&
        o.job == job &&
        o.title == title &&
        o.desc == desc &&
        o.imagePath == imagePath;
  }

  @override
  int get hashCode =>
      job.hashCode + title.hashCode + desc.hashCode + imagePath.hashCode;
}

// Jobs History

class JobList extends JobState {
  final List<String> jobs;
  const JobList(this.jobs);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobList && o.jobs == jobs;
  }

  @override
  int get hashCode => jobs.hashCode;
}

class JobDetails extends JobState {
  final Job job;
  const JobDetails(this.job);

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is JobDetails && o.job == job;
  }

  @override
  int get hashCode => job.hashCode;
}
