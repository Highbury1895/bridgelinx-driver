import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/DTO/stops.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

enum DriverStatus {
  driverInit,
  driverAccepted,
  driverMovingToPickup,
  driverArrived,
  driverLoading,
  driverLoadingCompleted,
  driverGatePass,
  driverLeft,
  driverMovingToDropOff,
  driverReachedDropOff,
  driverUnloading,
  driverUnloadingCompleted,
  driverProofOfDelivery,
  driverCheckout,
  driverMovingToBase,
  driverSetLoaders,
  driverExitPickup,
  driverSetUnloaders,
  driverExitDropoff,
  driverReachedStop,
  driverMovingToStop,
  driverExitStop
}

enum JobStatus {
  jobInit,
  jobScheduled,
  jobUnscheduled,
  jobOngoing,
  jobFinished,
  jobCritical,
  jobCancelled,
  jobScheduledStatic,
  jobCriticalStatic,
  jobCriticalSuper
}

enum VehicleType { small, medium, large, superx, xl20ft, xl40ft }
enum PaymentStatus { paid, unpaid }
enum PaymentType { atDrop, atPick, other }

// MultiDropoff
enum JobType { interCity, intraCity }
enum JobCreatedBy { mobile, portal }

class Job {
  String jobId;
  String clientId;
  String driverId;
  String vehicleId;
  int orderNumber;
  DriverType assignedDriverType;
  List<String> clientImages;
  List<String> driverImages;
  Timestamp pickupTime, startTime, endTime;
  DriverStatusTS driverStatus;
  JobStatusTS jobStatus;
  bool biggerTruckSent, orderModifyRequested, orderModifyProcessed;
  VehicleType vehicleType;
  Price priceInfo;
  Package packageInfo;
  double cashForGoods;
  ClientCoordinates clientCoordinates;
  Coordinates driverCurrentCoordinates;
  int numberOfLoaders, numberOfUnloaders;
  List jobStatusHistory;
  List driverStatusHistory;

  String businessCode;

  Map<String, dynamic> distances = {};
  Map<String, dynamic> times = {};

  //MultiDropoff
  int jobCash;
  int clientAppVersion;
  JobType jobType;
  JobCreatedBy createdBy;
  String startJourney;
  ClientCoordinatesJourney clientCoordinatesJourney;

  Map<String, dynamic> journey = {};
  Map<String, dynamic> driverStatusHistoryJourney = {};

  Map<String, dynamic> packageInfoJourney = {};
  Map<String, dynamic> numberOfLoadersJourney = {};
  Map<String, dynamic> numberOfUnloadersJourney = {};

  Map<String, dynamic> clientImagesJourney = {};
  Map<String, dynamic> driverImagesJourney = {};

  Map<String, dynamic> unscheduledStops = {};
  Map<String, dynamic> scheduledStops = {};

  DriverStop currentStop;

  Job({
    @required this.clientId,
    @required this.priceInfo,
    @required this.packageInfo,
    @required this.clientCoordinates,
    this.jobId,
    this.driverId,
    this.vehicleId,
    this.clientImages,
    this.driverImages,
    this.driverStatus,
    this.jobStatus,
    this.driverCurrentCoordinates,
    this.assignedDriverType,
    this.biggerTruckSent,
    this.cashForGoods,
    this.pickupTime,
    this.endTime,
    this.startTime,
    this.orderModifyProcessed,
    this.orderModifyRequested,
    this.vehicleType,
    this.numberOfLoaders,
    this.numberOfUnloaders,
    this.driverStatusHistory,
    this.jobStatusHistory,
  }); //20 params

  Map<String, dynamic> toJson() => {
        '_id': this.jobId,
        'clientId': this.clientId,
        'vehicleId': this.vehicleId,
        'driverId': this.driverId,
        'businessCode': this.businessCode,
        'orderNumber': this.orderNumber,
        'clientImages': this.clientImagesJourney ?? {},
        'driverImages': this.driverImagesJourney ?? {},
        'driverStatus': this.driverStatus.toJson(),
        'jobStatus': this.jobStatus.toJson(),
        'priceInfo': this.priceInfo.toJson(),
        'numberOfLoaders': this.numberOfLoadersJourney ?? {},
        'numberOfUnloaders': this.numberOfUnloadersJourney ?? {},
        'packageInfo': this.packageInfoJourney ?? {},
        'clientCoordinates': this.clientCoordinatesJourney ?? {},
        'driverCurrentCoordinates': this.driverCurrentCoordinates != null
            ? this.driverCurrentCoordinates.toJson()
            : null,
        'assignedDriverType': this.assignedDriverType != null
            ? this.assignedDriverType.toString().split('.').last
            : '',
        'biggerTruckSent':
            this.biggerTruckSent != null ? this.biggerTruckSent.toString() : '',
        'cashForGoods': this.cashForGoods.toString(),
        'endTime': this.endTime.seconds.toString(),
        'startTime': this.startTime.seconds.toString(),
        'pickupTime': this.pickupTime.seconds.toString(),
        'orderModifyRequested': this.orderModifyRequested != null
            ? this.orderModifyRequested.toString()
            : '',
        'orderModifyProcessed': this.orderModifyProcessed != null
            ? this.orderModifyProcessed.toString()
            : '',
        'vehicleType': this.vehicleType.toString().split('.').last,
        'driverStatusHistory': this.driverStatusHistoryJourney,
        'jobStatusHistory': this.jobStatusHistory,
        'distances': this.distances,
        'times': this.times,
        'journey': {'start': this.startJourney, 'flow': this.journey},
        'jobCash': this.jobCash,
        'clientAppVersion': this.clientAppVersion,
        'jobType': this.jobType != null
            ? this.jobType.toString().split('.').last
            : null,
        'createdBy': this.createdBy != null
            ? this.createdBy.toString().split('.').last
            : null,
        'unscheduledStops': this.unscheduledStops,
        'stops': this.scheduledStops,
        'currentStop': this.currentStop?.toJson(),
      };

  Job.fromJson(Map<String, dynamic> jsonStr)
      : this.jobId = jsonStr['_id'],
        this.clientId = jsonStr['clientId'],
        this.vehicleId = jsonStr['vehicleId'],
        this.driverId = jsonStr['driverId'],
        this.orderNumber = jsonStr['orderNumber'],
        this.businessCode = jsonStr['businessCode'],
        this.clientImages = List.from(jsonStr['clientImages'] ?? List.empty()),
        this.driverImages = List.from(jsonStr['driverImages'] ?? List.empty()),
        this.numberOfLoaders =
            jsonStr['numberOfLoaders'] != null ? jsonStr['numberOfLoaders'] : 0,
        this.numberOfUnloaders = jsonStr['numberOfUnloaders'] != null
            ? jsonStr['numberOfUnloaders']
            : 0,
        this.driverStatus = DriverStatusTS.fromJson(jsonStr['driverStatus']),
        this.jobStatus = JobStatusTS.fromJson(jsonStr['jobStatus']),
        this.priceInfo = Price.fromJson(jsonStr['priceInfo']),
        this.packageInfo = Package.fromJson(jsonStr['packageInfo']),
        this.clientCoordinates = ClientCoordinates.fromJson(
          jsonStr['clientCoordinates'],
        ),
        this.assignedDriverType = jsonStr['assignedDriverType'] != '' &&
                jsonStr['assignedDriverType'] != null
            ? DriverType.values.firstWhere((element) =>
                element.toString().split('.').last ==
                jsonStr['assignedDriverType'])
            : null,
        this.biggerTruckSent =
            jsonStr['biggerTruckSent'] == 'true' ? true : false,
        this.cashForGoods = jsonStr['cashForGoods'] != null
            ? double.parse(jsonStr['cashForGoods'].toString())
            : 0,
        this.endTime = Timestamp.fromDate(
          DateTime.fromMillisecondsSinceEpoch(
            int.parse(
              jsonStr['endTime'].toString(),
            ),
          ),
        ),
        this.startTime = Timestamp.fromDate(
          DateTime.fromMillisecondsSinceEpoch(
            int.parse(
              jsonStr['startTime'].toString(),
            ),
          ),
        ),
        this.pickupTime = Timestamp.fromDate(
          DateTime.fromMillisecondsSinceEpoch(
            int.parse(
              jsonStr['pickupTime'].toString(),
            ),
          ),
        ),
        this.orderModifyProcessed =
            jsonStr['orderModifyProcessed'] == 'true' ? true : false,
        this.orderModifyRequested =
            jsonStr['orderModifyRequested'] == 'true' ? true : false,
        this.vehicleType = VehicleType.values.firstWhere((element) =>
            element.toString().split('.').last == jsonStr['vehicleType']),
        this.driverCurrentCoordinates =
            jsonStr['driverCurrentCoordinates'] != null
                ? Coordinates.fromJson(jsonStr['driverCurrentCoordinates'])
                : null,
        this.jobStatusHistory = jsonStr['jobStatusHistory'] ?? new List(),
        this.driverStatusHistory = jsonStr['driverStatusHistory'] ?? new List(),
        this.distances = jsonStr['distances'] ?? {},
        this.times = jsonStr['times'] ?? {},
        this.clientAppVersion = jsonStr['clientAppVersion'] != null
            ? int.tryParse(jsonStr['clientAppVersion'].toString())
            : 32;

  Job.fromJsonJourney(Map<String, dynamic> jsonStr)
      : this.jobId = jsonStr['_id'],
        this.clientId = jsonStr['clientId'],
        this.vehicleId = jsonStr['vehicleId'],
        this.driverId = jsonStr['driverId'],
        this.orderNumber = jsonStr['orderNumber'],
        this.businessCode = jsonStr['businessCode'],
        this.driverStatus = DriverStatusTS.fromJson(jsonStr['driverStatus']),
        this.jobStatus = JobStatusTS.fromJson(jsonStr['jobStatus']),
        this.priceInfo = Price.fromJson(jsonStr['priceInfo']),
        this.assignedDriverType = jsonStr['assignedDriverType'] != '' &&
                jsonStr['assignedDriverType'] != null
            ? DriverType.values.firstWhere((element) =>
                element.toString().split('.').last ==
                jsonStr['assignedDriverType'])
            : null,
        this.biggerTruckSent =
            jsonStr['biggerTruckSent'] == 'true' ? true : false,
        this.cashForGoods = jsonStr['cashForGoods'] != null
            ? double.parse(jsonStr['cashForGoods'].toString())
            : 0,
        this.endTime = Timestamp.fromDate(
          DateTime.fromMillisecondsSinceEpoch(
            int.parse(
              jsonStr['endTime'].toString(),
            ),
          ),
        ),
        this.startTime = Timestamp.fromDate(
          DateTime.fromMillisecondsSinceEpoch(
            int.parse(
              jsonStr['startTime'].toString(),
            ),
          ),
        ),
        this.pickupTime = Timestamp.fromDate(
          DateTime.fromMillisecondsSinceEpoch(
            int.parse(
              jsonStr['pickupTime'].toString(),
            ),
          ),
        ),
        this.orderModifyProcessed =
            jsonStr['orderModifyProcessed'] == 'true' ? true : false,
        this.orderModifyRequested =
            jsonStr['orderModifyRequested'] == 'true' ? true : false,
        this.vehicleType = VehicleType.values.firstWhere((element) =>
            element.toString().split('.').last == jsonStr['vehicleType']),
        this.driverCurrentCoordinates =
            jsonStr['driverCurrentCoordinates'] != null
                ? Coordinates.fromJson(jsonStr['driverCurrentCoordinates'])
                : null,
        this.jobStatusHistory = jsonStr['jobStatusHistory'] ?? new List(),
        this.distances = jsonStr['distances'] ?? {},
        this.times = jsonStr['times'] ?? {},
        // MultiDropoff
        this.jobCash = jsonStr['jobCash'] != null
            ? int.parse(jsonStr['jobCash'].toString())
            : 0,
        this.clientAppVersion = jsonStr['clientAppVersion'] != null
            ? int.parse(jsonStr['clientAppVersion'].toString())
            : 32,
        this.jobType = jsonStr['jobType'] != null
            ? JobType.values.firstWhere(
                (element) =>
                    element.toString().toLowerCase().split('.').last ==
                    jsonStr['jobType'].toString().toLowerCase(),
                orElse: () => JobType.intraCity)
            : JobType.intraCity,
        this.createdBy = jsonStr['createdBy'] != null
            ? JobCreatedBy.values.firstWhere(
                (element) =>
                    element.toString().toLowerCase().split('.').last ==
                    jsonStr['createdBy'].toString().toLowerCase(),
                orElse: () => JobCreatedBy.mobile)
            : JobCreatedBy.mobile,
        this.clientCoordinatesJourney = ClientCoordinatesJourney.fromJson(
          jsonStr['clientCoordinates'],
        ),
        this.startJourney = jsonStr['journey']['start'],
        this.journey = jsonStr['journey']['flow'],
        this.packageInfoJourney = jsonStr['packageInfo'],
        this.clientImagesJourney = jsonStr['clientImages'],
        this.driverImagesJourney = jsonStr['driverImages'],
        this.numberOfLoadersJourney = jsonStr['numberOfLoaders'],
        this.numberOfUnloadersJourney = jsonStr['numberOfUnloaders'],
        this.driverStatusHistoryJourney = jsonStr['driverStatusHistory'],
        this.unscheduledStops = jsonStr['unscheduledStops'] ?? {},
        this.scheduledStops = jsonStr['stops'] ?? {},
        this.currentStop = jsonStr['currentStop'] != null
            ? DriverStop.fromJson(jsonStr['currentStop'])
            : null;
}

class JobStatusTS {
  final JobStatus status;
  final Timestamp timestamp;
  JobStatusTS(this.status, this.timestamp);
  Map<String, dynamic> toJson() => {
        'status': this.status.toString().split('.').last,
        'timestamp': this.timestamp.millisecondsSinceEpoch.toString()
      };
  JobStatusTS.fromJson(Map<String, dynamic> json)
      : this.status = JobStatus.values.firstWhere(
            (element) => element.toString().split('.').last == json['status']),
        this.timestamp = Timestamp.fromMillisecondsSinceEpoch(
            int.parse(json['timestamp'].toString()));
}

class DriverStatusTS {
  final DriverStatus status;
  final Timestamp timestamp;
  final Coordinates coordinates;
  final String journey;
  DriverStatusTS(this.status, this.timestamp, this.coordinates, this.journey);
  Map<String, dynamic> toJson() => {
        'status': this.status.toString().split('.').last,
        'timestamp': this.timestamp.millisecondsSinceEpoch.toString(),
        'coordinates': this.coordinates?.toJsonOnly(),
        'journey': this.journey
      };
  DriverStatusTS.fromJson(Map<String, dynamic> json)
      : this.status = DriverStatus.values.firstWhere((element) =>
            element.toString().toLowerCase().split('.').last ==
            json['status'].toString().toLowerCase()),
        this.timestamp = Timestamp.fromMillisecondsSinceEpoch(
            int.parse(json['timestamp'].toString())),
        this.coordinates = null,
        this.journey = json['journey'];
}

class Coordinates {
  String id;
  double lng, lat;
  String contactName, contactPhone;
  String address,
      formattedAddress; // address == addressDetails from the form .. formattedAddress == from google object
  String timestamp;
  int number;
  Coordinates(
      {@required this.lat,
      @required this.lng,
      this.address,
      this.contactName,
      this.contactPhone,
      this.timestamp,
      this.formattedAddress,
      this.number});

  Map<String, dynamic> toJson() => {
        'coordinates': {
          'lng': this.lng.toString(),
          'lat': this.lat.toString(),
        },
        'contactName': this.contactName,
        'contactPhone': this.contactPhone,
        'address': this.address ?? '',
        'formattedAddress': this.formattedAddress ?? '',
        'timestamp': this.timestamp,
        'number': this.number ?? 0
      };

  Map<String, dynamic> toJsonOnly() => {
        'lng': this.lng.toString(),
        'lat': this.lat.toString(),
      };

  Coordinates.fromJson(Map<String, dynamic> json)
      : this.lng = double.parse(json['coordinates']['lng'].toString()),
        this.lat = double.parse(json['coordinates']['lat'].toString()),
        this.contactName = json['contactName'],
        this.contactPhone = json['contactPhone'],
        this.address = json['address'] ?? '',
        this.formattedAddress = json['formattedAddress'] ?? '',
        this.timestamp = json['timestamp']?.toString(),
        this.number =
            json['number'] != null ? int.parse(json['number'].toString()) : 0;

  Coordinates.fromJsonOnly(Map<String, dynamic> json, {var map})
      : this.lng = double.parse(json['lng'].toString()),
        this.lat = double.parse(json['lat'].toString()),
        this.address = map != null ? map['address'] : null,
        this.formattedAddress = map != null ? map['formattedAddress'] : null;
}

class Price {
  double estimatedAmount;
  double finalAmount;
  PaymentStatus paymentStatus;
  PaymentType paymentType;
  String paymentTypeJourney;
  double flatRate, variableRate, surcharge, discount;
  Price(
      {@required this.estimatedAmount,
      this.finalAmount,
      @required this.paymentStatus,
      @required this.paymentType,
      this.discount,
      this.flatRate,
      this.surcharge,
      this.variableRate,
      this.paymentTypeJourney});

  Map<String, dynamic> toJson() => {
        'estimatedAmount': this.estimatedAmount,
        'finalAmount': this.finalAmount,
        'paymentStatus':
            this.paymentStatus.toString().toLowerCase().split('.').last,
        'paymentType': this.paymentTypeJourney,
        // this.paymentTypeJourney.toString().toLowerCase().split('.').last,
        'discount': this.discount,
        'flatRate': this.flatRate,
        'surcharge': this.surcharge,
        'variableRate': this.variableRate,
      };

  Price.fromJson(Map<String, dynamic> json)
      : this.estimatedAmount = json['estimatedAmount'] != null
            ? double.parse(json['estimatedAmount'].toString())
            : null,
        this.finalAmount = json['finalAmount'] != null
            ? double.parse(json['finalAmount'].toString())
            : null,
        this.discount = json['discount'] != null
            ? double.parse(json['discount'].toString())
            : null,
        this.flatRate = json['flatRate'] != null
            ? double.parse(json['flatRate'].toString())
            : null,
        this.surcharge = json['surcharge'] != null
            ? double.parse(json['surcharge'].toString())
            : 0,
        this.variableRate = json['variableRate'] != null
            ? double.parse(json['variableRate'].toString())
            : null,
        this.paymentStatus = PaymentStatus.values.firstWhere((element) =>
            element.toString().toLowerCase().split('.').last ==
            json['paymentStatus'].toString().toLowerCase()),
        this.paymentType = PaymentType.values.firstWhere(
            (element) =>
                element.toString().toLowerCase().split('.').last ==
                json['paymentType'].toString().toLowerCase(),
            orElse: () => null),
        this.paymentTypeJourney = json['paymentType'];

  Price.fromCloud(Map<String, dynamic> json)
      : this.finalAmount = json['finalAmount'] != null
            ? double.parse(json['finalAmount'].toString())
            : null,
        this.discount = json['discount'] != null
            ? double.parse(json['discount'].toString())
            : null,
        this.flatRate = json['flatRate'] != null
            ? double.parse(json['flatRate'].toString())
            : null,
        this.surcharge = json['surcharge'] != null
            ? double.parse(json['surcharge'].toString())
            : 0,
        this.variableRate = json['variableRate'] != null
            ? double.parse(json['variableRate'].toString())
            : null;
}

class Package {
  int noOfUnits;
  String typeOfFreight;
  String freightDetails;
  int freightWeight;
  String freightWeightUnit;
  String customFreightType;

  Package(
      {@required this.noOfUnits,
      this.freightDetails,
      this.typeOfFreight,
      this.freightWeight,
      this.freightWeightUnit,
      this.customFreightType});

  Map<String, dynamic> toJson() => {
        'noOfUnits': this.noOfUnits.toString(),
        'freightDetails': this.freightDetails.toString(),
        'typeOfFreight': this.typeOfFreight.toString(),
        'freightWeightUnit': this.freightWeightUnit.toString(),
        'freightWeight': this.freightWeight,
        'customFreightType': this.customFreightType
      };
  Package.fromJson(Map<String, dynamic> json)
      : this.noOfUnits = int.parse(json['noOfUnits'].toString()),
        this.typeOfFreight = json['typeOfFreight'],
        this.freightDetails = json['freightDetails'],
        this.freightWeight = json['freightWeight'] ?? 0,
        this.freightWeightUnit = json['freightWeightUnit'] ?? 'kg',
        this.customFreightType = json['customFreightType'];
}

class ClientCoordinates {
  final Coordinates pickup, dropoff;
  ClientCoordinates(this.pickup, this.dropoff);

  Map<String, dynamic> toJson() => {
        'pickUp': this.pickup.toJson(),
        'dropOff': this.dropoff.toJson(),
      };

  ClientCoordinates.fromJson(Map<String, dynamic> json)
      : this.pickup = Coordinates.fromJson(json['pickUp']),
        this.dropoff = Coordinates.fromJson(json['dropOff']);
}

class ClientCoordinatesJourney {
  final Map<String, dynamic> pickup, dropoff;
  ClientCoordinatesJourney(this.pickup, this.dropoff);

  Map<String, dynamic> toJson() => {
        'pickUp': this.pickup,
        'dropOff': this.dropoff,
      };

  ClientCoordinatesJourney.fromJson(Map<String, dynamic> json)
      : this.pickup = json['pickUp'],
        this.dropoff = json['dropOff'];
}
