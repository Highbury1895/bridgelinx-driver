import 'dart:async';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'driver.dart';
import 'job.dart';
import 'journey.dart';

// Scheduled Stops

class ScheduledStops {
  String id;
  int number;
  String type;
  Coordinates coordinates;
  String address;
  String formattedAddress;
  Map<String, dynamic> reachTime;
  Map<String, dynamic> leaveTime;
  Map<String, dynamic> stopDuration;

  Map<String, dynamic> toJson() => {
        '_id': this.id,
        'number': this.number,
        'type': this.type,
        'address': this.address,
        'formattedAddress': this.formattedAddress,
        'coordinates': coordinates.toJsonOnly(),
        'reachTime': this.reachTime,
        'leaveTime': this.leaveTime,
        'stopOverDuration': this.stopDuration
      };

  ScheduledStops.fromJson(Map<String, dynamic> json)
      : this.id = json['_id'],
        this.number = json['number'],
        this.type = json['type'],
        this.address = json['address'],
        this.formattedAddress = json['formattedAddress'],
        this.coordinates = Coordinates.fromJsonOnly(json['coordinates'], map: {
          'address': json['address'],
          'formattedAddress': json['formattedAddress']
        }),
        this.reachTime = json['reachTime'],
        this.leaveTime = json['leaveTime'],
        this.stopDuration = json['stopOverDuration'];
}

// Unscheduled Stops

enum UnscheduledStopType {
  Challan,
  Rest,
  Refueling,
  Toll,
  Overnight,
  Breakdown
}

class UnscheduledStops {
  final String name;
  final String icon;
  final UnscheduledStopType type;
  final Color color;

  UnscheduledStops(this.name, this.icon, this.type, this.color);
}

class DriverStop {
  String id;
  Timestamp startTime;
  Timestamp endTime;
  UnscheduledStopType stopType;
  String journey;
  String jobId;
  Coordinates coordinates;
  int orderNumber;
  String driverId;
  String driverNumber;

  DriverStop(
      this.id,
      this.startTime,
      this.endTime,
      this.stopType,
      this.journey,
      this.jobId,
      this.coordinates,
      this.orderNumber,
      this.driverId,
      this.driverNumber);

  Map<String, dynamic> toJson() => {
        '_id': this.id,
        'startTime': this.startTime?.millisecondsSinceEpoch,
        'endTime': this.endTime?.millisecondsSinceEpoch,
        'stopType': this.stopType.toString().toLowerCase().split('.').last,
        'journey': this.journey,
        'jobId': this.jobId,
        'coordinates': this.coordinates?.toJsonOnly(),
        'orderNumber': this.orderNumber,
        'driverId': this.driverId,
        'driverNumber': this.driverNumber,
        'flag': true
      };

  DriverStop.fromJson(Map<String, dynamic> json)
      : this.id = json['_id'],
        this.startTime = json['startTime'] != null
            ? Timestamp.fromMillisecondsSinceEpoch(
                int.parse(json['startTime'].toString()))
            : null,
        this.endTime = json['endTime'] != null
            ? Timestamp.fromMillisecondsSinceEpoch(
                int.parse(json['endTime'].toString()))
            : null,
        this.stopType = UnscheduledStopType.values.firstWhere(
            (e) =>
                e.toString().toLowerCase().split('.').last == json['stopType'],
            orElse: () => null),
        this.journey = json['journey'],
        this.jobId = json['jobId'],
        this.coordinates = Coordinates.fromJsonOnly(json['coordinates']),
        this.orderNumber = int.parse(json['orderNumber']?.toString() ?? '0'),
        this.driverId = json['driverId'],
        this.driverNumber = json['driverNumber'];
}

List<Widget> widgetList = List.from([]);
List<UnscheduledStops> stopsList = [
  UnscheduledStops('challan'.tr(), 'assets/stops/policeman.png',
      UnscheduledStopType.Challan, Colors.red),
  UnscheduledStops('breakdown'.tr(), 'assets/stops/breakdown.png',
      UnscheduledStopType.Breakdown, Colors.orange),
  UnscheduledStops('refueling'.tr(), 'assets/stops/fuel.png',
      UnscheduledStopType.Refueling, Colors.pink),
  UnscheduledStops('toll'.tr(), 'assets/stops/toll.png',
      UnscheduledStopType.Toll, Colors.green),
  UnscheduledStops('overnight'.tr(), 'assets/stops/overnight.png',
      UnscheduledStopType.Overnight, Colors.cyan),
  UnscheduledStops('rest'.tr(), 'assets/stops/rest.png',
      UnscheduledStopType.Rest, Colors.indigo),
];

StreamController<Duration> _timeStream;
Timer _timer;

void _startTimer(Timestamp start) {
  _timeStream = StreamController<Duration>.broadcast();
  if (_timer != null && _timer.isActive) {
    _timer.cancel();
  }

  final startTime = start.toDate();
  Duration diff = DateTime.now().difference(startTime);
  _timeStream.add(diff);

  _timer = Timer.periodic(Duration(seconds: 1), (timer) {
    diff = DateTime.now().difference(startTime);
    _timeStream.add(diff);
  });
}

String _printDuration(Duration duration) {
  String twoDigits(int n) => n.toString().padLeft(2, "0");
  String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
  return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
}

_endStop(DriverStop stop, Job job, Journey journey) {
  stop.endTime = Timestamp.now();
  job = jobCubit.updateStops(stop, job, journey, true);
}

// UI

showUnscheduledStops(
    BuildContext context, Job job, Journey journey, Driver driver) {
  jobDialog = true;
  widgetList.clear();
  stopsList.forEach((e) => {
        widgetList.add(Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            MaterialButton(
              height: 50,
              minWidth: 50,
              onPressed: () async {
                Navigator.pop(context);

                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return WillPopScope(
                      onWillPop: () async => false,
                      child: Dialog(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                    margin: EdgeInsets.all(30),
                                    child: CircularProgressIndicator()),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );

                // Check Internet
                int _connected = await checkInternet();
                Navigator.pop(context);
                if (_connected == 0 || _connected == 1) {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) => WillPopScope(
                            onWillPop: () async => false,
                            child: Dialog(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              elevation: 2,
                              child: Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(15),
                                      width: 180,
                                      child: Text(
                                        'internet_off'.tr(),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 3,
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline5,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.all(15),
                                      width: 180,
                                      child: Text(
                                        'contact_support'.tr(),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 3,
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline5,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    FlatButton(
                                      padding: EdgeInsets.all(15),
                                      height: 50,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      color: Theme.of(context).primaryColorDark,
                                      onPressed: () {
                                        Navigator.pop(context);
                                        try {
                                          makePhoneCall(helpline);
                                        } catch (e) {
                                          print(e.toString());
                                        }
                                      },
                                      child: Center(
                                        child: Text(
                                          'okay'.tr(),
                                          style: Theme.of(context)
                                              .accentTextTheme
                                              .button,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )).then((_) {
                    showWaitingDialog(context, e, job, journey, driver);
                  });
                } else
                  showWaitingDialog(context, e, job, journey, driver);
              },
              color: e.color,
              textColor: Colors.white,
              child: Image.asset(
                e.icon,
                width: 30,
                height: 30,
              ),
              padding: EdgeInsets.all(16),
              shape: CircleBorder(),
            ),
            Container(
              margin: EdgeInsets.only(top: 5),
              child: Text(
                e.name,
                style: Theme.of(context).accentTextTheme.headline1,
              ),
            )
          ],
        ))
      });

  showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      builder: (BuildContext context) {
        jobDialog = true;
        return Container(
          padding: EdgeInsets.only(top: 20, bottom: 10, left: 10, right: 10),
          child: GridView.count(
            crossAxisCount: 3,
            shrinkWrap: true,
            crossAxisSpacing: 30,
            mainAxisSpacing: 30,
            padding: EdgeInsets.all(10),
            children: widgetList,
          ),
        );
      }).then((value) {
    jobDialog = false;
  });
}

showWaitingDialog(BuildContext context, UnscheduledStops stop, Job job,
    Journey journey, Driver driver) async {
  jobDialog = true;

  Position position;
  if (currentCoordinates == null)
    position = await getGeoCurrentLocation(journey);

  // Iniatialize Stop

  DriverStop _stop;
  Job _job;

  if (job.currentStop == null) {
    _stop = DriverStop(
        null,
        Timestamp.now(),
        null,
        stop.type,
        journey.id,
        job.jobId,
        currentCoordinates ??
            Coordinates(lat: position.latitude, lng: position.longitude),
        job.orderNumber,
        driver.driverId,
        driver.primaryPhoneNumber);
    job.currentStop = _stop;
    _job = jobCubit.updateStops(_stop, job, journey, false);
  } else {
    _stop = job.currentStop;
    _job = job;
  }

  _startTimer(_stop.startTime);

  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => StreamBuilder(
          stream: _timeStream.stream,
          builder: (BuildContext context, AsyncSnapshot<Duration> snapshot) {
            return StatefulBuilder(
              builder: (context, setState) => WillPopScope(
                onWillPop: () async => false,
                child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  elevation: 2,
                  child: Container(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Expanded(child: Container()),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.all(15),
                                width: 180,
                                child: Text(
                                  stop.name,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline5,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Expanded(
                              child: IconButton(
                                icon: Icon(
                                  FontAwesomeIcons.headset,
                                  color: Colors.black54,
                                  size: 30,
                                ),
                                onPressed: () => setState(() {
                                  makePhoneCall(helpline);
                                }),
                              ),
                            ),
                          ],
                        ),
                        MaterialButton(
                          height: 50,
                          minWidth: 50,
                          onPressed: () {},
                          color: stop.color,
                          child: Image.asset(
                            stop.icon,
                            width: 30,
                            height: 30,
                          ),
                          padding: EdgeInsets.all(16),
                          shape: CircleBorder(),
                        ),
                        Container(
                          margin: EdgeInsets.all(15),
                          width: 180,
                          child: Text(
                            snapshot != null && snapshot.data != null
                                ? _printDuration(snapshot.data)
                                : _printDuration(Duration(seconds: 0)),
                            style: Theme.of(context).accentTextTheme.headline4,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        FlatButton(
                          padding: EdgeInsets.all(15),
                          height: 50,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          color: Theme.of(context).primaryColorDark,
                          onPressed: () {
                            Navigator.pop(context);
                            _endStop(_stop, _job, journey);
                          },
                          child: Center(
                            child: Text(
                              'done'.tr(),
                              style: Theme.of(context).accentTextTheme.button,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          })).then((value) {
    jobDialog = false;
    _timer?.cancel();
    _timeStream?.close();
  });
}
