import 'dart:convert';
import 'package:bridgelinx_driver/DTO/profile_info.dart';
import 'package:flutter/cupertino.dart';

class Client {
  String clientId;
  String currentJob;
  bool isVerified;
  List<String> jobHistory;
  List<String> jobSched;
  List<String> addresses;

  Profile profileInfo;

  Client(
      {@required this.clientId,
      @required this.profileInfo,
      this.currentJob,
      this.isVerified: false,
      this.jobHistory,
      this.jobSched,
      this.addresses}); 

  Map<String, dynamic> toJson() => {
        'clientId': this.clientId,
        'currentJob': this.currentJob,
        'isVerified': this.isVerified.toString().toLowerCase(),
        'jobHistory': this.jobHistory,
        'jobSched': this.jobSched,
        'addresses': this.addresses,
        'profileInfo': this.profileInfo.toJson().toString(),
      };

  Client.fromJson(Map<String, dynamic> jsonStr)
      : this.clientId = jsonStr['clientId'],
        this.currentJob = jsonStr['currentJob'],
        this.isVerified = jsonStr['isVerified'],
        this.jobHistory = jsonStr['jobHistory'].cast<String>(),
        this.jobSched = jsonStr['jobSched'].cast<String>(),
        this.addresses = jsonStr['addresses'].cast<String>(),
        this.profileInfo =
            Profile.fromJson(json.decode(jsonStr['profileInfo']));
}
