import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/stops.dart';

enum JourneyType { Pickup, Dropoff, Stop }

class Journey {
  String id;
  String fromRef;
  String toRef;
  String nextRef;
  String number;
  int eta;
  List<String> clientImages;
  Package packageInfo;
  JourneyType journeyType;
  Coordinates toCoordinates, fromCoordinates;
  ScheduledStops toStop, fromStop;

  Journey.fromJson(Map<String, dynamic> json)
      : this.id = json['_id'],
        this.fromRef = json['from'],
        this.toRef = json['to'],
        this.nextRef = json['next'],
        this.number = json['number'].toString();
}
