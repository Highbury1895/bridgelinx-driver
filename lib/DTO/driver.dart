import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'profile_info.dart';

enum DriverType { staticDriver, dynamic, fiction, corporate }

class Driver {
  Driver(
      {this.driverId,
      this.isBase,
      this.isOnline,
      this.profileInfo,
      this.currentJob,
      this.jobHistory,
      this.primaryPhoneNumber,
      this.createdAt,
      this.driverType,
      this.driverBlocked,
      this.cashInHand,
      this.cashLimit});

  String driverId;
  String currentJob;
  List<String> jobHistory;
  bool isOnline;
  bool isBase;
  String createdAt;

  String profileInfo;

  String vehicleId;
  DriverType driverType;
  String primaryPhoneNumber;

  Base baseLocation;
  String city;

  Profile profile;

  bool driverBlocked;
  bool canHandleCash;
  double cashInHand;
  double cashLimit;

  Map<String, dynamic> toJson() => {
        '_id': this.driverId,
        'currentJob': this.currentJob,
        'isOnline': this.isOnline ?? false,
        'isBase': this.isBase ?? true,
        'jobHistory': this.jobHistory,
        'profileInfo': this.profileInfo,
        'createdAt': this.createdAt,
        'vehicleId': this.vehicleId,
        'primaryPhoneNumber': this.primaryPhoneNumber,
        'driverType': this.driverType.toString().split('.').last,
        'baseLocationId': null,
        'driverBlocked': false,
        'canHandleCash': false,
        'cashInHand': '0',
        'cashLimit': '100',
        'city': null
      };

  Map<String, dynamic> toJsonSP() => {
        '_id': this.driverId,
        'currentJob': this.currentJob,
        'isOnline': this.isOnline,
        'isBase': this.isBase,
        'jobHistory': this.jobHistory,
        'profileInfo': this.profileInfo,
        'profile': this.profile?.toJson() ?? null,
        'createdAt': this.createdAt,
        'vehicleId': this.vehicleId,
        'primaryPhoneNumber': this.primaryPhoneNumber,
        'driverType': this.driverType.toString().split('.').last,
        'baseLocationId': this.baseLocation?.id ?? null,
        'baseLocation': this.baseLocation?.toJson() ?? null,
        'driverBlocked': this.driverBlocked,
        'canHandleCash': this.canHandleCash,
        'cashInHand': this.cashInHand,
        'cashLimit': this.cashLimit,
        'city': this.city,
      };

  Driver.fromJson(Map<String, dynamic> jsonStr)
      : this.driverId = jsonStr['_id'],
        this.currentJob = jsonStr['currentJob'],
        this.isOnline = jsonStr['isOnline'] ?? false,
        this.isBase = jsonStr['isBase'] ?? false,
        this.jobHistory = jsonStr['jobHistory'] == null
            ? List<String>.empty()
            : jsonStr['jobHistory'].cast<String>(),
        this.profileInfo = jsonStr['profileInfo'],
        this.profile = jsonStr['profile'] != null
            ? Profile.fromJson(jsonStr['profile'])
            : null,
        this.createdAt = jsonStr['createdAt'],
        this.vehicleId = jsonStr['vehicleId'] ?? null,
        this.primaryPhoneNumber = jsonStr['primaryPhoneNumber'] ?? null,
        this.driverType = DriverType.values.firstWhere(
            (element) =>
                jsonStr['driverType'].toString().toLowerCase() ==
                element.toString().toLowerCase().split('.').last,
            orElse: () => null),
        this.driverBlocked = jsonStr['driverBlocked'] ?? false,
        this.canHandleCash = jsonStr['canHandleCash'] ?? false,
        this.cashInHand = jsonStr['cashInHand'] != null
            ? double.parse(jsonStr['cashInHand'].toString())
            : null,
        this.cashLimit = jsonStr['cashLimit'] != null
            ? double.parse(jsonStr['cashLimit'].toString())
            : null,
        this.city = jsonStr['city'],
        this.baseLocation = jsonStr['baseLocation'] != null
            ? Base.fromJson(jsonStr['baseLocation'])
            : null;
}

class Vehicle {
  Vehicle(
      {@required this.vehicleId,
      @required this.make,
      @required this.model,
      @required this.licenseNo,
      @required this.type,
      this.color,
      this.driver});

  String vehicleId;
  String make;
  String model;
  String licenseNo;
  String type;
  String color;

  Driver driver;

  Map<String, dynamic> toJson() => {
        '_id': this.vehicleId,
        'make': this.make,
        'model': this.model,
        'licenseNo': this.licenseNo,
        'type': this.type,
        'color': this.color,
        'driver': json.encode(this.driver.toJson())
      };

  Vehicle.fromJson(Map<String, dynamic> jsonStr)
      : this.vehicleId = jsonStr['_id'],
        this.make = jsonStr['make'],
        this.model = jsonStr['model'],
        this.licenseNo = jsonStr['licenseNo'],
        this.type = jsonStr['type'],
        this.color = jsonStr['color'],
        this.driver = Driver.fromJson(json.decode(jsonStr['driver']));
}

class Base {
  String id;
  String lat;
  String lng;
  String name;
  String updatedAt;
  double distance;
  String city;

  Map<String, dynamic> toJson() => {
        '_id': this.id,
        'name': this.name,
        'coordinates': {'lat': this.lat, 'lng': this.lng},
        'updated_at': this.updatedAt,
        'city': this.city,
        'distance': this.distance
      };

  Base.fromJson(Map<String, dynamic> json)
      : this.id = json['_id'],
        this.name = json['name'],
        this.lat = json['coordinates']['lat'],
        this.lng = json['coordinates']['lng'],
        this.updatedAt = json['updated_at'],
        this.city = json['city'],
        this.distance = double.parse(json['distance']?.toString() ?? '0');
}
