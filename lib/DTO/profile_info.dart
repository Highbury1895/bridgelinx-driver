import 'package:flutter/cupertino.dart';

enum UserType { driver, client }

class Profile {
  Profile(
      {@required this.profileId,
      this.driverId,
      this.firstName,
      this.lastName,
      @required this.phoneNo,
      @required this.joiningDate,
      @required this.createdAt,
      this.userType,
      this.cnic,
      this.profileImage});

  Profile.driver(
      {@required this.profileId,
      this.driverId,
      this.firstName,
      this.lastName,
      @required this.phoneNo,
      @required this.joiningDate,
      @required this.createdAt,
      this.cnic,
      this.profileImage}) {
    this.userType = UserType.driver;
  }

  Profile.client(
      {this.driverId,
      @required this.profileId,
      this.firstName,
      this.lastName,
      @required this.phoneNo,
      @required this.joiningDate,
      @required this.createdAt,
      this.cnic,
      this.profileImage}) {
    this.userType = UserType.client;
  }

  String driverId;
  String profileId;
  String firstName;
  String lastName;
  String phoneNo;
  String cnic;
  String joiningDate;
  String createdAt;
  String profileImage;
  UserType userType;

  Map<String, dynamic> toJson() => {
        'driverId': this.driverId,
        'profileId': this.profileId,
        'firstName': this.firstName,
        'lastName': this.lastName,
        'phoneNumber': this.phoneNo,
        'cnic': this.cnic,
        'joiningDate': this.joiningDate,
        'createdAt': this.createdAt,
        'profileImage': this.profileImage,
        'userType': this.userType.toString().split('.').last
      };

  Profile.fromJson(Map<String, dynamic> json)
      : this.driverId = json['driverId'],
        this.profileId = json['profileId'],
        this.firstName = json['firstName'],
        this.lastName = json['lastName'],
        this.phoneNo = json['phoneNumber'],
        this.cnic = json['cnic'],
        this.joiningDate = json['joiningDate'],
        this.createdAt = json['createdAt'],
        this.profileImage = json['profileImage'],
        this.userType = UserType.values.firstWhere((element) =>
            json['userType'].toString().toLowerCase() ==
            element.toString().toLowerCase().split('.').last);
}
