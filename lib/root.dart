import 'package:bridgelinx_driver/cubit/login_cubit.dart';
import 'package:bridgelinx_driver/dashboard/dashboard_root.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:bridgelinx_driver/utils/loading.dart';
import 'package:bridgelinx_driver/login/code_page.dart';
import 'package:bridgelinx_driver/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:bridgelinx_driver/errors/error_page.dart';

class RootPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    loginCubit.checkLogin();
    return BlocConsumer<LoginCubit, LoginState>(
      cubit: loginCubit,
      builder: (context, state) {
        if (state is LoginInitial)
          return LoginPage();
        else if (state is LoginLoading)
          return LoadingPage();
        else if (state is LoginLoaded)
          return DashBoardRoot();
        else if (state is LoginNumber)
          return NumberPage();
        else if (state is LoginCode)
          return CodePage(number: state.number);
        else if (state is LoginError)
          return ErrorPage(
            job: null,
            title: state.title,
            desc: state.desc,
            imagePath: state.imagePath,
          );
        else
          return LoginPage();
      },
      listener: (BuildContext context, state) {
        // if (state is LoginInitial) loginCubit.checkLogin();
        if (state is LoginError)
          Fluttertoast.showToast(
              msg: state.desc,);
              // toastLength: Toast.LENGTH_SHORT,
              // gravity: ToastGravity.BOTTOM,
              // timeInSecForIosWeb: 1,
              // backgroundColor: Theme.of(context).primaryColor,
              // textColor: Colors.white,
              // fontSize: 16.0);
      },
    );
  }
}
