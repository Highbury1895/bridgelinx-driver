import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/DTO/profile_info.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class DriverRepo {
  final _firestore = FirebaseFirestore.instance;

  Future<Driver> getDriver(User firebaseUser) async {
    final Profile _profile = await _firestore
        .collection('profiles')
        .doc(firebaseUser.uid)
        .get()
        .then((value) async {
      if (value.exists) {
        if (value.data()['userType'].toString() ==
            UserType.driver.toString().split('.').last)
          return Profile.fromJson(value.data());
        else
          return null;
      } else {
        final profile = Profile.driver(
            profileId: firebaseUser.uid,
            phoneNo: firebaseUser.phoneNumber,
            joiningDate: Timestamp.now().millisecondsSinceEpoch.toString(),
            createdAt: Timestamp.now().millisecondsSinceEpoch.toString());

        Driver driver = Driver(
            profileInfo: profile.profileId,
            createdAt: profile.joiningDate,
            primaryPhoneNumber: profile.phoneNo,
            isOnline: true,
            isBase: true,
            driverType: DriverType.dynamic,
            driverBlocked: false,
            cashInHand: 0,
            cashLimit: 100);

        final result =
            await _firestore.collection('drivers').add({}).then((value) async {
          driver.driverId = value.id;
          await _firestore
              .collection('drivers')
              .doc(value.id)
              .set(driver.toJson());

          profile.driverId = value.id;

          final _profile = await _firestore
              .collection('profiles')
              .doc(firebaseUser.uid)
              .set(profile.toJson())
              .then((value) => profile);

          return _profile;
        });

        return result;
      }
    });

    if (_profile == null) return null;

    final result = await _firestore
        .collection('drivers')
        .doc(_profile.driverId)
        .get()
        .then((DocumentSnapshot documentSnapshot) async {
      if (documentSnapshot.exists) {
        final driver = Driver.fromJson(documentSnapshot.data());
        // print(driver.toJson());
        // print(_profile.toJson());
        return driver;
      } else {
        Driver driver = Driver(
            profileInfo: _profile.profileId,
            createdAt: _profile.joiningDate,
            primaryPhoneNumber: _profile.phoneNo,
            isOnline: true,
            isBase: true,
            driverType: DriverType.dynamic,
            driverBlocked: false,
            cashInHand: 0,
            cashLimit: 100);

        final value =
            await _firestore.collection('drivers').add({}).then((value) async {
          driver.driverId = value.id;
          await _firestore
              .collection('drivers')
              .doc(value.id)
              .set(driver.toJson());
          return driver;
        });

        _profile.driverId = value.driverId;
        _firestore
            .collection('profiles')
            .doc(driver.profileInfo)
            .update({'driverId': value.driverId});

        return driver;
      }
    });

    result.profile = _profile;
    return result;
  }

  Future<void> setDriverOnline(bool _bool) async {
    currentDriver.isOnline = _bool;
    await saveCurrentDriver(currentDriver);
    final result = await _firestore
        .collection('drivers')
        .doc(currentDriver.driverId)
        .update({'isOnline': _bool});
    return result;
  }

  Future<void> setFCMToken(String token) async {
    final result = await _firestore
        .collection('drivers')
        .doc(currentDriver.driverId)
        .update({'notifToken': token});
    return result;
  }

  Future<void> blockDriver(Driver driver, bool _bool) async {
    if (driver == null || driver.driverId == null) return null;

    currentDriver.driverBlocked = _bool;
    await saveCurrentDriver(currentDriver);
    final result = await _firestore
        .collection('drivers')
        .doc(driver.driverId)
        .update({'driverBlocked': _bool});
    return result;
  }

  Stream driverStream() {
    return _firestore
        .collection('drivers')
        .doc(currentDriver.driverId)
        .snapshots();
  }
}
