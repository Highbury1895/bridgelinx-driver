import 'dart:convert';
import 'dart:io';
import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:bridgelinx_driver/DTO/stops.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:bridgelinx_driver/utils/google_services.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;

class JobRepo {
  // URLs

  final String apiUrl = appFlavor == FlavorType.Prod
      ? 'https://asia-south1-bridgelinx-client.cloudfunctions.net'
      : 'https://asia-south1-bridgelinx-dev.cloudfunctions.net';

  // Instances

  final _firestore = FirebaseFirestore.instance;
  final _storage = FirebaseStorage.instance;
  final _database = FirebaseDatabase.instance;

  // Steams

  Stream jobStream(String docId) {
    return _firestore.collection('jobs').doc(docId).snapshots();
  }

  // Realtime Database
  Future<void> jobStaticCritical(bool flag, Timestamp timestamp, String jobId,
      String driverId, String driverPhone) async {
    jobNotifTime = null;

    if (!flag) {
      _database
          .reference()
          .child('jobStaticCritical')
          .child(jobId)
          .once()
          .then((DataSnapshot snapshot) async {
        if (snapshot.value == null) {
          return null;
        } else {
          return await _database
              .reference()
              .child('jobStaticCritical')
              .child(jobId)
              .child('job')
              .update({
            'jobId': jobId,
            'driverId': driverId,
            'phoneNumber': driverPhone,
            'flag': flag,
          });
        }
      });
    } else {
      setJobStatus(jobId, JobStatusTS(JobStatus.jobCriticalStatic, timestamp));

      return await _database
          .reference()
          .child('jobStaticCritical')
          .child(jobId)
          .child('job')
          .update({
        'jobId': jobId,
        'driverId': driverId,
        'phoneNumber': driverPhone,
        'flag': flag,
        'timestamp': timestamp.millisecondsSinceEpoch
      });
    }
  }

  Future<void> jobETA130(Job job, Driver driver, Timestamp timestamp) async {
    List<double> eta = [];
    List<int> times = [];

    eta.add(double.parse(job.times[journeys[0]]["estimated"].toString()));
    eta.add(double.parse(job.times[journeys[1]]["estimated"].toString()));

    times.add(DateTime.fromMillisecondsSinceEpoch(int.parse(job
            .driverStatusHistory
            .where((element) =>
                element['status'].toString().toLowerCase() ==
                DriverStatus.driverArrived
                    .toString()
                    .split('.')
                    .last
                    .toLowerCase())
            .first['timestamp']
            .toString()))
        .difference(DateTime.fromMillisecondsSinceEpoch(int.parse(job
            .driverStatusHistory
            .where((element) =>
                element['status'].toString().toLowerCase() ==
                DriverStatus.driverMovingToPickup
                    .toString()
                    .split('.')
                    .last
                    .toLowerCase())
            .first['timestamp']
            .toString())))
        .inSeconds);

    times.add(DateTime.fromMillisecondsSinceEpoch(int.parse(job
            .driverStatusHistory
            .where((element) =>
                element['status'].toString().toLowerCase() ==
                DriverStatus.driverReachedDropOff
                    .toString()
                    .split('.')
                    .last
                    .toLowerCase())
            .first['timestamp']
            .toString()))
        .difference(DateTime.fromMillisecondsSinceEpoch(int.parse(job
            .driverStatusHistory
            .where((element) =>
                element['status'].toString().toLowerCase() ==
                DriverStatus.driverMovingToDropOff
                    .toString()
                    .split('.')
                    .last
                    .toLowerCase())
            .first['timestamp']
            .toString())))
        .inSeconds);

    if (times[0] > (eta[0] * 1.3))
      _database
          .reference()
          .child('jobETA130')
          .child(job.jobId)
          .child(journeys[0])
          .update({
        'jobId': job.jobId,
        'driverId': driver.driverId,
        'phoneNumber': driver.primaryPhoneNumber,
        'journey': journeys[0],
        'timestamp': timestamp.millisecondsSinceEpoch
      });

    if (times[1] > (eta[1] * 1.3))
      _database
          .reference()
          .child('jobETA130')
          .child(job.jobId)
          .child(journeys[1])
          .update({
        'jobId': job.jobId,
        'driverId': driver.driverId,
        'phoneNumber': driver.primaryPhoneNumber,
        'journey': journeys[1],
        'timestamp': timestamp.millisecondsSinceEpoch
      });
  }

  Future<void> jobETA130Journey(
      Job job, Journey journey, Driver driver, Timestamp timestamp) {
    return _database
        .reference()
        .child('jobETA130')
        .child(job.jobId)
        .child(journey.id)
        .once()
        .then((DataSnapshot snapshot) async {
      if (snapshot.value != null)
        return null;
      else
        return await _database
            .reference()
            .child('jobETA130')
            .child(job.jobId)
            .child(journey.id)
            .update({
          'jobId': job.jobId,
          'driverId': driver.driverId,
          'phoneNumber': driver.primaryPhoneNumber,
          'journey': journey.id,
          'timestamp': timestamp.millisecondsSinceEpoch,
          'flag': true
        });
    });
  }
  // Realtime Database End

  // Job Flow
  Future<void> reachedBase(Job job) async {
    if (currentDriver == null) return;

    currentDriver.isBase = true;
    await saveCurrentDriver(currentDriver);

    final result = await _firestore
        .collection('drivers')
        .doc(currentDriver.driverId)
        .update({
      'isBase': true,
      // 'jobHistory': FieldValue.arrayUnion([job.jobId])
    });
    return result;
  }

  Future<void> leftBase() async {
    if (currentDriver == null) return;
    currentDriver.isBase = false;
    await saveCurrentDriver(currentDriver);
    final result = await _firestore
        .collection('drivers')
        .doc(currentDriver.driverId)
        .update({'isBase': false});
    return result;
  }

  Future<void> setBase(String baseLocationId) async {
    if (currentDriver == null) return;
    final result = await _firestore
        .collection('drivers')
        .doc(currentDriver.driverId)
        .update({'baseLocationId': baseLocationId});
    return result;
  }

  // Getters

  Future<Base> getBase() async {
    List<Base> bases = List();
    Base selectedBase;
    final currentPos = await getGeoCurrentLocation(null);

    QuerySnapshot querySnapshot =
        await _firestore.collection("base_locations").get();
    bases.addAll(querySnapshot.docs.map((e) {
      final base = Base.fromJson(e.data());
      base.distance = Geolocator.distanceBetween(currentPos.latitude,
          currentPos.longitude, double.parse(base.lat), double.parse(base.lng));
      return base;
    }).toList());

    bases.sort((a, b) => a.distance.compareTo(b.distance));
    selectedBase = bases.first;

    // Set Base in DB
    setBase(selectedBase.id);
    currentDriver.baseLocation = selectedBase;
    await saveCurrentDriver(currentDriver);

    return selectedBase;
  }

  Future<Job> getCurrentJob(String jobId) async {
    final job =
        await _firestore.collection('jobs').doc(jobId).get().then((value) {
      if (value == null || value.data() == null) return null;
      if (value.data()['createdBy'] == 'web')
        return Job.fromJsonJourney(value.data());
      else if (value.data()['createdBy'] == 'mobile')
        return value.data()['clientAppVersion'] != null &&
                int.parse(value.data()['clientAppVersion'].toString()) >=
                    journeyVersion
            ? Job.fromJsonJourney(value.data())
            : null;
    });

    await saveCurrentJob(job);

    // getDistanceMatrix(job);
    // getLoadingUnloadingTimes(job);
    // Price price = await getPrice(job);
    // print(price.toJson());

    return job != null &&
            (job.driverId == null || job.driverId == currentDriver.driverId)
        ? job
        : null;
  }

  Future<Journey> getJourney(
      Job job, String journeyId, Coordinates coordinates) async {
    Map<String, dynamic> journeyMap = job.journey[journeyId];
    Journey _journey = Journey.fromJson(journeyMap);

    String _from = _journey.fromRef;
    String _dest = _journey.toRef;

    _journey.packageInfo = job.packageInfoJourney[_dest] != null
        ? Package.fromJson(job.packageInfoJourney[_dest])
        : null;
    _journey.clientImages =
        List.from(job.clientImagesJourney[_dest] ?? List.empty());

    if (job.clientCoordinatesJourney.pickup.containsKey(_dest)) {
      _journey.journeyType = JourneyType.Pickup;
      _journey.toCoordinates =
          Coordinates.fromJson(job.clientCoordinatesJourney.pickup[_dest]);
    } else if (job.clientCoordinatesJourney.dropoff.containsKey(_dest)) {
      _journey.journeyType = JourneyType.Dropoff;
      _journey.toCoordinates =
          Coordinates.fromJson(job.clientCoordinatesJourney.dropoff[_dest]);
    } else if (job.scheduledStops.containsKey(_dest)) {
      _journey.journeyType = JourneyType.Stop;
      _journey.toStop = ScheduledStops.fromJson(job.scheduledStops[_dest]);
    }

    if (_from != null) {
      if (job.clientCoordinatesJourney.pickup.containsKey(_from))
        _journey.fromCoordinates =
            Coordinates.fromJson(job.clientCoordinatesJourney.pickup[_from]);
      else if (job.clientCoordinatesJourney.dropoff.containsKey(_from))
        _journey.fromCoordinates =
            Coordinates.fromJson(job.clientCoordinatesJourney.dropoff[_from]);
      else if (job.scheduledStops.containsKey(_from))
        _journey.fromStop = ScheduledStops.fromJson(job.scheduledStops[_from]);
    }

    _journey.eta = int.parse(job.times[_journey.id]["estimated"].toString());
    currentJourney = _journey;
    return _journey;
  }

  // Setters

  Future<void> setCurrentJob(String jobId, histJob) async {
    if (currentDriver == null) return;
    final result = await _firestore
        .collection('drivers')
        .doc(currentDriver.driverId)
        .update({
      'currentJob': jobId,
      'updateBy': {
        'by': 'Driver App',
        'time': DateTime.now().millisecondsSinceEpoch.toString()
      },
      'jobHistory': histJob != null
          ? FieldValue.arrayUnion([histJob])
          : FieldValue.arrayUnion([])
    });
    return result;
  }

  Future<void> cancelJob(String jobId, String driverId) async {
    WriteBatch batch = _firestore.batch();

    final snapshot = await _firestore.collection('drivers').doc(driverId).get();
    if (snapshot.data()['currentJob'] != null) {
      batch.update(
          _firestore.collection('drivers').doc(driverId), {'currentJob': null});

      batch.update(_firestore.collection('jobs').doc(jobId),
          {'driverId': null, 'assignedDriverType': null, 'vehicleId': null});

      batch.commit();
    }

    jobCubit.setCurrentJob(null, null);
  }

  Future<void> setJobTimes(
      String jobId, Timestamp startTimestamp, Timestamp endTimestamp) async {
    final result = await _firestore.collection('jobs').doc(jobId).update({
      'startTime': startTimestamp.millisecondsSinceEpoch.toString(),
      'endTime': endTimestamp.millisecondsSinceEpoch.toString(),
    });
    return result;
  }

  Future<void> setJobStatus(String jobId, JobStatusTS jobStatus) async {
    final result = await _firestore.collection('jobs').doc(jobId).update({
      'jobStatusHistory': FieldValue.arrayUnion([jobStatus.toJson()]),
      'jobStatus': jobStatus.toJson()
    });
    return result;
  }

  Future<void> setDriverStatus(String jobId, DriverStatusTS driverStatus,
      Map<String, dynamic> driverStatusHistory) async {
    final result = await _firestore.collection('jobs').doc(jobId).update({
      'driverStatusHistory':
          driverStatusHistory, //FieldValue.arrayUnion([driverStatus.toJson()]),
      'driverStatus': driverStatus.toJson()
    });

    return result;
  }

  Future<void> setDriverJobStatus(String jobId, DriverStatusTS driverStatus,
      JobStatusTS jobStatus, Map<String, dynamic> driverStatusHistory) async {
    WriteBatch batch = _firestore.batch();

    batch.update(_firestore.collection('jobs').doc(jobId), {
      'driverStatusHistory':
          driverStatusHistory, //FieldValue.arrayUnion([driverStatus.toJson()]),
      'jobStatusHistory': FieldValue.arrayUnion([jobStatus.toJson()]),
      'driverStatus': driverStatus.toJson(),
      'jobStatus': jobStatus.toJson(),
    });

    final result = batch.commit();

    return result;
  }

  // Update

  Future<void> updateJobCo(String jobId, Coordinates coordinates) async {
    currentCoordinates = coordinates;
    final result = await _firestore
        .collection('jobs')
        .doc(jobId)
        .update({'driverCurrentCoordinates': coordinates.toJson()});
    return result;
  }

  Future<void> updateDriverCo(String driverId, Coordinates coordinates) async {
    currentCoordinates = coordinates;
    final result = await _firestore
        .collection('drivers')
        .doc(driverId)
        .update({'currentLocation': coordinates.toJson()});
    return result;
  }

  Future<void> updateCoordinates(
      String jobId, String driverId, Coordinates coordinates) async {
    currentCoordinates = coordinates;
    WriteBatch batch = _firestore.batch();

    batch.update(_firestore.collection('drivers').doc(driverId),
        {'currentLocation': coordinates.toJson()});

    batch.update(_firestore.collection('jobs').doc(jobId),
        {'driverCurrentCoordinates': coordinates.toJson()});

    batch.commit();
  }

  Future<void> updateJobPrice(String jobId, Price price) async {
    final result = await _firestore
        .collection('jobs')
        .doc(jobId)
        .update({'priceInfo': price.toJson()});
    return result;
  }

  Future<void> updateCashInHand(double amount) async {
    if (currentDriver == null) return;
    currentDriver.cashInHand = amount;
    await saveCurrentDriver(currentDriver);
    final result = await _firestore
        .collection('drivers')
        .doc(currentDriver.driverId)
        .update({'cashInHand': amount.toInt().toString()});
    return result;
  }

  Future<void> updateJobLoaders(String jobId, Map<String, dynamic> loaders,
      Map<String, dynamic> unloaders) async {
    final result = await _firestore
        .collection('jobs')
        .doc(jobId)
        .update({'numberOfLoaders': loaders, 'numberOfUnloaders': unloaders});
    return result;
  }

  Future<void> updateDistances(Job job) async {
    final result = await _firestore
        .collection('jobs')
        .doc(job.jobId)
        .update({'distances': job.distances});
    return result;
  }

  Future<void> updateTimes(Job job) async {
    final result = await _firestore
        .collection('jobs')
        .doc(job.jobId)
        .update({'times': job.times});
    return result;
  }

  Future<void> updateDistancesAndTimes(Job job) async {
    final result = await _firestore
        .collection('jobs')
        .doc(job.jobId)
        .update({'times': job.times, "distances": job.distances});
    return result;
  }

  // Stops

  Job updateStops(DriverStop stop, Job job, bool end) {
    //Update Realtime

    if (!end) {
      String key = _database.reference().child(stop.jobId).push().key;
      stop.id = key;
    } else {
      job.currentStop = null;
    }

    _database
        .reference()
        .child('jobUnsheduledStops')
        .child(stop.jobId)
        .child(stop.id)
        .update(stop.toJson());

    // Update Firestore

    if (job.unscheduledStops != null &&
        job.unscheduledStops.containsKey(stop.journey)) {
      if (List.from(job.unscheduledStops[stop.journey]).firstWhere(
              (e) => DriverStop.fromJson(e).id == stop.id,
              orElse: () => null) !=
          null) {
        final list = List.from(job.unscheduledStops[stop.journey]);
        list.removeWhere((e) => DriverStop.fromJson(e).id == stop.id);
        job.unscheduledStops[stop.journey] = list;
      }
      job.unscheduledStops[stop.journey].add(stop.toJson());
    } else {
      job.unscheduledStops[stop.journey] = List.from([stop.toJson()]);
    }

    _firestore.collection('jobs').doc(job.jobId).update({
      'currentStop': !end ? stop.toJson() : null,
      'unscheduledStops': job.unscheduledStops
    });

    saveCurrentJob(job);
    return job;
  }

  Future<void> updateStopTime(Job job) async {
    final result = await _firestore
        .collection('jobs')
        .doc(job.jobId)
        .update({'stops': job.scheduledStops});
    return result;
  }

  // Calculation

  Future<Job> setTotalDistance(Job job, String key) async {
    double distance = 0;
    Coordinates lastCo;

    List<Coordinates> distanceTravelled = await getDistances();

    distanceTravelled != null && distanceTravelled.isNotEmpty
        ? distanceTravelled.asMap().forEach((index, val) {
            if (index <= 0) {
              lastCo = val;
            } else {
              double dist = Geolocator.distanceBetween(
                  lastCo.lat, lastCo.lng, val.lat, val.lng);
              if (dist >= 40) {
                distance += dist;
                lastCo = val;
              }
            }
          })
        : DoNothingAction();

    job.distances[key]["actual"] = distance.toInt();
    job.distances['total']['actual'] =
        int.parse(job.distances['total']['actual']?.toString() ?? '0') +
            distance.toInt();
    await saveCurrentJob(job);
    clearDistances();
    updateDistances(job);
    return job;
  }

  // Images

  Future<Job> uploadPic(
      Job job, Journey journey, String imagePath, String imageName) async {
    //Get the file from the image picker and store it
    File image = File(imagePath);
    String baseName = basename(image.path);

    final lastIndex = imagePath.lastIndexOf(new RegExp(r'.jp'));
    final splitted = imagePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${imagePath.substring(lastIndex)}";

    final compressedImage = await FlutterImageCompress.compressAndGetFile(
        imagePath, outPath,
        quality: 70);

    //Create a reference to the location you want to upload to in firebase
    StorageReference reference = _storage
        .ref()
        .child("public/images/jobs/${job.jobId}/driver/$baseName");

    //Upload the file to firebase
    StorageUploadTask uploadTask = reference.putFile(compressedImage);

    StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

    // Waits till the file is uploaded then stores the download url
    String url = await taskSnapshot.ref.getDownloadURL();

    // job.driverImages.add(url);
    if (job.driverImagesJourney == null)
      job.driverImagesJourney = {
        journey.id: Map.from({imageName: url})
      };
    else if (job.driverImagesJourney.containsKey(journey.id)) {
      job.driverImagesJourney[journey.id][imageName] = url;
    } else {
      job.driverImagesJourney[journey.id] = Map.from({imageName: url});
    }

    if (baseName.toLowerCase().contains(jobImageLabels[2])) {
      // job.clientImages.add(url);
      saveGatepass(url);
    }

    await saveCurrentJob(job);

    _firestore
        .collection('jobs')
        .doc(job.jobId)
        .update({'driverImages': job.driverImagesJourney});

    return job;
  }

  // Cloud Functions

  Future<Job> getDistanceMatrix(Job job) async {
    final googleMapsServices = GoogleMapsServices();

    List<Journey> journeyList = List.from([]);
    job.journey.values.forEach((e) => journeyList.add(Journey.fromJson(e)));

    journeyList
        .sort((a, b) => int.parse(a.number).compareTo(int.parse(b.number)));

    List<String> toRefList = List.from([]);
    journeyList.forEach((e) => toRefList.add(e.toRef));

    List<Coordinates> coordinatesList = List.from([]);
    if (currentCoordinates == null) {
      final _pos = await getGeoCurrentLocation(null);
      currentCoordinates = Coordinates(lat: _pos.latitude, lng: _pos.longitude);
    }
    coordinatesList.add(currentCoordinates);
    toRefList.forEach((e) {
      if (job.clientCoordinatesJourney.pickup.containsKey(e))
        coordinatesList
            .add(Coordinates.fromJson(job.clientCoordinatesJourney.pickup[e]));
      else if (job.clientCoordinatesJourney.dropoff.containsKey(e))
        coordinatesList
            .add(Coordinates.fromJson(job.clientCoordinatesJourney.dropoff[e]));
      else if (job.scheduledStops.containsKey(e))
        coordinatesList.add(
            Coordinates.fromJsonOnly(job.scheduledStops[e]['coordinates']));
    });

    final map = await googleMapsServices.getDistanceMatrix(
        coordinatesList, journeyList);
    job.distances = map["distances"];
    job.times = map["times"];

    updateDistancesAndTimes(job);

    return job;
  }

  Future<Job> getDistanceMatrixSep(Job job) async {
    List<Journey> journeyList = List.from([]);
    job.journey.values.forEach((e) => journeyList.add(Journey.fromJson(e)));

    Map<String, dynamic> journeyMapDistance = {
      "total": {"estimated": 0, "actual": 0}
    };
    Map<String, dynamic> journeyMapTime = {
      "total": {"estimated": 0, "actual": 0}
    };
    for (Journey j in journeyList) {
      journeyMapDistance[j.id] = {"estimated": 0, "actual": 0, "remaining": 0};
      journeyMapTime[j.id] = {"estimated": 0, "actual": 0, "remaining": 0};
    }

    Map<String, dynamic> timesMap = journeyMapTime;
    Map<String, dynamic> distanceMap = journeyMapDistance;

    int distance = 0;
    int time = 0;

    List<Map<String, dynamic>> list =
        await Future.wait(journeyList.map((journey) async {
      String toRef, fromRef;
      Coordinates to, from;

      toRef = journey.toRef;
      fromRef = journey.fromRef;

      if (toRef == null) {
        if (currentCoordinates == null) {
          final _pos = await getGeoCurrentLocation(null);
          currentCoordinates =
              Coordinates(lat: _pos.latitude, lng: _pos.longitude);
        }
        to = currentCoordinates;
      } else if (job.clientCoordinatesJourney.pickup.containsKey(toRef))
        to = Coordinates.fromJson(job.clientCoordinatesJourney.pickup[toRef]);
      else if (job.clientCoordinatesJourney.dropoff.containsKey(toRef))
        to = Coordinates.fromJson(job.clientCoordinatesJourney.dropoff[toRef]);
      else if (job.scheduledStops.containsKey(toRef))
        to = Coordinates.fromJsonOnly(job.scheduledStops[toRef]['coordinates']);

      if (fromRef == null) {
        if (currentCoordinates == null) {
          final _pos = await getGeoCurrentLocation(null);
          currentCoordinates =
              Coordinates(lat: _pos.latitude, lng: _pos.longitude);
        }
        from = currentCoordinates;
      } else if (job.clientCoordinatesJourney.pickup.containsKey(fromRef))
        from =
            Coordinates.fromJson(job.clientCoordinatesJourney.pickup[fromRef]);
      else if (job.clientCoordinatesJourney.dropoff.containsKey(fromRef))
        from =
            Coordinates.fromJson(job.clientCoordinatesJourney.dropoff[fromRef]);
      else if (job.scheduledStops.containsKey(fromRef))
        from = Coordinates.fromJsonOnly(
            job.scheduledStops[fromRef]['coordinates']);

      final url =
          'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${from.lat.toString()},${from.lng.toString()}&destinations=${to.lat.toString()},${to.lng.toString()}&key=AIzaSyCQvfbYZc9HKiJj6CLnA2PTn2j-WG7Ryn8';
      http.Response response = await http.get(url);
      Map values = jsonDecode(response.body);
      int time = (values["rows"][0]["elements"][0]["duration"]["value"]);
      int distance = (values["rows"][0]["elements"][0]["distance"]["value"]);

      return {"journey": journey.id, "time": time, "distance": distance};
    }));

    for (Map<String, dynamic> map in list) {
      String j = map['journey'];
      int t = map['time'];
      int d = map['distance'];

      time += t;
      timesMap[j]["estimated"] = t;
      timesMap["total"]["estimated"] = time;

      distance += d;
      distanceMap[j]["estimated"] = d;
      distanceMap["total"]["estimated"] = distance;
    }

    job.distances = distanceMap;
    job.times = timesMap;

    updateDistancesAndTimes(job);

    return job;
  }

  Future<int> getJobDistance(Job job) async {
    final googleMapsServices = GoogleMapsServices();

    List<Journey> journeyList = List.from([]);
    job.journey.values.forEach((e) => journeyList.add(Journey.fromJson(e)));

    // print('Journey Length ${job.journey.values.first}');
    // print('JourneyList Length: ${journeyList.length}');

    journeyList
        .sort((a, b) => int.parse(a.number).compareTo(int.parse(b.number)));

    // journeyList.forEach((e) => print(e.id));

    List<String> toRefList = List.from([]);
    journeyList.forEach((e) => toRefList.add(e.toRef));

    // print('ToRef Length: ${toRefList.length}');

    List<Coordinates> coordinatesList = List.from([]);
    toRefList.forEach((e) {
      if (job.clientCoordinatesJourney.pickup.containsKey(e))
        coordinatesList
            .add(Coordinates.fromJson(job.clientCoordinatesJourney.pickup[e]));
      else if (job.clientCoordinatesJourney.dropoff.containsKey(e))
        coordinatesList
            .add(Coordinates.fromJson(job.clientCoordinatesJourney.dropoff[e]));
    });

    // print('Coordinates Length: ${coordinatesList.length}');

    final distance = await googleMapsServices.getRouteDistance(coordinatesList);
    return distance;
  }

  Map<String, dynamic> getLoadingUnloadingTimes(Job job) {
    // int loadingTime = 0;
    // int unloadingTime = 0;

    Map<String, dynamic> loadingMap = {};
    Map<String, dynamic> unloadingMap = {};

    job.driverStatusHistoryJourney.forEach((k, v) {
      String toId = job.journey[k]['to'];
      List driverStatusHistory = List<Map>.from(v);

      if (driverStatusHistory.singleWhere(
              (it) =>
                  it['status'].toString().toLowerCase() ==
                  DriverStatus.driverLoading
                      .toString()
                      .toLowerCase()
                      .split('.')
                      .last,
              orElse: () => null) !=
          null) {
        int start = int.parse(driverStatusHistory
            .firstWhere((element) =>
                element['status'].toString().toLowerCase() ==
                DriverStatus.driverLoading
                    .toString()
                    .toLowerCase()
                    .split('.')
                    .last)['timestamp']
            .toString());
        int end = int.parse(driverStatusHistory
            .firstWhere((element) =>
                element['status'].toString().toLowerCase() ==
                DriverStatus.driverLoadingCompleted
                    .toString()
                    .toLowerCase()
                    .split('.')
                    .last)['timestamp']
            .toString());

        loadingMap[toId] = {'start': start, 'end': end};

        // loadingTime += DateTime.fromMillisecondsSinceEpoch(end)
        //     .difference(DateTime.fromMillisecondsSinceEpoch(start))
        //     .inSeconds;

      } else if (driverStatusHistory.singleWhere(
              (it) =>
                  it['status'].toString().toLowerCase() ==
                  DriverStatus.driverUnloading
                      .toString()
                      .toLowerCase()
                      .split('.')
                      .last,
              orElse: () => null) !=
          null) {
        int start = int.parse(driverStatusHistory
            .firstWhere((element) =>
                element['status'].toString().toLowerCase() ==
                DriverStatus.driverUnloading
                    .toString()
                    .toLowerCase()
                    .split('.')
                    .last)['timestamp']
            .toString());
        int end = int.parse(driverStatusHistory
            .firstWhere((element) =>
                element['status'].toString().toLowerCase() ==
                DriverStatus.driverUnloadingCompleted
                    .toString()
                    .toLowerCase()
                    .split('.')
                    .last)['timestamp']
            .toString());

        unloadingMap[toId] = {'start': start, 'end': end};
      }
    });

    /*
    Map<String, dynamic> times = {
      'loading': {
        'start': DateTime.now().millisecondsSinceEpoch,
        'end': DateTime.now()
            .add(Duration(seconds: loadingTime))
            .millisecondsSinceEpoch
      },
      'unloading': {
        'start': DateTime.now().millisecondsSinceEpoch,
        'end': DateTime.now()
            .add(Duration(seconds: unloadingTime))
            .millisecondsSinceEpoch
      },
    };
    */

    Map<String, dynamic> times = {
      'loading': loadingMap,
      'unloading': unloadingMap,
    };

    return times;
  }

  Future<Price> getPrice(Job job) async {
    final distance = int.parse(job.distances["total"]["estimated"].toString());
    final times = getLoadingUnloadingTimes(job);

    Map<String, dynamic> body = {
      'distance': distance / 1000,
      'loadingTime': times['loading'],
      'unloadingTime': times['unloading'],
      'vehicleType': job.vehicleType.toString().split('.').last,
    };

    var result = await http.post(
      '$apiUrl/pricingAlgo',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(body),
    );

    Price finalPrice = Price.fromCloud(json.decode(result.body));

    return finalPrice;
  }

  Future<String> applyForJob(Job job) async {
    Map<String, dynamic> body = {
      'jobId': job.jobId,
      'driverId': currentDriver.driverId,
      'driverType': currentDriver.driverType.toString().split('.').last,
      'vehicleId': currentDriver.vehicleId
    };

    var result = await http.post(
      '$apiUrl/firestoreTransaction',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(body),
    );

    // Fluttertoast.showToast(msg: json.decode(result.body)['message']);
    String _jobId = json.decode(result.body)['jobId'];

    return _jobId;
  }

  void sendFCM(Job job, String title, String notBody) async {
    Map<String, dynamic> body = {
      'clientId': job.clientId,
      'message': {
        'notification': {
          'title':
              '#${NumberFormat('0000').format(double.parse(job.orderNumber.toString()))}-$title',
          'body': notBody
        },
        'data': {
          'jobId': job.jobId,
          'click_action': 'FLUTTER_NOTIFICATION_CLICK',
        },
      }
    };

    await http.post(
      '$apiUrl/sendFCM',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(body),
    );
  }

  Future<Job> updateJourey(Job job) async {
    Map<String, dynamic> body = {
      'jobId': job.jobId,
    };

    var result = await http.post(
      '$apiUrl/journeyAlgoAPI',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(body),
    );

    return Job.fromJsonJourney(jsonDecode(result.body));
  }

  /*
  Future<void> applyForJob(String jobId) async {

    final _job =
        await _firestore.collection('jobs').doc(jobId).get();

    try {
      // Return and set the updated "likes" count from the transaction
      String _jobId = await FirebaseFirestore.instance
          .runTransaction<String>((transaction) async {
        DocumentSnapshot txSnapshot = await transaction.get(_firestore.collection('jobs').doc(jobId));
        if (!txSnapshot.exists) {
          throw Exception("Document does not exist!");
        }
        int updatedLikes = (txSnapshot.data()['likes'] ?? 0) + 1;
        // transaction.update(widget.reference, {'likes': updatedLikes});
        // return updatedLikes;
      });
    } catch (e) {
      print("Failed to update likes for document! $e");

    }
  }
  */

  Future<void> createJob() async {
    final result = await _firestore.collection('jobs').add({}).then((value) {
      print(value.id);
      // Replace {} with Job JSON
      Map<String, dynamic> _job = {
        "clientImages": {
          "pickup-707ae9bd-5adf-4ec8-b4ac-4fe145432d09": [
            "https://firebasestorage.googleapis.com/v0/b/bridgelinx-dev.appspot.com/o/public%2Fimages%2Fjobs%2F1jomo9zI9UJI0mEKsi3L%2Fdriver%2Fpickup1604509967718.jpg?alt=media&token=55126a88-2b2c-4d32-bb4e-731c4eadb83a",
            "https://firebasestorage.googleapis.com/v0/b/bridgelinx-dev.appspot.com/o/public%2Fimages%2Fjobs%2F1jomo9zI9UJI0mEKsi3L%2Fdriver%2Floading1604509994850.jpg?alt=media&token=4f9f549c-e444-4c9d-b610-5f3ffee22e8e",
            "https://firebasestorage.googleapis.com/v0/b/bridgelinx-dev.appspot.com/o/public%2Fimages%2Fjobs%2F1jomo9zI9UJI0mEKsi3L%2Fdriver%2Fpickup_gatepass1604510003782.jpg?alt=media&token=6bcb2abb-73cf-40e1-9cf0-19d394bfe576"
          ],
        },
        "jobStatus": {"timestamp": 1608023689522, "status": "jobInit"},
        "driverImages": null,
        "driverCurrentCoordinates": null,
        "orderNumber": 0,
        "vehicleSubtype": "open",
        "driverStatus": {
          "timestamp": 1608023683759,
          "status": "driverInit",
          "journey": null,
          "coordinates": null
        },
        "numberOfUnloaders": null,
        "cashForGoods": 0,
        "createdAt": 1608023683779,
        "priceInfo": {
          "surcharge": null,
          "flatRate": 0.0,
          "cashCollected": 0.0,
          "finalAmount": null,
          "discount": null,
          "variableRate": null,
          "estimatedAmount": 1506.0,
          "paymentStatus": "unpaid",
          "paymentType": "dropoff-4df7402c-26e4-483d-9b01-a89fb44da902"
        },
        "pickupTime": 1608026412222,
        "businessCode": null,
        "startTime": 1608022812222,
        "vehicleId": null,
        "vehicleType": "medium",
        "numberOfLoaders": null,
        "clientId": "eDoaX68X1j0BjAn87855",
        "biggerTruckSent": false,
        "driverStatusHistory": null,
        "clientCoordinates": {
          "dropOff": {
            "dropoff-4df7402c-26e4-483d-9b01-a89fb44da902": {
              "_id": "dropoff-4df7402c-26e4-483d-9b01-a89fb44da902",
              "address": "",
              "formattedAddress":
                  "CC Masjid, Lahore, Dha Phase 4, Sector CC, Punjab, Lahore",
              "contactName": "hii",
              "coordinates": {
                "lng": 74.37935382127762,
                "lat": 31.466216057188767
              },
              "addressSpec": "",
              "contactPhone": "+923219339669",
              "timestamp": 1608023683778,
              "type": "dropoff",
              "number": 3
            },
            "dropoff-uuid": {
              "_id": "dropoff-uuid",
              "address": "",
              "formattedAddress":
                  "CC Masjid, Lahore, Dha Phase 4, Sector CC, Punjab, Lahore",
              "contactName": "hii",
              "coordinates": {
                "lng": 74.37935382127762,
                "lat": 31.466216057188767
              },
              "addressSpec": "",
              "contactPhone": "+923219339669",
              "timestamp": 1608023683778,
              "type": "dropoff",
              "number": 2
            },
          },
          "pickUp": {
            "pickup-707ae9bd-5adf-4ec8-b4ac-4fe145432d09": {
              "_id": "pickup-707ae9bd-5adf-4ec8-b4ac-4fe145432d09",
              "address": "",
              "formattedAddress":
                  "5, Lahore, Gulberg III, Block E 2, Punjab, Lahore",
              "contactName": "Hamza  Malik",
              "addressSpec": "",
              "coordinates": {"lng": 74.339732, "lat": 31.509332},
              "contactPhone": "+923224894276",
              "timestamp": 1608023683778,
              "type": "pickup",
              "number": 1
            }
          }
        },
        "driverId": null,
        "jobStatusHistory": [
          {"status": "jobScheduledStatic", "timestamp": 1608023689522}
        ],
        "orderModifyProcessed": null,
        "_id": value.id,
        "endTime": 1608040812222,
        "orderModifyRequested": null,
        "packageInfo": {
          "pickup-707ae9bd-5adf-4ec8-b4ac-4fe145432d09": {
            "noOfUnits": 500,
            "freightDetails": "hii",
            "typeOfFreight": "boxes",
            "customFreightType": "",
            "freightWeightUnit": "kg",
            "freightWeight": 50
          },
          "dropoff-4df7402c-26e4-483d-9b01-a89fb44da902": {
            "noOfUnits": 500,
            "freightDetails": "hii",
            "typeOfFreight": "boxes",
            "customFreightType": "",
            "freightWeightUnit": "kg",
            "freightWeight": 50
          },
          "dropoff-uuid": {
            "noOfUnits": 500,
            "freightDetails": "hii",
            "typeOfFreight": "boxes",
            "customFreightType": "",
            "freightWeightUnit": "kg",
            "freightWeight": 50
          },
        },
        "assignedDriverType": null,
        "jobType": "intraCity",
        "jobCash": 15000,
        "clientAppVersion": 34,
        "createdBy": "mobile",
        "journey": null,
        "stops": {
          "stop-uuid": {
            "_id": "stop-uuid",
            "address": "Apna office",
            "formattedAddress":
                "Block M, Block M Commercial Area Model Town, Lahore, Punjab, Pakistan",
            "coordinates": {
              "lat": 31.482198003185683,
              "lng": 74.3102552681429,
            },
            "reachTime": {
              "estimated": Timestamp.now().millisecondsSinceEpoch,
              "actual": null,
            },
            "leaveTime": {
              "estimated": Timestamp.fromDate(
                      Timestamp.now().toDate().add(Duration(hours: 5)))
                  .millisecondsSinceEpoch,
              "actual": null,
            },
            "stopOverDuration": {
              "estimated": 18000, // in seconds
              "actual": null, // in seconds
            },
            "number": 1,
            "type": "stop"
          }
        },
        /*
        {
          "start": "journey-39b90e92-d7d6-41c1-9366-15342e0504fa",
          "flow": {
            "journey-39b90e92-d7d6-41c1-9366-15342e0504fa": {
              "_id": "journey-39b90e92-d7d6-41c1-9366-15342e0504fa",
              "to": "pickup-707ae9bd-5adf-4ec8-b4ac-4fe145432d09",
              "from": null,
              "next": "journey-fdd328ae-3ac8-4ecf-a7ab-4a5497cd1097",
              "number": "1"
            },
            "journey-fdd328ae-3ac8-4ecf-a7ab-4a5497cd1097": {
              "_id": "journey-fdd328ae-3ac8-4ecf-a7ab-4a5497cd1097",
              "to": "dropoff-f5c44a26-02ff-4455-acd8-cd0801bc6c2b",
              "from": "pickup-707ae9bd-5adf-4ec8-b4ac-4fe145432d09",
              "next": "journey-87f64797-9ca5-43ff-8355-87c1418603f3",
              "number": "2"
            },
            "journey-87f64797-9ca5-43ff-8355-87c1418603f3": {
              "_id": "journey-87f64797-9ca5-43ff-8355-87c1418603f3",
              "to": "dropoff-4df7402c-26e4-483d-9b01-a89fb44da902",
              "from": "dropoff-f5c44a26-02ff-4455-acd8-cd0801bc6c2b",
              "next": "journey-7bbc5bd4-1cb7-43e4-b5c6-ff7a09592bad",
              "number": "3"
            },
          }
        }
        */
      };
      return _firestore.collection('jobs').doc(value.id).set(_job);
    });
    return result;
  }
}
