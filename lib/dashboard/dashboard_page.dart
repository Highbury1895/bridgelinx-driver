import 'dart:async';
import 'dart:convert';
import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_foreground_plugin/flutter_foreground_plugin.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:foreground_service/foreground_service.dart';
import 'package:geolocator/geolocator.dart';
import '../utils/version_check.dart';
import '../utils/job_dialog.dart';
import '../utils/globals.dart';

class MyAppBar extends PreferredSize {
  final Function call;

  MyAppBar({this.call});

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    bool isOn = currentDriver.isOnline;

    return AppBar(
        centerTitle: true,
        title: currentDriver.driverType == DriverType.dynamic
            ? StatefulBuilder(
                builder: (BuildContext context, StateSetter setSheetState) =>
                    Row(
                      children: [
                        Text(
                          isOn ? 'online'.tr() : 'offline'.tr(),
                          style: Theme.of(context)
                              .accentTextTheme
                              .headline1
                              .copyWith(color: Colors.white),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Switch(
                            value: isOn,
                            onChanged: (val) {
                              isOn = !isOn;
                              loginCubit.setDriverOnline(isOn);
                              setSheetState(() {
                                currentDriver.isOnline = isOn;
                              });
                            }),
                      ],
                    ))
            : Container(),
        actions: [
          IconButton(
              icon: Icon(FontAwesomeIcons.headset),
              onPressed: () {
                call();
              }),
        ]);
  }
}

void startForegroundService() async {
  await FlutterForegroundPlugin?.stopForegroundService();
  await FlutterForegroundPlugin?.setServiceMethodInterval(seconds: 10);
  await FlutterForegroundPlugin?.setServiceMethod(globalForegroundService);
  await FlutterForegroundPlugin?.startForegroundService(
    holdWakeLock: true,
    onStarted: () {
      // print("Foreground on Started");
    },
    onStopped: () {
      // print("Foreground on Stopped");
    },
    title: "BridgeLinx Driver",
    content: "Updating Coordinates",
    iconName: "ic_stat_hot_tub",
    subtext: "",
  );
}

void globalForegroundService() async {
  // Check Notif Time
  if (currentDriver.driverType != DriverType.dynamic && jobNotifTime != null) {
    final now = DateTime.now();
    int diff = now.difference(jobNotifTime).inSeconds;
    if (diff > (15 * 60)) {
      jobCubit.jobStaticCritical(
          true,
          Timestamp.fromDate(jobNotifTime),
          currentJob.jobId,
          currentDriver.driverId,
          currentDriver.primaryPhoneNumber);
    }
  }

  // Get Driver Current Location
  getGeoCurrentLocation(null).then((value) {
    if (value == null) return;

    // Send Driver Coordinates
    Coordinates driverCurrentCoordinates = Coordinates(
        lat: value.latitude,
        lng: value.longitude,
        timestamp: DateTime.now().toUtc().millisecondsSinceEpoch.toString());
    jobCubit.updateDriverCo(currentDriver.driverId, driverCurrentCoordinates);
  });
}

//use an async method so we can await
void _maybeStartFGS() async {
  ///if the app was killed+relaunched, this function will be executed again
  ///but if the foreground service stayed alive,
  ///this does not need to be re-done
  if (await ForegroundService.foregroundServiceIsStarted())
    await ForegroundService.stopForegroundService();

  await ForegroundService.setServiceIntervalSeconds(10);

  //necessity of editMode is dubious (see function comments)
  await ForegroundService.notification.startEditMode();

  await ForegroundService.notification.setTitle("BridgeLinx Driver is Running");
  await ForegroundService.notification.setText("Updating Driver Coordinates");

  await ForegroundService.notification.finishEditMode();

  await ForegroundService.setContinueRunningAfterAppKilled(false);

  await ForegroundService.startForegroundService(_foregroundServiceFunction);
  await ForegroundService.getWakeLock();

  ///this exists solely in the main app/isolate,
  ///so needs to be redone after every app kill+relaunch
  await ForegroundService.setupIsolateCommunication((data) async {
    // debugPrint("main received: $data");

    if (currentDriver == null) return;

    // Check Notif Time
    if (currentDriver != null &&
        currentDriver.driverType != DriverType.dynamic &&
        jobNotifTime != null) {
      final now = DateTime.now();
      int diff = now.difference(jobNotifTime).inSeconds;
      if (diff > (15 * 60)) {
        jobCubit.jobStaticCritical(
            true,
            Timestamp.fromDate(jobNotifTime),
            currentJob.jobId,
            currentDriver.driverId,
            currentDriver.primaryPhoneNumber);
      }
    }

    // Send Driver Coordinates
    jobCubit?.updateDriverCo(
        currentDriver?.driverId, Coordinates.fromJson(json.decode(data)));
  });
}

void _foregroundServiceFunction() async {
  // Get Driver Current Location
  Position value = await getGeoCurrentLocation(null);
  if (value == null) {
    return;
  }

  // print('Position Received: $value');

  // Change Notif Text
  // await ForegroundService.notification.startEditMode();
  // await ForegroundService.notification
  //     .setText("${value.latitude.toString()}, ${value.longitude.toString()}");
  // await ForegroundService.notification.finishEditMode();

  Coordinates driverCurrentCoordinates = Coordinates(
      lat: value.latitude,
      lng: value.longitude,
      timestamp: DateTime.now().toUtc().millisecondsSinceEpoch.toString());

  ForegroundService.sendToPort(
      "${json.encode(driverCurrentCoordinates.toJson())}");

  // print('Coordinates Sent');

  if (!ForegroundService.isIsolateCommunicationSetup) {
    ForegroundService.setupIsolateCommunication((data) {
      // print("bg isolate received: $data");

      if (currentDriver == null) return;

      // Check Notif Time
      if (currentDriver != null &&
          currentDriver.driverType != DriverType.dynamic &&
          jobNotifTime != null) {
        final now = DateTime.now();
        int diff = now.difference(jobNotifTime).inSeconds;
        if (diff > (15 * 60)) {
          jobCubit.jobStaticCritical(
              true,
              Timestamp.fromDate(jobNotifTime),
              currentJob.jobId,
              currentDriver.driverId,
              currentDriver.primaryPhoneNumber);
        }
      }

      // Send Driver Coordinates
      jobCubit?.updateDriverCo(
          currentDriver?.driverId, Coordinates.fromJson(json.decode(data)));
    });
  }
}

class DashboardPage extends StatefulWidget {
  final Job job;
  final Driver driverInfo;
  final String jobId;

  const DashboardPage({Key key, this.job, this.driverInfo, this.jobId})
      : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
    with WidgetsBindingObserver {
  bool initRun;
  StreamSubscription _subscription;

  initConnectivity() async {
    // Check Internet
    if (jobDialog || dashDialog) return;
    int _connected = await checkInternet();
    if (_connected == 0) {
      loginCubit.showError(errors[1], 'please_turn_on_internet_services'.tr(),
          'assets/no_internet.png');
      return;
    }
  }

  @override
  void initState() {
    // Firebase Crash Test
    // FirebaseCrashlytics.instance.crash();

    // Add Bindings
    WidgetsBinding.instance.addObserver(this);

    // Foreground Service
    _maybeStartFGS();

    // Get Location Permission
    if (currentJob == null &&
        currentJobId == null &&
        currentDriver?.currentJob == null)
      checkLocationPermission(context, true);

    // Internet Check Stream
    _subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      initConnectivity();
    });

    if (dashDialog && mounted) {
      dashDialog = false;
      Navigator.pop(context);
    }

    if (jobDialog && mounted) {
      jobDialog = false;
      Navigator.pop(context);
    }

    // To prevent on setState
    initRun = false;

    if (!initRun) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        initRun = true;
        if (currentDriver.cashInHand >= currentDriver.cashLimit) {
          dashDialog = true;
          loginCubit.blockDriver(currentDriver, true);
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) => WillPopScope(
                    onWillPop: () async => false,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      elevation: 2,
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              margin: EdgeInsets.all(15),
                              width: 180,
                              child: Text(
                                'deposit_cash_at_bridgeLinx_office'.tr(),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style:
                                    Theme.of(context).accentTextTheme.headline5,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(15),
                              width: 180,
                              child: Text(
                                'contact_support'.tr(),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style:
                                    Theme.of(context).accentTextTheme.headline5,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            FlatButton(
                              padding: EdgeInsets.all(15),
                              height: 50,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              color: Theme.of(context).primaryColorDark,
                              onPressed: () {
                                try {
                                  makePhoneCall(helpline);
                                } catch (e) {
                                  print(e.toString());
                                }
                              },
                              child: Center(
                                child: Icon(FontAwesomeIcons.phoneAlt,
                                    color: Colors.white, size: 20),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ));
        } else if (currentDriver.driverBlocked) {
          dashDialog = true;
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) => WillPopScope(
                    onWillPop: () async => false,
                    child: Dialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      elevation: 2,
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              margin: EdgeInsets.all(15),
                              width: 180,
                              child: Text(
                                'driver_blocked'.tr(),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style:
                                    Theme.of(context).accentTextTheme.headline5,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(15),
                              width: 180,
                              child: Text(
                                'contact_support'.tr(),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style:
                                    Theme.of(context).accentTextTheme.headline5,
                                textAlign: TextAlign.center,
                              ),
                            ),
                            FlatButton(
                              padding: EdgeInsets.all(15),
                              height: 50,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              color: Theme.of(context).primaryColorDark,
                              onPressed: () {
                                try {
                                  makePhoneCall(helpline);
                                } catch (e) {
                                  print(e.toString());
                                }
                              },
                              child: Center(
                                child: Icon(FontAwesomeIcons.phoneAlt,
                                    color: Colors.white, size: 20),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ));
        } else if (widget.job != null) {
          if (widget.job.jobStatus.status == JobStatus.jobOngoing &&
              widget.job.driverStatus.status != DriverStatus.driverInit &&
              widget.job.jobId == currentDriver.currentJob) {
            jobCubit.continueJob(widget.job, null);
          } else if (widget.job.jobStatus.status == JobStatus.jobFinished) {
            if (currentDriver.driverType == DriverType.dynamic)
              jobCubit.setCurrentJob(null, widget.job.jobId);
            else
              jobCubit.moveToBase(widget.job);
          } else {
            // Check 2 hours Difference
            if (widget.job.pickupTime
                    .toDate()
                    .difference(DateTime.now())
                    .inMinutes <
                120) {
              dashDialog = true;
              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) => JobDialog(
                  title: "new_order".tr(),
                  job: widget.job,
                ),
              ).then((value) {
                dashDialog = false;
              });
            }
          }
        } else if (widget.jobId == '') {
          dashDialog = true;
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (_) => AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              elevation: 2,
              title: Text(
                'order_rejected'.tr(),
                style: Theme.of(context).accentTextTheme.headline5,
              ),
              content: Text(
                'order_was_assigned_to_another_driver'.tr(),
                style: Theme.of(context).accentTextTheme.headline1,
              ),
              actions: [
                FlatButton(
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    padding: EdgeInsets.all(10),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'okay'.tr(),
                      style: Theme.of(context)
                          .accentTextTheme
                          .headline1
                          .copyWith(color: Colors.white),
                    ))
              ],
            ),
          ).then((value) {
            dashDialog = false;
          });
        } else if (widget.jobId != null && widget.jobId != '') {
          jobCubit.getCurrentJob(widget.jobId);
        } else if (!currentDriver.isBase &&
            currentDriver.driverType != DriverType.dynamic) {
          final job = await getLastJob();
          if (job != null) jobCubit.moveToBase(job);
        }

        // Check App Updates
        if (!jobDialog && !dashDialog)
          try {
            VersionCheck.versionCheck(context);
          } catch (e) {
            print(e);
          }
      });
    }

    super.initState();
  }

  @override
  void dispose() async {
    WidgetsBinding.instance.removeObserver(this);
    _subscription.cancel();
    await ForegroundService?.stopForegroundService();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // if (appFlavor == FlavorType.Dev) {
        //   await SystemChannels.platform
        //       .invokeMethod<void>('SystemNavigator.pop');
        // } else {
        // if (currentDriver.driverType == DriverType.dynamic) {
        dashDialog = true;
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (_) => AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            elevation: 2,
            title: Text(
              'exit'.tr(),
              style: Theme.of(context).accentTextTheme.headline5,
            ),
            content: Text(
              'are_you_sure'.tr(),
              style: Theme.of(context).accentTextTheme.headline1,
            ),
            actions: [
              FlatButton(
                minWidth: 80,
                color: Colors.blueGrey[50],
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                        color: Theme.of(context).primaryColor,
                        width: 1,
                        style: BorderStyle.solid),
                    borderRadius: BorderRadius.circular(5)),
                padding: EdgeInsets.all(10),
                onPressed: () async {
                  Navigator.pop(context);
                  await FlutterForegroundPlugin?.stopForegroundService();
                  await SystemChannels.platform
                      .invokeMethod<void>('SystemNavigator.pop');
                },
                child: Text(
                  'yes'.tr(),
                  style: Theme.of(context)
                      .accentTextTheme
                      .headline1
                      .copyWith(color: Theme.of(context).primaryColor),
                ),
              ),
              FlatButton(
                minWidth: 80,
                color: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                padding: EdgeInsets.all(10),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  'no'.tr(),
                  style: Theme.of(context)
                      .accentTextTheme
                      .headline1
                      .copyWith(color: Colors.white),
                ),
              ),
            ],
          ),
        ).then((value) {
          dashDialog = false;
        });
        /*
          } else {
            dashDialog = true;
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (_) => AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                elevation: 2,
                title: Text(
                  'Exit',
                  style: Theme.of(context).accentTextTheme.headline5,
                ),
                content: Text(
                  'Static Drivers cannot exit app',
                  style: Theme.of(context).accentTextTheme.headline1,
                ),
                actions: [
                  FlatButton(
                      minWidth: 80,
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      padding: EdgeInsets.all(10),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Okay',
                        style: Theme.of(context)
                            .accentTextTheme
                            .headline1
                            .copyWith(color: Colors.white),
                      ))
                ],
              ),
            ).then((value) {
              dashDialog = false;
            });
          }
        }
          */
        return true;
      },
      child: Scaffold(
          drawer: Drawer(
            child: Column(
              // Important: Remove any padding from the ListView.
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                // Header
                DrawerHeader(
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Icon(
                          FontAwesomeIcons.solidUserCircle,
                          color: Colors.white,
                          size: 50,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          currentDriver.profile.firstName == null ||
                                  currentDriver.profile.lastName == null
                              ? 'not_registered_yet'.tr()
                              : '${currentDriver.profile.firstName} ${currentDriver.profile.lastName}',
                          style: Theme.of(context)
                              .accentTextTheme
                              .headline1
                              .copyWith(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          currentDriver.primaryPhoneNumber.startsWith('+')
                              ? '0${currentDriver.primaryPhoneNumber.substring(3)}'
                              : currentDriver.primaryPhoneNumber,
                          style: Theme.of(context)
                              .accentTextTheme
                              .headline1
                              .copyWith(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                  ),
                ),

                // Drawer Body

                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        // Jobs

                        ListTile(
                          title: Container(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(
                                  'jobs'.tr(),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .button
                                      .copyWith(color: Colors.black54),
                                ),
                                Icon(
                                  FontAwesomeIcons.list,
                                  color: Colors.black54,
                                )
                              ],
                            ),
                          ),
                          onTap: () {
                            jobCubit.jobListPage(currentDriver.jobHistory);
                          },
                        ),

                        // Wallet

                        /*
                        ListTile(
                          title: Container(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(
                                  'Wallet',
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .button
                                      .copyWith(color: Colors.black54),
                                ),
                                Icon(
                                  FontAwesomeIcons.wallet,
                                  color: Colors.black54,
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            Fluttertoast.showToast(msg: 'Coming soon');
                          },
                        ),
                        */
                        // Languages
                        ExpansionTile(
                          title: Container(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(
                                  "language".tr(),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .button
                                      .copyWith(color: Colors.black54),
                                ),
                                Icon(
                                  FontAwesomeIcons.language,
                                  color: Colors.black54,
                                ),
                              ],
                            ),
                          ),
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                context.locale = EasyLocalization.of(context)
                                    .supportedLocales[0];
                                saveLocale(context.locale.toString());
                              },
                              child: Container(
                                padding: EdgeInsets.all(10),
                                child: Text(
                                  "English",
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .button
                                      .copyWith(color: Colors.black54),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                context.locale = EasyLocalization.of(context)
                                    .supportedLocales[1];
                                saveLocale(context.locale.toString());
                              },
                              child: Container(
                                padding: EdgeInsets.all(10),
                                child: Text(
                                  "اردو",
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .button
                                      .copyWith(color: Colors.black54),
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),

                // Logout

                ListTile(
                  title: Container(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          'logout'.tr(),
                          style: Theme.of(context)
                              .accentTextTheme
                              .button
                              .copyWith(color: Colors.black54),
                        ),
                        Icon(
                          FontAwesomeIcons.signOutAlt,
                          color: Colors.black54,
                        )
                      ],
                    ),
                  ),
                  onTap: () {
                    loginCubit.logout();
                  },
                ),
              ],
            ),
          ),
          appBar: MyAppBar(
            call: () {
              setState(() {
                makePhoneCall(helpline);
              });
            },
          ),
          body: Stack(
            children: [
              Positioned.fill(
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.all(15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  // Driver Details
                                  Container(
                                    padding: EdgeInsets.all(20),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(4),
                                        topRight: Radius.circular(4),
                                      ),
                                      border: Border.all(
                                        width: 1,
                                        color: Colors.grey,
                                      ),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        // Driver Name
                                        Container(
                                          margin: EdgeInsets.all(5),
                                          child: Icon(
                                            FontAwesomeIcons.user,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Container(
                                          width: 200,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'name'.tr(),
                                                style: Theme.of(context)
                                                    .accentTextTheme
                                                    .bodyText2,
                                              ),
                                              Text(
                                                currentDriver.profile
                                                                .firstName ==
                                                            null ||
                                                        currentDriver.profile
                                                                .lastName ==
                                                            null
                                                    ? 'not_registered_yet'.tr()
                                                    : '${currentDriver.profile.firstName} ${currentDriver.profile.lastName}',
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 5,
                                                style: Theme.of(context)
                                                    .accentTextTheme
                                                    .headline2,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  // Phone Number
                                  Container(
                                    padding: EdgeInsets.all(20),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(4),
                                        bottomRight: Radius.circular(4),
                                      ),
                                      border: Border.all(
                                        width: 1,
                                        color: Colors.grey,
                                      ),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.all(5),
                                          child: Icon(FontAwesomeIcons.phoneAlt,
                                              color: Theme.of(context)
                                                  .primaryColor),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Container(
                                          width: 200,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'phone_number'.tr(),
                                                style: Theme.of(context)
                                                    .accentTextTheme
                                                    .bodyText2,
                                              ),
                                              Text(
                                                currentDriver.primaryPhoneNumber
                                                        .startsWith('+')
                                                    ? '0${currentDriver.primaryPhoneNumber.substring(3)}'
                                                    : currentDriver
                                                            .primaryPhoneNumber ??
                                                        'not_registered_yet'
                                                            .tr(),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 5,
                                                style: Theme.of(context)
                                                    .accentTextTheme
                                                    .headline2,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 25,
                            ),
                            Container(
                              margin: EdgeInsets.all(20),
                              height: 122,
                              width: 122,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xffEEEEEE)),
                              child: Stack(
                                children: [
                                  Center(
                                    child: Icon(
                                      FontAwesomeIcons.truck,
                                      size: 50,
                                    ),
                                  ),
                                  Center(
                                    child: Transform.rotate(
                                      angle: 45,
                                      child: Container(
                                        width: 10,
                                        height: double.infinity,
                                        color: Color(0xFFC4C4C4),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(10),
                              child: Text(
                                'no_orders'.tr(),
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .accentTextTheme
                                    .headline5
                                    .copyWith(color: Color(0xffFF6363)),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(),
                            Container(
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.all(30),
                                    padding: EdgeInsets.all(15),
                                    decoration: BoxDecoration(
                                      color: currentDriver.cashInHand <
                                              currentDriver.cashLimit
                                          ? Colors.blue[100]
                                          : Colors.red[100],
                                      border: Border.all(
                                          width: 1,
                                          color: currentDriver.cashInHand <
                                                  currentDriver.cashLimit
                                              ? Theme.of(context).primaryColor
                                              : Color(0xFFFF4949)),
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'cash_in_hand'.tr(),
                                          style: Theme.of(context)
                                              .accentTextTheme
                                              .headline5,
                                        ),
                                        Text(
                                          'Rs'.tr(args: [
                                            currentDriver.cashInHand
                                                .toInt()
                                                .toString()
                                          ]),
                                          style: Theme.of(context)
                                              .accentTextTheme
                                              .headline5,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    right: 0,
                                    bottom: 0,
                                    left: 0,
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 5, horizontal: 15),
                                      child: Text(
                                        appVersion,
                                        textAlign: TextAlign.end,
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .bodyText1,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
          // : Container(
          //     padding: EdgeInsets.all(15),
          //     child: Column(
          //       crossAxisAlignment: CrossAxisAlignment.stretch,
          //       mainAxisAlignment: MainAxisAlignment.start,
          //       children: [
          //         Container(
          //           margin: EdgeInsets.all(10),
          //           child: Text(
          //             'Active Jobs',
          //             style: style.copyWith(fontWeight: FontWeight.bold),
          //           ),
          //         ),
          //         Container(
          //           padding: EdgeInsets.all(10),
          //           decoration: BoxDecoration(
          //               border: Border.all(width: 1, color: Colors.grey)),
          //           child: Column(
          //             crossAxisAlignment: CrossAxisAlignment.stretch,
          //             mainAxisAlignment: MainAxisAlignment.start,
          //             mainAxisSize: MainAxisSize.min,
          //             children: [
          //               Container(
          //                 padding: EdgeInsets.all(10),
          //                 child: Text('You have an active ride scheduled for'),
          //               ),
          //               Container(
          //                 padding: EdgeInsets.all(10),
          //                 child: Row(
          //                   mainAxisAlignment: MainAxisAlignment.start,
          //                   crossAxisAlignment: CrossAxisAlignment.center,
          //                   mainAxisSize: MainAxisSize.max,
          //                   children: [
          //                     Icon(FontAwesomeIcons.calendar),
          //                     SizedBox(
          //                       width: 5,
          //                     ),
          //                     Text(
          //                         '${DateFormat.yMMMd().format(DateTime.fromMillisecondsSinceEpoch(int.parse(widget.job.pickupCo.timestamp)))}'),
          //                   ],
          //                 ),
          //               ),
          //               Container(
          //                 color: Colors.green,
          //                 padding: EdgeInsets.all(10),
          //                 child: Row(
          //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                   crossAxisAlignment: CrossAxisAlignment.center,
          //                   mainAxisSize: MainAxisSize.max,
          //                   children: [
          //                     Text(
          //                       'Tap to start',
          //                       style: style.copyWith(color: Colors.white),
          //                     ),
          //                     Icon(FontAwesomeIcons.arrowRight),
          //                   ],
          //                 ),
          //               )
          //             ],
          //           ),
          //         )
          //       ],
          //     ),
          //   ),
          ),
    );
  }
}
