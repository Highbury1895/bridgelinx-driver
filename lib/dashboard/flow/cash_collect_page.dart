import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:bridgelinx_driver/utils/globals.dart';

class CashCollectPage extends StatefulWidget {
  final Job job;
  final Coordinates coordinates;
  final String buttonText;
  final Journey journey;

  const CashCollectPage(
      {Key key, this.job, this.coordinates, this.buttonText, this.journey})
      : super(key: key);
  @override
  _CashCollectPageState createState() => _CashCollectPageState();
}

class _CashCollectPageState extends State<CashCollectPage> {
  Job _job;
  Coordinates _coordinates;
  String _buttonText;
  Journey _journey;
  @override
  void initState() {
    _job = widget.job;
    _coordinates = widget.coordinates;
    _buttonText = widget.buttonText;
    _journey = widget.journey;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'collect_cash'.tr(),
          style: Theme.of(context)
              .accentTextTheme
              .headline1
              .copyWith(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: Icon(FontAwesomeIcons.headset),
            onPressed: () => setState(() {
              makePhoneCall(helpline);
            }),
          ),
        ],
      ),
      body: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.all(30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'collect_cash_from'.tr(),
                          style: Theme.of(context)
                              .accentTextTheme
                              .headline1
                              .copyWith(color: Color(0xFF1D1D1D)),
                          textAlign: TextAlign.start,
                        ),

                        SizedBox(
                          height: 30,
                        ),

                        // User Details

                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            // User

                            Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                // CircleAvatar(
                                //   backgroundColor: Colors.blueGrey[100],
                                //   radius: 25,
                                //   child: Center(
                                //     child: Icon(
                                //       FontAwesomeIcons.userAlt,
                                //       color: Colors.grey,
                                //       size: 20,
                                //     ),
                                //   ),
                                // ),
                                Container(
                                  // padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: Text(
                                    _coordinates.contactName,
                                    style: Theme.of(context)
                                        .accentTextTheme
                                        .headline1,
                                  ),
                                )
                              ],
                            ),

                            // Buttons

                            Wrap(
                              alignment: WrapAlignment.end,
                              children: [
                                ButtonTheme(
                                  height: 40,
                                  minWidth: 40,
                                  child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    color: Theme.of(context).primaryColorDark,
                                    onPressed: () {
                                      try {
                                        makePhoneCall(
                                            _coordinates.contactPhone);
                                      } catch (e) {
                                        print(e.toString());
                                      }
                                    },
                                    child: Center(
                                      child: Icon(FontAwesomeIcons.phoneAlt,
                                          color: Colors.white, size: 20),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),

                        SizedBox(
                          height: 50,
                        ),

                        Text(
                          'cash_details'.tr(),
                          style: Theme.of(context)
                              .accentTextTheme
                              .headline1
                              .copyWith(
                                color: Color(0XFF1D1D1D),
                              ),
                        ),

                        SizedBox(
                          height: 30,
                        ),

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                color: Colors.blue[50],
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Visibility(
                                    visible: _job.cashForGoods > 0,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'product_price'.tr(),
                                          style: Theme.of(context)
                                              .accentTextTheme
                                              .headline5
                                              .copyWith(
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                        ),
                                        SizedBox(height: 20),
                                        Text(
                                          'Rs'.tr(args: [
                                            _job.cashForGoods.toInt().toString()
                                          ]),
                                          style: Theme.of(context)
                                              .accentTextTheme
                                              .headline5
                                              .copyWith(
                                                color: Theme.of(context)
                                                    .primaryColorDark,
                                              ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'ride_cost'.tr(),
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline5
                                            .copyWith(
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                      ),
                                      Text(
                                        'Rs'.tr(args: [
                                          (_job.priceInfo.finalAmount == null
                                                  ? _job
                                                      .priceInfo.estimatedAmount
                                                  : _job.priceInfo
                                                              .estimatedAmount <
                                                          _job.priceInfo
                                                              .finalAmount
                                                      ? _job.priceInfo
                                                          .estimatedAmount
                                                      : _job.priceInfo
                                                          .finalAmount)
                                              .toInt()
                                              .toString()
                                        ]),
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline5
                                            .copyWith(
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'total_cost'.tr(),
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline5
                                            .copyWith(
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                      ),
                                      Text(
                                        'Rs'.tr(args: [
                                          ((_job.cashForGoods ?? 0) +
                                                  (_job.priceInfo.finalAmount ==
                                                          null
                                                      ? _job.priceInfo
                                                          .estimatedAmount
                                                      : _job.priceInfo
                                                                  .estimatedAmount <
                                                              _job.priceInfo
                                                                  .finalAmount
                                                          ? _job.priceInfo
                                                              .estimatedAmount
                                                          : _job.priceInfo
                                                              .finalAmount))
                                              .toInt()
                                              .toString()
                                        ]),
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline5
                                            .copyWith(
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    FlatButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      color: Color(0xFF00C78B),
                      onPressed: () {
                        if (_job.priceInfo.paymentStatus == PaymentStatus.paid)
                          jobCubit.nextJourney(_job, _journey);
                        else if (_journey.journeyType == JourneyType.Pickup &&
                            _job.priceInfo.paymentStatus != PaymentStatus.paid)
                          jobCubit.exitPickup(_job, _journey);
                        else if (_journey.journeyType == JourneyType.Dropoff &&
                            _job.priceInfo.paymentStatus != PaymentStatus.paid)
                          jobCubit.exitDropoff(_job, _journey);
                        // else if (_job.priceInfo.paymentType ==
                        //         PaymentType.atDrop &&
                        //     _job.priceInfo.paymentStatus == PaymentStatus.paid)
                        //   jobCubit.nextJourney(_job, _journey);
                      },
                      child: Center(
                        child: Container(
                          padding: EdgeInsets.all(15),
                          child: Text(
                            _buttonText,
                            style: Theme.of(context)
                                .accentTextTheme
                                .headline1
                                .copyWith(
                                  color: Colors.white,
                                ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
