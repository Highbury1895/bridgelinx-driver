import 'dart:async';
import 'dart:io';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:bridgelinx_driver/dashboard/flow/image_hint_page.dart';
// import 'package:camera/camera.dart';
import 'package:flutter_better_camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:easy_localization/easy_localization.dart';

class CameraPage extends StatefulWidget {
  final String title;
  final Job job;
  final String imageName;
  final Function func;
  final Journey journey;

  const CameraPage(
      {Key key, this.job, this.imageName, this.func, this.title, this.journey})
      : super(key: key);

  @override
  _CameraPageState createState() => _CameraPageState();
}

/// Returns a suitable camera icon for [direction].
IconData getCameraLensIcon(CameraLensDirection direction) {
  switch (direction) {
    case CameraLensDirection.back:
      return Icons.camera_rear;
    case CameraLensDirection.front:
      return Icons.camera_front;
    case CameraLensDirection.external:
      return Icons.camera;
  }
  throw ArgumentError('Unknown lens direction');
}

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class _CameraPageState extends State<CameraPage> with WidgetsBindingObserver {
  CameraController controller;
  String imagePath;
  bool enableAudio = true;
  FlashMode flashMode = FlashMode.off;

  bool hint = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    onNewCameraSelected(cameras[0]);

    if (!hint) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        hint = true;
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ImageHintPage(
                      job: widget.job,
                    )));
      });
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    controller?.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        onNewCameraSelected(controller.description);
      }
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          widget.title,
          style: Theme.of(context)
              .accentTextTheme
              .headline1
              .copyWith(color: Colors.white),
        ),
      ),
      body: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Column(
          children: [
            Expanded(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(),
                            Container(
                              child: FlatButton(
                                color: Colors.transparent,
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ImageHintPage(
                                                job: widget.job,
                                              )));
                                },
                                child: Icon(
                                  FontAwesomeIcons.infoCircle,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(1.0),
                          child: Center(
                            child: imagePath == null
                                ? _cameraPreviewWidget()
                                : _thumbnailWidget(),
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          border: Border.all(
                            color: controller != null &&
                                    controller.value.isRecordingVideo
                                ? Colors.redAccent
                                : Colors.grey,
                            width: 3.0,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: imagePath == null
                          ? _captureControlRowWidget()
                          : _retakeButtonsWidget(),
                    ),
                    Visibility(visible: false, child: _toggleAudioWidget()),
                    Visibility(
                      visible: false,
                      child: Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Visibility(
                                  visible: false,
                                  child: _cameraTogglesRowWidget()),
                              _thumbnailWidget(),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Display retake and submit image button
  Widget _retakeButtonsWidget() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            color: Color(0xFF00C78B),
            onPressed: () async {
              // Check Internet
              int _connected = await checkInternet();
              if (_connected == 2) {
                Job _job = await jobCubit.uploadPic(
                    widget.job, widget.journey, imagePath, widget.imageName);
                widget.func(_job);
              } else {
                jobCubit.showError(
                    currentJob,
                    errors[1],
                    'please_turn_on_internet_services'.tr(),
                    'assets/no_internet.png',
                    widget.journey);
              }
            },
            child: Center(
              child: Container(
                padding: EdgeInsets.all(15),
                child: Text(
                  'submit_picture'.tr(),
                  style: Theme.of(context).accentTextTheme.headline1.copyWith(
                        color: Colors.white,
                      ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            color: Color(0xFFC4C4C4),
            onPressed: () {
              if (mounted)
                setState(() {
                  imagePath = null;
                });
            },
            child: Center(
              child: Container(
                padding: EdgeInsets.all(15),
                child: Text(
                  'retake_picture'.tr(),
                  style: Theme.of(context).accentTextTheme.headline1.copyWith(
                        color: Colors.white,
                      ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Display the preview from the camera (or a message if the preview is not available).
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return Text(
        'tap_a_camera'.tr(),
        style: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.w900,
        ),
      );
    } else {
      return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CameraPreview(controller),
      );
    }
  }

  /// Toggle recording audio
  Widget _toggleAudioWidget() {
    return Padding(
      padding: const EdgeInsets.only(left: 25),
      child: Row(
        children: <Widget>[
          Text('enable_audio'.tr()),
          Switch(
            value: enableAudio,
            onChanged: (bool value) {
              enableAudio = value;
              if (controller != null) {
                onNewCameraSelected(controller.description);
              }
            },
          ),
        ],
      ),
    );
  }

  /// Display the thumbnail of the captured image or video.
  Widget _thumbnailWidget() {
    return Align(
      alignment: Alignment.center,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          imagePath == null
              ? Container()
              : Container(
                  child: Image.file(File(imagePath)),
                ),
        ],
      ),
    );
  }

  /// Display the control bar with buttons to take pictures and record videos.
  Widget _captureControlRowWidget() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: Center(
              child: Container(
                // padding: EdgeInsets.all(20),
                child: _flashButton(),
                // Row(
                //   children: [
                // Text(
                //   'Flash',
                //   style: Theme.of(context).accentTextTheme.headline1,
                // ),
                // Switch(
                //     value: lampOn,
                //     onChanged: (val) async {
                //       lampOn = !lampOn;
                //       controller.dispose().then((value) {
                //         try {
                //           lampOn
                //               ? hasLamp
                //                   ? Lamp.turnOn(intensity: 0.4)
                //                   : DoNothingAction()
                //               : hasLamp
                //                   ? Lamp.turnOff()
                //                   : DoNothingAction();
                //         } catch (e) {
                //           print(e.toString());
                //         }

                //         controller = CameraController(
                //           cameras[0],
                //           ResolutionPreset.medium,
                //           enableAudio: enableAudio,
                //         );
                //       });
                //     }),
                //   ],
                // ),
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: controller != null &&
                      controller.value.isInitialized &&
                      !controller.value.isRecordingVideo
                  ? onTakePictureButtonPressed
                  : null,
              child: CircleAvatar(
                backgroundColor: Colors.grey[100],
                radius: 50,
                child: Container(
                  width: 80.0,
                  height: 80.0,
                  decoration: new BoxDecoration(
                    color: Colors.blue[300],
                    borderRadius:
                        new BorderRadius.all(new Radius.circular(50.0)),
                    border: new Border.all(
                      color: Colors.grey,
                      width: 4.0,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(child: Container()),
        ],
      ),
    );
  }

  /// Flash Toggle Button
  Widget _flashButton() {
    IconData iconData = Icons.flash_off;
    Color color = Colors.black;
    if (flashMode == FlashMode.alwaysFlash) {
      iconData = Icons.flash_on;
      color = Colors.blue;
    } else if (flashMode == FlashMode.autoFlash) {
      iconData = Icons.flash_auto;
      color = Colors.red;
    }
    return IconButton(
      icon: Icon(
        iconData,
        size: 40,
      ),
      color: color,
      onPressed: controller != null && controller.value.isInitialized
          ? _onFlashButtonPressed
          : null,
    );
  }

  /// Toggle Flash
  Future<void> _onFlashButtonPressed() async {
    // bool hasFlash = false;
    if (flashMode == FlashMode.off || flashMode == FlashMode.torch) {
      // Turn on the flash for capture
      flashMode = FlashMode.alwaysFlash;
    }
    // else if (flashMode == FlashMode.alwaysFlash) {
    //   // Turn on the flash for capture if needed
    //   flashMode = FlashMode.autoFlash;
    // }
    else {
      // Turn off the flash
      flashMode = FlashMode.off;
    }
    // Apply the new mode
    await controller.setFlashMode(flashMode);

    // Change UI State
    setState(() {});
  }

  /// Display a row of toggle to select the camera (or a message if no camera is available).
  Widget _cameraTogglesRowWidget() {
    final List<Widget> toggles = <Widget>[];

    if (cameras.isEmpty) {
      return Text('no_camera_found'.tr());
    } else {
      for (CameraDescription cameraDescription in cameras) {
        toggles.add(
          SizedBox(
            width: 90.0,
            child: RadioListTile<CameraDescription>(
              title: Icon(getCameraLensIcon(cameraDescription.lensDirection)),
              groupValue: controller?.description,
              value: cameraDescription,
              onChanged: controller != null && controller.value.isRecordingVideo
                  ? null
                  : onNewCameraSelected,
            ),
          ),
        );
      }
    }

    return Row(children: toggles);
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
      enableAudio: enableAudio,
    );

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void onTakePictureButtonPressed() {
    // if (lampOn && hasLamp) Lamp.turnOff();

    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          imagePath = filePath;
        });
        // if (filePath != null) showInSnackBar('Picture saved to $filePath');
      }
    });
  }

  Future<void> pauseVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.pauseVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      rethrow;
    }
  }

  Future<void> resumeVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.resumeVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      rethrow;
    }
  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${widget.imageName}${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  void _showCameraException(CameraException e) {
    logError(e.code, e.description);
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }
}
