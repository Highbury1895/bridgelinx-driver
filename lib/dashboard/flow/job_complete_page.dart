import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:bridgelinx_driver/utils/globals.dart';

class JobCompletePage extends StatefulWidget {
  final Job job;

  const JobCompletePage({Key key, this.job}) : super(key: key);
  @override
  _JobCompletePageState createState() => _JobCompletePageState();
}

class _JobCompletePageState extends State<JobCompletePage> {
  Job _job;
  List<Coordinates> pickupCoordinates = List.from([]);
  List<Coordinates> dropoffCoordinates = List.from([]);

  getClientCoordinates(Job job) {
    Map<String, dynamic> pickupMap = _job.clientCoordinatesJourney.pickup;
    Map<String, dynamic> dropoffMap = _job.clientCoordinatesJourney.dropoff;

    pickupCoordinates
        .addAll(pickupMap.values.map((e) => Coordinates.fromJson(e)).toList());
    dropoffCoordinates
        .addAll(dropoffMap.values.map((e) => Coordinates.fromJson(e)).toList());
  }

  @override
  void initState() {
    _job = widget.job;
    getClientCoordinates(_job);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'order_complete'.tr(),
          style: Theme.of(context)
              .accentTextTheme
              .headline1
              .copyWith(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: Icon(FontAwesomeIcons.headset),
            onPressed: () => setState(() {
              makePhoneCall(helpline);
            }),
          ),
        ],
      ),
      body: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.all(15),
                color: Colors.green[100],
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      FontAwesomeIcons.checkCircle,
                      color: Colors.green,
                      size: 20,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'order_completed_successfully'.tr(),
                      style: Theme.of(context)
                          .accentTextTheme
                          .headline1
                          .copyWith(color: Color(0xFF00C78B)),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 20,
              child: SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'order_details'.tr(),
                        style: Theme.of(context)
                            .accentTextTheme
                            .headline1
                            .copyWith(color: Color(0xFF1D1D1D)),
                        textAlign: TextAlign.start,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                        // height: (100 * pickupCoordinates.length).toDouble(),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(4),
                              topRight: Radius.circular(4),
                            ),
                            border: Border.all(color: Colors.grey, width: 1)),
                        child: Center(
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: pickupCoordinates.length,
                            physics: ScrollPhysics(),
                            itemBuilder: (context, index) {
                              final _coordinates = pickupCoordinates[index];
                              return Container(
                                // height: 65,
                                margin: EdgeInsets.only(bottom: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      child: Icon(
                                        FontAwesomeIcons.mapMarkerAlt,
                                        color: Theme.of(context).primaryColor,
                                        size: 20,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Container(
                                      width: 200,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            _coordinates.address,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 5,
                                            style: Theme.of(context)
                                                .accentTextTheme
                                                .bodyText1,
                                          ),
                                          Text(
                                            _coordinates.formattedAddress,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 5,
                                            style: Theme.of(context)
                                                .accentTextTheme
                                                .bodyText1,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      Container(
                        // height: (100 * dropoffCoordinates.length).toDouble(),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(4),
                                bottomRight: Radius.circular(4)),
                            border: Border.all(color: Colors.grey, width: 1)),
                        child: Center(
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: dropoffCoordinates.length,
                            physics: ScrollPhysics(),
                            itemBuilder: (context, index) {
                              final _coordinates = dropoffCoordinates[index];
                              return Container(
                                // height: 65,
                                margin: EdgeInsets.only(bottom: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      child: Icon(
                                        FontAwesomeIcons.mapMarkerAlt,
                                        color: Color(0xFFFF4949),
                                        size: 20,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Container(
                                      width: 200,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            _coordinates.address,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 5,
                                            style: Theme.of(context)
                                                .accentTextTheme
                                                .bodyText1,
                                          ),
                                          Text(
                                            _coordinates.formattedAddress,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 5,
                                            style: Theme.of(context)
                                                .accentTextTheme
                                                .bodyText1,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ),

                      /*
                            SizedBox(
                              height: 30,
                            ),

                            // User Details

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                // User

                                Wrap(
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  children: [
                                    // CircleAvatar(
                                    //   backgroundColor: Colors.blueGrey[100],
                                    //   radius: 25,
                                    //   child: Center(
                                    //     child: Icon(
                                    //       FontAwesomeIcons.userAlt,
                                    //       color: Colors.grey,
                                    //       size: 20,
                                    //     ),
                                    //   ),
                                    // ),
                                    Container(
                                      // padding: EdgeInsets.symmetric(horizontal: 10),
                                      child: Text(
                                        _job.clientCoordinates.dropoff.contactName,
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline1,
                                      ),
                                    )
                                  ],
                                ),

                                // Buttons

                                Wrap(
                                  alignment: WrapAlignment.end,
                                  children: [
                                    ButtonTheme(
                                      height: 40,
                                      minWidth: 40,
                                      child: FlatButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(5)),
                                        color: Theme.of(context).primaryColorDark,
                                        onPressed: () {
                                          try {
                                            makePhoneCall(_job.clientCoordinates
                                                .dropoff.contactPhone);
                                          } catch (e) {
                                            print(e.toString());
                                          }
                                        },
                                        child: Center(
                                          child: Icon(FontAwesomeIcons.phoneAlt,
                                              color: Colors.white, size: 20),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            */

                      SizedBox(
                        height: 50,
                      ),
                      Text(
                        'cash_details'.tr(),
                        style: Theme.of(context)
                            .accentTextTheme
                            .headline1
                            .copyWith(color: Color(0xFF1D1D1D)),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              color: Colors.blue[50],
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Visibility(
                                  visible: _job.cashForGoods > 0,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'product_price'.tr(),
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline5
                                            .copyWith(
                                                color: Theme.of(context)
                                                    .primaryColorDark),
                                      ),
                                      SizedBox(height: 20),
                                      Text(
                                        'Rs'.tr(args: [
                                          _job.cashForGoods.toInt().toString()
                                        ]),
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline5
                                            .copyWith(
                                                color: Theme.of(context)
                                                    .primaryColorDark),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'ride_cost'.tr(),
                                      style: Theme.of(context)
                                          .accentTextTheme
                                          .headline5
                                          .copyWith(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                    ),
                                    Text(
                                      'Rs'.tr(args: [
                                        _job.businessCode == null
                                            ? (_job.priceInfo.finalAmount ==
                                                        null
                                                    ? _job.priceInfo
                                                        .estimatedAmount
                                                    : _job.priceInfo
                                                                .estimatedAmount <
                                                            _job.priceInfo
                                                                .finalAmount
                                                        ? _job.priceInfo
                                                            .estimatedAmount
                                                        : _job.priceInfo
                                                            .finalAmount)
                                                .toInt()
                                                .toString()
                                            : '0'
                                      ]),
                                      style: Theme.of(context)
                                          .accentTextTheme
                                          .headline5
                                          .copyWith(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'total_cost'.tr(),
                                      style: Theme.of(context)
                                          .accentTextTheme
                                          .headline5
                                          .copyWith(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                    ),
                                    Text(
                                      'Rs'.tr(args: [
                                        _job.businessCode == null
                                            ? ((_job.cashForGoods ?? 0) +
                                                    (_job.priceInfo
                                                                .finalAmount ==
                                                            null
                                                        ? _job.priceInfo
                                                            .estimatedAmount
                                                        : _job.priceInfo
                                                                    .estimatedAmount <
                                                                _job.priceInfo
                                                                    .finalAmount
                                                            ? _job.priceInfo
                                                                .estimatedAmount
                                                            : _job.priceInfo
                                                                .finalAmount))
                                                .toInt()
                                                .toString()
                                            : '0'
                                      ]),
                                      style: Theme.of(context)
                                          .accentTextTheme
                                          .headline5
                                          .copyWith(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Container(
                margin: EdgeInsets.all(30),
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  color: Color(0xFF00C78B),
                  onPressed: () {
                    if (currentDriver.driverType == DriverType.dynamic)
                      jobCubit.setCurrentJob(null, _job.jobId);
                    else
                      jobCubit.moveToBase(_job);
                  },
                  child: Center(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        'order_complete'.tr(),
                        style: Theme.of(context)
                            .accentTextTheme
                            .button
                            .copyWith(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
