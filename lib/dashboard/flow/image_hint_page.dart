import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class ImageHintPage extends StatefulWidget {
  final Job job;

  const ImageHintPage({Key key, this.job}) : super(key: key);

  @override
  _ImageHintPageState createState() => _ImageHintPageState();
}

class _ImageHintPageState extends State<ImageHintPage> {
  String _title;
  String _desc;
  String _imagePath;

  initVars() {
    switch (widget.job.driverStatus.status) {
      case DriverStatus.driverLoading:
        _title = 'checkin_pickup_image'.tr();
        _desc = 'take_picture_vehicle_before_loading'.tr();
        _imagePath = 'assets/truck_empty.png';
        break;
      case DriverStatus.driverLoadingCompleted:
        _title = 'take_loading_complete_image'.tr();
        _desc = 'take_picture_vehicle_after_loading'.tr();
        _imagePath = 'assets/truck_loaded.png';
        break;
      case DriverStatus.driverLeft:
        _title = 'take_gatepass_image'.tr();
        _desc = 'take_picture_of_gate_pass'.tr();
        _imagePath = 'assets/gatepass.png';
        break;
      case DriverStatus.driverUnloading:
        _title = 'take_checkin_dropoff_image'.tr();
        _desc = 'take_picture_vehicle_before_unloading'.tr();
        _imagePath = 'assets/truck_loaded.png';
        break;
      case DriverStatus.driverUnloadingCompleted:
        _title = 'take_unloading_complete_image'.tr();
        _desc = 'take_picture_vehicle_after_unloading'.tr();
        _imagePath = 'assets/truck_empty.png';
        break;
      case DriverStatus.driverProofOfDelivery:
        _title = 'take_receiving_image'.tr();
        _desc = 'take_picture_receiving_document'.tr();
        _imagePath = 'assets/receiving.png';
        break;
      default:
        _title = 'wrong_status'.tr();
        _desc = 'programmer_made_mistake'.tr();
        _imagePath = 'assets/receiving.png';
        break;
    }
  }

  @override
  void initState() {
    jobDialog = true;
    initVars();
    super.initState();
  }

  @override
  void dispose() {
    jobDialog = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'image_hint'.tr(),
          style: Theme.of(context)
              .accentTextTheme
              .headline1
              .copyWith(color: Colors.white),
        ),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: Container(
          padding: EdgeInsets.all(30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    _title,
                    style: Theme.of(context).accentTextTheme.headline4,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Image.asset(
                        _imagePath,
                        height: 300,
                        width: 300,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    _desc,
                    style: Theme.of(context).accentTextTheme.headline5,
                  ),
                ],
              ),
              FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                color: Color(0xFF00C78B),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Center(
                  child: Container(
                    padding: EdgeInsets.all(15),
                    child: Text(
                      'okay'.tr(),
                      style:
                          Theme.of(context).accentTextTheme.headline1.copyWith(
                                color: Colors.white,
                              ),
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
