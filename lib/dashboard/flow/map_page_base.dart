import 'dart:async';
import 'dart:convert';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_foreground_plugin/flutter_foreground_plugin.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_mapbox_navigation/library.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:foreground_service/foreground_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:bridgelinx_driver/utils/google_services.dart';
import 'package:bridgelinx_driver/utils/loading.dart';
import 'package:location/location.dart';
import 'package:easy_localization/easy_localization.dart';

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void startForegroundService() async {
  await FlutterForegroundPlugin?.stopForegroundService();
  await FlutterForegroundPlugin?.setServiceMethodInterval(seconds: 5);
  await FlutterForegroundPlugin?.setServiceMethod(globalForegroundService);
  await FlutterForegroundPlugin?.startForegroundService(
    holdWakeLock: true,
    onStarted: () {
      // print("Foreground on Started");
    },
    onStopped: () {
      // print("Foreground on Stopped");
    },
    title: "BridgeLinx Driver",
    content: "Updating Coordinates",
    iconName: "ic_stat_hot_tub",
    subtext: "",
  );
}

void globalForegroundService() async {
  // Get Driver Current Location

  getGeoCurrentLocation(null).then((value) {
    final _googleMapsServices = GoogleMapsServices();
    // Send Driver Coordinates
    final driverCurrentCoordinates = Coordinates(
        lat: value.latitude,
        lng: value.longitude,
        timestamp: DateTime.now().toUtc().millisecondsSinceEpoch.toString());
    jobCubit.updateDriverCo(currentDriver.driverId, driverCurrentCoordinates);

    // Add Coordinates
    addDistances(Coordinates(
        lat: driverCurrentCoordinates.lat, lng: driverCurrentCoordinates.lng));
    // distanceTravelled.add(Coordinates(
    //     lat: driverCurrentCoordinates.lat, lng: driverCurrentCoordinates.lng));

    double distance = _googleMapsServices.calculateDistance(
        LatLng(driverCurrentCoordinates.lat, driverCurrentCoordinates.lng),
        LatLng(double.parse(currentDriver.baseLocation.lat),
            double.parse(currentDriver.baseLocation.lng)));

    distance < 0.05
        ? showToast('arrived_at_base'.tr(), 'you_have_arrived_at_base'.tr())
        : DoNothingAction();
  });
}

//use an async method so we can await
void _maybeStartFGS() async {
  ///if the app was killed+relaunched, this function will be executed again
  ///but if the foreground service stayed alive,
  ///this does not need to be re-done
  if (await ForegroundService.foregroundServiceIsStarted()) {
    await ForegroundService.stopForegroundService();

    //necessity of editMode is dubious (see function comments)
    await ForegroundService.notification.startEditMode();

    await ForegroundService.notification
        .setTitle("BridgeLinx Driver is Running");
    await ForegroundService.notification.setText("Updating Base Coordinates");

    await ForegroundService.notification.finishEditMode();

    await ForegroundService.setContinueRunningAfterAppKilled(true);

    await ForegroundService.startForegroundService(_foregroundServiceFunction);
    await ForegroundService.getWakeLock();
  }

  // Main Isolate
  ///this exists solely in the main app/isolate,
  ///so needs to be redone after every app kill+relaunch
  await ForegroundService.setupIsolateCommunication((data) async {
    // debugPrint("main received: $data");

    if (currentDriver == null) return;

    final _googleMapsServices = GoogleMapsServices();

    // Send Driver Coordinates
    final driverCurrentCoordinates = Coordinates.fromJson(json.decode(data));
    jobCubit.updateDriverCo(currentDriver.driverId, driverCurrentCoordinates);

    // Add Coordinates
    addDistances(Coordinates(
        lat: driverCurrentCoordinates.lat, lng: driverCurrentCoordinates.lng));
    // distanceTravelled.add(Coordinates(
    //     lat: driverCurrentCoordinates.lat, lng: driverCurrentCoordinates.lng));

    double distance = _googleMapsServices.calculateDistance(
        LatLng(driverCurrentCoordinates.lat, driverCurrentCoordinates.lng),
        LatLng(double.parse(currentDriver.baseLocation.lat),
            double.parse(currentDriver.baseLocation.lng)));

    distance < 0.05
        ? showToast('arrived_at_base'.tr(), 'you_have_arrived_at_base'.tr())
        : DoNothingAction();
  });
}

void _foregroundServiceFunction() async {
  // Get Driver Current Location
  Position value = await getGeoCurrentLocation(null);
  if (value == null) return;

  // print('Position Received: $value');

  // Change Notif Text
  // await ForegroundService.notification.startEditMode();
  // await ForegroundService.notification
  //     .setText("${value.latitude.toString()}, ${value.longitude.toString()}");
  // await ForegroundService.notification.finishEditMode();

  Coordinates driverCurrentCoordinates = Coordinates(
      lat: value.latitude,
      lng: value.longitude,
      timestamp: DateTime.now().toUtc().millisecondsSinceEpoch.toString());

  ForegroundService.sendToPort(
      "${json.encode(driverCurrentCoordinates.toJson())}");

  // print('Coordinates Sent');

  // BG Isolate
  if (!ForegroundService.isIsolateCommunicationSetup) {
    ForegroundService.setupIsolateCommunication((data) {
      // print("bg isolate received: $data");

      if (currentJob == null || currentDriver == null) return;

      final _googleMapsServices = GoogleMapsServices();

      // Send Driver Coordinates
      final driverCurrentCoordinates = Coordinates.fromJson(json.decode(data));
      jobCubit.updateDriverCo(currentDriver.driverId, driverCurrentCoordinates);

      // Add Coordinates
      addDistances(Coordinates(
          lat: driverCurrentCoordinates.lat,
          lng: driverCurrentCoordinates.lng));

      double distance = _googleMapsServices.calculateDistance(
          LatLng(driverCurrentCoordinates.lat, driverCurrentCoordinates.lng),
          LatLng(double.parse(currentDriver.baseLocation.lat),
              double.parse(currentDriver.baseLocation.lng)));

      distance < 0.05
          ? showToast('arrived_at_base'.tr(), 'you_have_arrived_at_base'.tr())
          : DoNothingAction();
    });
  }
}

class BaseMapsPage extends StatefulWidget {
  final String buttonText;
  final Color buttonColor;
  final Coordinates coordinates;
  final Function buttonFunction;
  final String title;

  const BaseMapsPage(
      {Key key,
      this.buttonText,
      this.buttonColor,
      this.coordinates,
      this.buttonFunction,
      this.title})
      : super(key: key);
  @override
  _BaseMapsPageState createState() => _BaseMapsPageState();
}

class _BaseMapsPageState extends State<BaseMapsPage> {
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;
  Position _currentLocation;
  LatLng _center;
  final Set<Marker> _markers = {};

  Location _location = new Location();
  final Set<Polyline> _polyLines = {};
  Set<Polyline> get polyLines => _polyLines;

  BitmapDescriptor truckIcon;

  final _googleMapServices = GoogleMapsServices();
  StreamSubscription<LocationData> _getPositionSubscription;

  bool showMapInit = false;

  // Remaining
  double remainingDistance = 0;
  // String remainingTime = "0 mins";

  // Mapbox
  // bool _arrived = false;
  bool _isMultipleStop = false;
  // double _distanceRemaining, _durationRemaining;
  // bool _routeBuilt = false;
  // bool _isNavigating = false;
  // String _instruction;

  // Change Variables
  String _buttonText;
  Color _buttonColor;
  Coordinates _coordinates;
  Function _buttonFunction;
  String _title;

  setVariables() {
    _buttonText = widget.buttonText;
    _buttonColor = widget.buttonColor;
    _coordinates = widget.coordinates;
    _buttonFunction = widget.buttonFunction;
    _title = widget.title;
  }

  // Add marker method
  void addMarkers(Marker _marker) {
    _markers.add(_marker);
  }

  // Create Route
  void createRoute(String encondedPoly) {
    _polyLines.add(Polyline(
        polylineId: PolylineId(_currentLocation.toString()),
        width: 6,
        endCap: Cap.roundCap,
        startCap: Cap.buttCap,
        jointType: JointType.mitered,
        points: _googleMapServices
            .convertToLatLng(_googleMapServices.decodePoly(encondedPoly)),
        color: Colors.blue));
  }

  void _onMapCreated(GoogleMapController controller) async {
    !_controller.isCompleted
        ? _controller.complete(controller)
        : DoNothingAction();
    mapController = await _controller.future;
  }

  void _moveCamera() async {
    final l = await getGeoCurrentLocation(null);
    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(l.latitude, l.longitude), zoom: 15)),
    );
  }

  void _setListener() {
    // Location Listener
    _getPositionSubscription =
        _location.onLocationChanged.listen((LocationData value) {
      if (value == null) return;

      _markers
          .removeWhere((element) => element.markerId == MarkerId("current"));

      _currentLocation = Position(
          accuracy: value.accuracy,
          altitude: value.altitude,
          heading: value.heading,
          speed: value.speed,
          speedAccuracy: value.speedAccuracy,
          latitude: value.latitude,
          longitude: value.longitude,
          timestamp: DateTime.fromMillisecondsSinceEpoch(value.time.toInt()));

      _center = LatLng(_currentLocation.latitude, _currentLocation.longitude);

      double distance = _googleMapServices.calculateDistance(
          _center, LatLng(_coordinates.lat, _coordinates.lng));

      remainingDistance = double.parse(distance.toString());
      _buttonFunction =
          remainingDistance < 5000.30 || appFlavor == FlavorType.Dev
              ? widget.buttonFunction
              : null;

      if (mounted)
        setState(() {
          addMarkers(Marker(
            markerId: MarkerId("current"),
            position: _center,
            icon: truckIcon,
          ));
        });
    });
  }

  Future<void> _onRouteEvent(e) async {
    // _distanceRemaining = await directions.distanceRemaining;
    // _durationRemaining = await directions.durationRemaining;

    switch (e.eventType) {
      case MapBoxEvent.progress_change:
        // var progressEvent = e.data as RouteProgressEvent;
        // _arrived = progressEvent.arrived;
        // if (progressEvent.currentStepInstruction != null)
        // _instruction = progressEvent.currentStepInstruction;
        break;
      case MapBoxEvent.route_building:
      case MapBoxEvent.route_built:
        // _routeBuilt = true;
        break;
      case MapBoxEvent.route_build_failed:
        // _routeBuilt = false;
        break;
      case MapBoxEvent.navigation_running:
        // _isNavigating = true;
        break;
      case MapBoxEvent.on_arrival:
        // _arrived = true;
        if (!_isMultipleStop) {
          await Future.delayed(Duration(seconds: 2));
          await directions.finishNavigation();
        } else {}
        break;
      case MapBoxEvent.navigation_finished:
      case MapBoxEvent.navigation_cancelled:
        // _routeBuilt = false;
        // _isNavigating = false;
        break;
      default:
        break;
    }
    //refresh UI
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    // Set Variables
    setVariables();

    // Mapbox
    directions = MapBoxNavigation(onRouteEvent: _onRouteEvent);

    // Set Truck Icon
    getBytesFromAsset('assets/truck_marker.png', 60)
        .then((value) => truckIcon = BitmapDescriptor.fromBytes(value));

    // Add Markers
    addMarkers(Marker(
      markerId: MarkerId("pickUp"),
      position: LatLng(_coordinates.lat, _coordinates.lng),
      icon: BitmapDescriptor.defaultMarker,
    ));

    // Get Driver Current Location
    getGeoCurrentLocation(null).then((value) async {
      if (value == null) return;

      _currentLocation = value;
      _center = LatLng(_currentLocation.latitude, _currentLocation.longitude);

      _googleMapServices
          .getRouteCoordinates(
              _center, LatLng(_coordinates.lat, _coordinates.lng))
          .then((value) {
        if (value == null) return;
        createRoute(value);
        if (mounted)
          setState(() {
            addMarkers(Marker(
              markerId: MarkerId("current"),
              position: _center,
              icon: truckIcon,
            ));
          });
      });
    });

    // Set Listeners
    _setListener();
    // startForegroundService();
    _maybeStartFGS();

    super.initState();
  }

  @override
  void dispose() async {
    _getPositionSubscription?.cancel();
    // await FlutterForegroundPlugin?.stopForegroundService();
    await ForegroundService.stopForegroundService();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Scaffold(
            appBar: AppBar(
              title: Text(
                'go_to_base'.tr(),
                style: Theme.of(context)
                    .accentTextTheme
                    .headline1
                    .copyWith(color: Colors.white),
              ),
              centerTitle: true,
              actions: [
                IconButton(
                  icon: Icon(FontAwesomeIcons.headset),
                  onPressed: () => setState(() {
                    makePhoneCall(helpline);
                  }),
                ),
              ],
            ),
            body: _center == null
                ? LoadingPage()
                : Container(
                    child: Stack(
                      children: [
                        // Google Map View
                        Align(
                          alignment: Alignment.center,
                          child: GoogleMap(
                            polylines: _polyLines,
                            markers: _markers,
                            onMapCreated: _onMapCreated,
                            initialCameraPosition: CameraPosition(
                              target: _center,
                              zoom: 15.0,
                              tilt: 30.0,
                            ),
                            myLocationEnabled: false,
                            myLocationButtonEnabled: false,
                            zoomGesturesEnabled: true,
                            zoomControlsEnabled: false,
                            tiltGesturesEnabled: true,
                            rotateGesturesEnabled: true,
                          ),
                        ),
                        // Top Field
                        Positioned(
                          right: 30,
                          left: 30,
                          top: 30,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(4),
                              border: Border.all(
                                width: 1,
                                color: Colors.grey,
                              ),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                // Left Side
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      child: Icon(
                                        FontAwesomeIcons.mapMarkerAlt,
                                        color: Theme.of(context).primaryColor,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Container(
                                      width: 200,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'base'.tr(),
                                            style: Theme.of(context)
                                                .accentTextTheme
                                                .bodyText2,
                                          ),
                                          Text(
                                            _coordinates.address,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 5,
                                            style: Theme.of(context)
                                                .accentTextTheme
                                                .headline2,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Text(
                                  '${remainingDistance.toStringAsFixed(2)} Km',
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline2
                                      .copyWith(
                                          color:
                                              Theme.of(context).primaryColor),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // Bottom Menu
                        showBottomMenu(
                            context,
                            _coordinates,
                            _center,
                            _buttonFunction,
                            null,
                            _buttonText,
                            _buttonColor,
                            _title,
                            _moveCamera,
                            null,
                            null,
                            null)
                      ],
                    ),
                  )));
  }
}
