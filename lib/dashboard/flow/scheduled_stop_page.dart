import 'dart:convert';

import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:foreground_service/foreground_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

//use an async method so we can await
void _maybeStartFGS() async {
  ///if the app was killed+relaunched, this function will be executed again
  ///but if the foreground service stayed alive,
  ///this does not need to be re-done
  if (await ForegroundService.foregroundServiceIsStarted())
    await ForegroundService.stopForegroundService();

  await ForegroundService.setServiceIntervalSeconds(900);

  //necessity of editMode is dubious (see function comments)
  await ForegroundService.notification.startEditMode();

  await ForegroundService.notification.setTitle("BridgeLinx Driver is Running");
  await ForegroundService.notification.setText("Updating Job Coordinates");

  await ForegroundService.notification.finishEditMode();

  await ForegroundService.setContinueRunningAfterAppKilled(true);

  await ForegroundService.startForegroundService(_foregroundServiceFunction);
  await ForegroundService.getWakeLock();

  ///this exists solely in the main app/isolate,
  ///so needs to be redone after every app kill+relaunch
  await ForegroundService.setupIsolateCommunication((data) async {
    // debugPrint("main received: $data");

    if (currentJob == null || currentDriver == null) return;

    try {
      // Send Driver Coordinates
      Coordinates driverCurrentCoordinates =
          Coordinates.fromJson(json.decode(data));

      currentJob.driverCurrentCoordinates = driverCurrentCoordinates;
      jobCubit.updateCoordinates(
          currentJob.jobId, currentDriver.driverId, driverCurrentCoordinates);
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }
  });
}

void _foregroundServiceFunction() async {
  // Get Driver Current Location
  Position value = await getGeoCurrentLocation(null);
  if (value == null) return;

  Coordinates driverCurrentCoordinates = Coordinates(
      lat: value.latitude,
      lng: value.longitude,
      timestamp: DateTime.now().toUtc().millisecondsSinceEpoch.toString());

  ForegroundService.sendToPort(
      "${json.encode(driverCurrentCoordinates.toJson())}");

  if (!ForegroundService.isIsolateCommunicationSetup) {
    ForegroundService.setupIsolateCommunication((data) {
      if (currentJob == null || currentDriver == null) return;

      try {
        // Send Driver Coordinates
        Coordinates driverCurrentCoordinates =
            Coordinates.fromJson(json.decode(data));

        currentJob.driverCurrentCoordinates = driverCurrentCoordinates;
        jobCubit.updateCoordinates(
            currentJob.jobId, currentDriver.driverId, driverCurrentCoordinates);
      } catch (e) {
        Fluttertoast.showToast(msg: e.toString());
      }
    });
  }
}

class ScheduledStopPage extends StatefulWidget {
  final Job job;
  final Journey journey;
  final String buttonText;
  final String title;

  const ScheduledStopPage(
      {Key key, this.buttonText, this.title, this.job, this.journey})
      : super(key: key);

  @override
  _ScheduledStopPageState createState() => _ScheduledStopPageState();
}

class _ScheduledStopPageState extends State<ScheduledStopPage> {
  Job _job;
  Journey _journey;
  String _buttonText;
  String _title;

  Timestamp _checkInTime;
  Timestamp _checkOutTime;
  String _gifUrl;

  bool _buttonPressed;
  bool _loopActive;
  double _progress;

  @override
  void initState() {
    _job = widget.job;
    _journey = widget.journey;
    _buttonText = widget.buttonText;
    _title = widget.title;

    _checkInTime = Timestamp.fromDate(DateTime.fromMillisecondsSinceEpoch(
        int.parse(_journey.toStop.reachTime['estimated'].toString())));
    _checkOutTime = Timestamp.fromDate(DateTime.fromMillisecondsSinceEpoch(
        int.parse(_journey.toStop.leaveTime['estimated'].toString())));
    _gifUrl = 'assets/sleeping.gif';

    _buttonPressed = false;
    _loopActive = false;
    _progress = 0;

    _maybeStartFGS();

    super.initState();
  }

  @override
  void dispose() async {
    await ForegroundService?.stopForegroundService();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          _title,
          style: Theme.of(context)
              .accentTextTheme
              .headline1
              .copyWith(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: Icon(FontAwesomeIcons.headset),
            onPressed: () => setState(() {
              makePhoneCall(helpline);
            }),
          ),
        ],
      ),
      body: WillPopScope(
        onWillPop: () async => false,
        child: Stack(
          fit: StackFit.expand,
          overflow: Overflow.visible,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(4),
                            topRight: Radius.circular(4),
                          ),
                          color: Colors.white,
                          border: Border.all(color: Colors.grey, width: 1),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Icon(
                              FontAwesomeIcons.clock,
                              color: Colors.black54,
                              size: 20,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                              'checkin_time'.tr(args: [
                                DateFormat.jm()
                                    .format(_checkInTime.toDate())
                                    .toString()
                              ]),
                              style:
                                  Theme.of(context).accentTextTheme.headline5,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(4),
                            bottomRight: Radius.circular(4),
                          ),
                          color: Colors.white,
                          border: Border.all(color: Colors.grey, width: 1),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Icon(
                              FontAwesomeIcons.clock,
                              color: Colors.red,
                              size: 20,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                              'checkout_time'.tr(args: [
                                DateFormat.jm()
                                    .format(_checkOutTime.toDate())
                                    .toString()
                              ]),
                              style: Theme.of(context)
                                  .accentTextTheme
                                  .headline5
                                  .copyWith(color: Colors.red),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 15),
                    child: Image.asset(
                      _gifUrl,
                      height: 350.0,
                      width: 350.0,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(),
                )
              ],
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                margin: EdgeInsets.all(30),
                child: StatefulBuilder(
                  builder: (context, setButtonState) => Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Visibility(
                        visible: _buttonPressed,
                        child: Container(
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.lightGreen[50]),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              (Text(
                                'keep_button_pressed'.tr(),
                                style:
                                    Theme.of(context).accentTextTheme.headline1,
                              )),
                              SizedBox(
                                height: 8,
                              ),
                              CircularPercentIndicator(
                                radius: 100.0,
                                lineWidth: 10.0,
                                percent: _progress >= 1.0 ? 1.0 : _progress,
                                center: Text(
                                  (_progress * 100).toStringAsFixed(0),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .bodyText2,
                                ),
                                progressColor: Theme.of(context).primaryColor,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Listener(
                        onPointerDown: (val) async {
                          _buttonPressed = true;
                          DateTime _pressedTime = DateTime.now();

                          if (_loopActive) return;

                          _loopActive = true;

                          while (_buttonPressed) {
                            if (DateTime.now()
                                    .difference(_pressedTime)
                                    .inSeconds >=
                                2) {
                              // Fluttertoast.showToast(msg: 'Complete');
                              jobCubit.exitStop(_job, _journey);
                              _buttonPressed = false;
                              return;
                            }
                            if (mounted)
                              setButtonState(() {
                                _progress += 0.1;
                              });

                            await Future.delayed(Duration(milliseconds: 200));
                          }

                          _loopActive = false;
                        },
                        onPointerUp: (val) {
                          if (mounted)
                            setButtonState(() {
                              _progress = 0;
                              _buttonPressed = false;
                            });
                        },
                        child: Container(
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xFF00C78B),
                          ),
                          child: Center(
                            child: Text(
                              _buttonText,
                              style: Theme.of(context)
                                  .accentTextTheme
                                  .button
                                  .copyWith(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
