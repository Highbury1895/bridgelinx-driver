import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:bridgelinx_driver/utils/size_config.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class JobShowImagesPage extends StatefulWidget {
  final Job job;
  final Journey journey;

  const JobShowImagesPage({Key key, this.job, this.journey}) : super(key: key);
  @override
  _JobShowImagesPageState createState() => _JobShowImagesPageState();
}

class _JobShowImagesPageState extends State<JobShowImagesPage> {
  Job _job;
  Journey _journey;
  Package packageInfo;
  List<String> _clientImages;

  @override
  void initState() {
    _job = widget.job;
    _journey = widget.journey;
    packageInfo = Package.fromJson(_job.packageInfoJourney[_journey.toRef]);
    _clientImages = List.from(_job.clientImagesJourney[_journey.toRef] ?? []);
    jobDialog = true;
    addGatepass();
    super.initState();
  }

  addGatepass() async {
    final gatepass = await getGatepass();
    gatepass != null && mounted
        ? setState(() {
            _clientImages.add(gatepass);
          })
        : DoNothingAction();
  }

  Dialog _imageDialog(String url) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        width: 500,
        height: 500,
        padding: EdgeInsets.all(15),
        color: Colors.blueGrey[100],
        child: CachedNetworkImage(
          fit: BoxFit.fitHeight,
          imageUrl: url,
          progressIndicatorBuilder: (context, url, downloadProgress) => Center(
              child:
                  CircularProgressIndicator(value: downloadProgress.progress)),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      ),
    );
  }

  void _showImageDialog(BuildContext context, String url) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) => _imageDialog(url),
    );
  }

  @override
  void dispose() {
    _clientImages.clear();
    jobDialog = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'order_details'.tr(),
          style: Theme.of(context)
              .accentTextTheme
              .headline1
              .copyWith(color: Colors.white),
        ),
        automaticallyImplyLeading: true,
        centerTitle: true,
      ),
      body: Container(
        height: SizeConfig.screenHeight,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              flex: 11,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    flex: 6,
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.all(28),
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          color: Colors.blue[50],
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'type_of_freight'.tr(),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline5
                                      .copyWith(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                      ),
                                ),
                                SizedBox(height: 20),
                                Text(
                                  packageInfo.typeOfFreight,
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline5
                                      .copyWith(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                      ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'no_of_units'.tr(),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline5
                                      .copyWith(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                      ),
                                ),
                                Text(
                                  packageInfo.noOfUnits.toString(),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline5
                                      .copyWith(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                      ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'freight_details'.tr(),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline5
                                      .copyWith(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                      ),
                                ),
                                Text(
                                  packageInfo.freightDetails,
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline5
                                      .copyWith(
                                        color:
                                            Theme.of(context).primaryColorDark,
                                      ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 10,
                    child: Container(
                      color: Colors.white,
                      child: SingleChildScrollView(
                        child: Container(
                          padding: EdgeInsets.all(30),
                          child: _clientImages.length <= 0
                              ? Center(
                                  child: Text(
                                    'no_images_found'.tr(),
                                    style: Theme.of(context)
                                        .accentTextTheme
                                        .headline5,
                                  ),
                                )
                              : GridView.count(
                                  shrinkWrap: true,
                                  physics: ScrollPhysics(),
                                  crossAxisCount: 2,
                                  childAspectRatio: 1.0,
                                  padding: const EdgeInsets.all(10.0),
                                  mainAxisSpacing: 20.0,
                                  crossAxisSpacing: 20.0,
                                  children: _clientImages.map((String url) {
                                    return InkWell(
                                      onTap: () =>
                                          _showImageDialog(context, url),
                                      child: GridTile(
                                        child: CachedNetworkImage(
                                          fit: BoxFit.cover,
                                          imageUrl: url,
                                          progressIndicatorBuilder: (context,
                                                  url, downloadProgress) =>
                                              Center(
                                            child: CircularProgressIndicator(
                                                value:
                                                    downloadProgress.progress),
                                          ),
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error),
                                        ),
                                      ),
                                    );
                                  }).toList(),
                                ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.all(30),
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  color: Color(0xFF00C78B),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Center(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: Text(
                        'okay'.tr(),
                        style: Theme.of(context)
                            .accentTextTheme
                            .headline1
                            .copyWith(
                              color: Colors.white,
                            ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
