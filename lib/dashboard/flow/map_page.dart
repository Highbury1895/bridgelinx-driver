import 'dart:async';
import 'dart:convert';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:bridgelinx_driver/dashboard/flow/job_show_images.dart';
import 'package:bridgelinx_driver/utils/google_services.dart';
import 'package:bridgelinx_driver/utils/loading.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_foreground_plugin/flutter_foreground_plugin.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_mapbox_navigation/library.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:foreground_service/foreground_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:location/location.dart';
import 'package:bridgelinx_driver/DTO/stops.dart';

enum DialogType {
  Start,
  ExitPick,
  ExitDrop,
  ExitStop,
  Loaders,
  UnLoaders,
  None
}

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void startForegroundService() async {
  await FlutterForegroundPlugin?.stopForegroundService();
  await FlutterForegroundPlugin?.setServiceMethodInterval(seconds: 5);
  await FlutterForegroundPlugin?.setServiceMethod(globalForegroundService);
  await FlutterForegroundPlugin?.startForegroundService(
    holdWakeLock: true,
    onStarted: () {
      // print("Foreground on Started");
    },
    onStopped: () {
      // print("Foreground on Stopped");
    },
    title: "BridgeLinx Driver",
    content: "Updating Coordinates",
    iconName: "ic_stat_hot_tub",
    subtext: "",
  );
}

void globalForegroundService() async {
  // Get Driver Current Location
  if (currentJob == null || currentJob.jobId == null) return;

  getGeoCurrentLocation(null).then((value) {
    if (value == null) return;

    final _googleMapsServices = GoogleMapsServices();
    // Send Driver Coordinates
    Coordinates driverCurrentCoordinates = Coordinates(
        lat: value.latitude,
        lng: value.longitude,
        timestamp: DateTime.now().toUtc().millisecondsSinceEpoch.toString());
    jobCubit.updateCoordinates(
        currentJob.jobId, currentDriver.driverId, driverCurrentCoordinates);

    // Add Coordinates
    addDistances(Coordinates(
        lat: driverCurrentCoordinates.lat, lng: driverCurrentCoordinates.lng));

    currentJob.driverCurrentCoordinates = driverCurrentCoordinates;

    if (currentJob.driverStatus.status == DriverStatus.driverMovingToPickup) {
      double distance = _googleMapsServices.calculateDistance(
          LatLng(driverCurrentCoordinates.lat, driverCurrentCoordinates.lng),
          LatLng(currentJob.clientCoordinates.pickup.lat,
              currentJob.clientCoordinates.pickup.lng));

      distance < 0.05
          ? showToast(
              'arrived_at_pickup'.tr(), 'you_have_arrived_at_pickup'.tr())
          : DoNothingAction();
    } else if (currentJob.driverStatus.status ==
        DriverStatus.driverMovingToDropOff) {
      double distance = _googleMapsServices.calculateDistance(
          LatLng(driverCurrentCoordinates.lat, driverCurrentCoordinates.lng),
          LatLng(currentJob.clientCoordinates.dropoff.lat,
              currentJob.clientCoordinates.dropoff.lng));

      distance < 0.05
          ? showToast(
              'arrived_at_dropoff'.tr(), 'you_have_arrived_at_dropoff'.tr())
          : DoNothingAction();
    }
  });
}

//use an async method so we can await
void _maybeStartFGS() async {
  ///if the app was killed+relaunched, this function will be executed again
  ///but if the foreground service stayed alive,
  ///this does not need to be re-done
  if (await ForegroundService.foregroundServiceIsStarted())
    await ForegroundService.stopForegroundService();

  await ForegroundService.setServiceIntervalSeconds(5);

  //necessity of editMode is dubious (see function comments)
  await ForegroundService.notification.startEditMode();

  await ForegroundService.notification.setTitle("BridgeLinx Driver is Running");
  await ForegroundService.notification.setText("Updating Job Coordinates");

  await ForegroundService.notification.finishEditMode();

  await ForegroundService.setContinueRunningAfterAppKilled(true);

  await ForegroundService.startForegroundService(_foregroundServiceFunction);
  await ForegroundService.getWakeLock();

  ///this exists solely in the main app/isolate,
  ///so needs to be redone after every app kill+relaunch
  await ForegroundService.setupIsolateCommunication((data) async {
    // debugPrint("main received: $data");

    if (currentJob == null || currentDriver == null) return;

    try {
      final _googleMapsServices = GoogleMapsServices();

      // Send Driver Coordinates
      Coordinates driverCurrentCoordinates =
          Coordinates.fromJson(json.decode(data));

      currentJob.driverCurrentCoordinates = driverCurrentCoordinates;
      jobCubit.updateCoordinates(
          currentJob.jobId, currentDriver.driverId, driverCurrentCoordinates);

      // Add Coordinates
      addDistances(Coordinates(
          lat: driverCurrentCoordinates.lat,
          lng: driverCurrentCoordinates.lng));

      if (currentJob.driverStatus.status == DriverStatus.driverMovingToPickup ||
          currentJob.driverStatus.status ==
              DriverStatus.driverMovingToDropOff) {
        double distance = _googleMapsServices.calculateDistance(
            LatLng(driverCurrentCoordinates.lat, driverCurrentCoordinates.lng),
            LatLng(currentJourney.toCoordinates.lat,
                currentJourney.toCoordinates.lng));

        if (currentJob.driverStatus.status ==
            DriverStatus.driverMovingToPickup) {
          distance < 0.05
              ? showToast(
                  'arrived_at_pickup'.tr(), 'you_have_arrived_at_pickup'.tr())
              : DoNothingAction();
        } else if (currentJob.driverStatus.status ==
            DriverStatus.driverMovingToDropOff) {
          distance < 0.05
              ? showToast(
                  'arrived_at_dropoff'.tr(), 'you_have_arrived_at_dropoff'.tr())
              : DoNothingAction();
        } else if (currentJob.driverStatus.status ==
            DriverStatus.driverMovingToStop) {
          distance < 0.05
              ? showToast(
                  'arrived_at_stop'.tr(), 'you_have_arrived_at_stop'.tr())
              : DoNothingAction();
        }
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }
  });
}

void _foregroundServiceFunction() async {
  // Get Driver Current Location
  Position value = await getGeoCurrentLocation(null);
  if (value == null) return;

  // print('Position Received: $value');

  // Change Notif Text
  // await ForegroundService.notification.startEditMode();
  // await ForegroundService.notification
  //     .setText("${value.latitude.toString()}, ${value.longitude.toString()}");
  // await ForegroundService.notification.finishEditMode();

  Coordinates driverCurrentCoordinates = Coordinates(
      lat: value.latitude,
      lng: value.longitude,
      timestamp: DateTime.now().toUtc().millisecondsSinceEpoch.toString());

  ForegroundService.sendToPort(
      "${json.encode(driverCurrentCoordinates.toJson())}");

  // print('Coordinates Sent');

  if (!ForegroundService.isIsolateCommunicationSetup) {
    ForegroundService.setupIsolateCommunication((data) {
      // print("bg isolate received: $data");

      if (currentJob == null || currentDriver == null) return;

      try {
        final _googleMapsServices = GoogleMapsServices();

        // Send Driver Coordinates
        Coordinates driverCurrentCoordinates =
            Coordinates.fromJson(json.decode(data));

        currentJob.driverCurrentCoordinates = driverCurrentCoordinates;
        jobCubit.updateCoordinates(
            currentJob.jobId, currentDriver.driverId, driverCurrentCoordinates);

        // Add Coordinates
        addDistances(Coordinates(
            lat: driverCurrentCoordinates.lat,
            lng: driverCurrentCoordinates.lng));
        // distanceTravelled.add(Coordinates(
        //     lat: driverCurrentCoordinates.lat, lng: driverCurrentCoordinates.lng));

        if (currentJob.driverStatus.status ==
                DriverStatus.driverMovingToPickup ||
            currentJob.driverStatus.status ==
                DriverStatus.driverMovingToDropOff) {
          double distance = _googleMapsServices.calculateDistance(
              LatLng(
                  driverCurrentCoordinates.lat, driverCurrentCoordinates.lng),
              LatLng(currentJourney.toCoordinates.lat,
                  currentJourney.toCoordinates.lng));

          if (currentJob.driverStatus.status ==
              DriverStatus.driverMovingToPickup) {
            distance < 0.05
                ? showToast(
                    'arrived_at_pickup'.tr(), 'you_have_arrived_at_pickup'.tr())
                : DoNothingAction();
          } else if (currentJob.driverStatus.status ==
              DriverStatus.driverMovingToDropOff) {
            distance < 0.05
                ? showToast('arrived_at_dropoff'.tr(),
                    'you_have_arrived_at_dropoff'.tr())
                : DoNothingAction();
          } else if (currentJob.driverStatus.status ==
              DriverStatus.driverMovingToStop) {
            distance < 0.05
                ? showToast(
                    'arrived_at_stop'.tr(), 'you_have_arrived_at_stop'.tr())
                : DoNothingAction();
          }
        }
      } catch (e) {
        Fluttertoast.showToast(msg: e.toString());
      }
    });
  }
}

class JobMapsPage extends StatefulWidget {
  final Job job;
  final String buttonText;
  final Color buttonColor;
  final bool showMap;
  final bool showTopField;
  final Coordinates coordinates;
  final Function buttonFunction;
  final Function docFunction;
  final DialogType dialogType;
  final String title;
  final String appBarTitle;
  final String gifUrl;
  final Journey journey;

  const JobMapsPage({
    Key key,
    this.job,
    this.buttonText,
    this.buttonColor,
    this.coordinates,
    this.buttonFunction,
    this.docFunction,
    this.showMap,
    this.showTopField,
    this.dialogType,
    this.title,
    this.appBarTitle,
    this.gifUrl,
    this.journey,
  }) : super(key: key);
  @override
  _JobMapsPageState createState() => _JobMapsPageState();
}

class _JobMapsPageState extends State<JobMapsPage> with WidgetsBindingObserver {
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;
  Position _currentLocation;
  LatLng _center;
  final Set<Marker> _markers = {};

  Location _location = new Location();
  final Set<Polyline> _polyLines = {};
  Set<Polyline> get polyLines => _polyLines;

  BitmapDescriptor truckIcon;

  final _googleMapServices = GoogleMapsServices();
  StreamSubscription<LocationData> _getPositionSubscription;

  // Timer _timer;
  bool initRun;

  bool showMapInit = false;

  // Remaining
  double remainingDistance = 0;
  // String remainingTime = "0 mins";

  //Times
  double _eta;
  bool _etaSent;

  // Mapbox
  // bool _arrived = false;
  bool _isMultipleStop = false;
  double _distanceRemaining, _durationRemaining;
  // bool _routeBuilt = false;
  // bool _isNavigating = false;
  // bool _reroute = true;
  // Timestamp _rerouteTime;
  // String _instruction;
  Timestamp _remainingSent;

  // State Change Variables
  Job _job;
  String _buttonText;
  Color _buttonColor;
  bool _showMap = false;
  bool _showTopField = true;
  Coordinates _coordinates;
  Function _buttonFunction;
  Function _docFunction;
  DialogType _dialogType;
  String _title;
  String _appBarTitle;
  String _gifUrl;
  Journey _journey;

  setVariables() async {
    _job = widget.job;
    _buttonText = widget.buttonText;
    _buttonColor = widget.buttonColor;
    _showMap = widget.showMap;
    _showTopField = widget.showTopField;
    _coordinates = widget.coordinates;
    _buttonFunction =
        _job.driverStatus.status == DriverStatus.driverMovingToPickup ||
                _job.driverStatus.status == DriverStatus.driverMovingToDropOff
            ? null
            : widget.buttonFunction;
    _docFunction = widget.docFunction;
    _dialogType = widget.dialogType;
    _title = widget.title;
    _appBarTitle = widget.appBarTitle;
    _gifUrl = widget.gifUrl;
    _journey = widget.journey;
    _eta = _journey.eta != null
        ? double.parse(_journey.eta.toString()) * 0.8
        : null;
    _etaSent = false;
    _remainingSent = Timestamp.now();
    // _rerouteTime = Timestamp.now();
  }

  // Add marker method
  void addMarkers(Marker _marker) {
    _markers.add(_marker);
  }

  // Create Route
  void createRoute(String encondedPoly) {
    _polyLines.add(Polyline(
        polylineId: PolylineId(_currentLocation.toString()),
        width: 6,
        endCap: Cap.roundCap,
        startCap: Cap.buttCap,
        jointType: JointType.mitered,
        points: _googleMapServices
            .convertToLatLng(_googleMapServices.decodePoly(encondedPoly)),
        color: Colors.blue));
  }

  void _onMapCreated(GoogleMapController controller) async {
    !_controller.isCompleted
        ? _controller.complete(controller)
        : DoNothingAction();
    mapController = await _controller.future;
  }

  void _moveCamera() {
    mapController.animateCamera(
      CameraUpdate.newCameraPosition(CameraPosition(target: _center, zoom: 15)),
    );
  }

  void _setListener() {
    // Location Listener
    _getPositionSubscription =
        _location.onLocationChanged.listen((LocationData value) {
      if (value == null) return;

      _markers
          .removeWhere((element) => element.markerId == MarkerId("current"));

      _currentLocation = Position(
          accuracy: value.accuracy,
          altitude: value.altitude,
          heading: value.heading,
          speed: value.speed,
          speedAccuracy: value.speedAccuracy,
          latitude: value.latitude,
          longitude: value.longitude,
          timestamp: DateTime.fromMillisecondsSinceEpoch(value.time.toInt()));

      _center = LatLng(_currentLocation.latitude, _currentLocation.longitude);

      double distance = _googleMapServices.calculateDistance(
          _center, LatLng(_coordinates.lat, _coordinates.lng));

      remainingDistance = double.parse(distance.toString());

      if ((_job.driverStatus.status == DriverStatus.driverMovingToPickup ||
              _job.driverStatus.status == DriverStatus.driverMovingToDropOff) &&
          _eta != null) {
        DateTime.now()
                        .difference(_job.driverStatus.timestamp.toDate())
                        .inSeconds >
                    _eta ||
                remainingDistance < 0.10
            ? _buttonFunction = widget.buttonFunction
            : _buttonFunction = null;

        // print(
        //     'Difference Left: ${DateTime.now().difference(_job.driverStatus.timestamp.toDate()).inSeconds}');

        // Check ETA
        if (!_etaSent && _job != null && _eta != null) {
          Timestamp startTime = _job.driverStatus.timestamp;
          int difference =
              Timestamp.now().toDate().difference(startTime.toDate()).inSeconds;

          if (difference > (_eta * 1.3)) {
            _etaSent = true;
            jobCubit.jobETA130(_job, _journey, currentDriver, Timestamp.now());
          }
        }
      } else if (_job.driverStatus.status == DriverStatus.driverMovingToStop) {
        remainingDistance < 0.20 || appFlavor == FlavorType.Dev
            ? _buttonFunction = widget.buttonFunction
            : _buttonFunction = null;

        // Check ETA
        if (!_etaSent && _job != null && _eta != null) {
          Timestamp startTime = _job.driverStatus.timestamp;
          int difference =
              Timestamp.now().toDate().difference(startTime.toDate()).inSeconds;

          if (difference > (_eta * 1.3)) {
            _etaSent = true;
            jobCubit.jobETA130(_job, _journey, currentDriver, Timestamp.now());
          }
        }
      }

      if (mounted)
        setState(() {
          addMarkers(Marker(
            markerId: MarkerId("current"),
            position: _center,
            icon: truckIcon,
          ));
        });
    });
  }

  // Show Modal Dialog at start
  _showBottomDialog() {
    if (!initRun) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        initRun = true;
        if (!_showMap) {
          if ((_dialogType == DialogType.Start ||
                  _dialogType == DialogType.ExitPick ||
                  _dialogType == DialogType.ExitDrop ||
                  _dialogType == DialogType.ExitStop) &&
              !jobDialog) {
            showBottomDialog(context, _dialogType, _job, _journey);
          } else if ((_dialogType == DialogType.Loaders ||
                  _dialogType == DialogType.UnLoaders) &&
              !jobDialog) {
            showLoaderDialog(context, _dialogType, _job, _journey);
          } else {
            return;
          }
        }
      });
    }
  }

  _showDocumentsPage() {
    if (!initRun) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        initRun = true;
        if ((_job.driverStatus.status == DriverStatus.driverArrived ||
                _job.driverStatus.status ==
                    DriverStatus.driverReachedDropOff) &&
            _dialogType == DialogType.None) {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => JobShowImagesPage(
                    job: _job,
                    journey: _journey,
                  )));
        }
      });
    }
  }

  Future<void> _onRouteEvent(e) async {
    _distanceRemaining = await directions.distanceRemaining;
    _durationRemaining = await directions.durationRemaining;

    if (DateTime.now().difference(_remainingSent.toDate()).inSeconds > 5) {
      _remainingSent = Timestamp.now();
      _job.times[_journey.id]['remaining'] = _durationRemaining?.toInt() ?? 0;
      _job.distances[_journey.id]['remaining'] =
          _distanceRemaining?.toInt() ?? 0;
      jobCubit.updateDistancesAndTimes(_job);
      // print(
      //     'Remaining Time: $_durationRemaining, Distance $_distanceRemaining, Timestamp ${DateTime.now()}');
    }

    switch (e.eventType) {
      case MapBoxEvent.progress_change:
        // var progressEvent = e.data as RouteProgressEvent;
        // _arrived = progressEvent.arrived;
        // if (progressEvent.currentStepInstruction != null)
        //   _instruction = progressEvent.currentStepInstruction;
        break;
      case MapBoxEvent.route_building:
      case MapBoxEvent.route_built:
        // _routeBuilt = true;
        break;
      case MapBoxEvent.route_build_failed:
        // _routeBuilt = false;
        break;
      case MapBoxEvent.navigation_running:
        // _isNavigating = true;
        break;
      case MapBoxEvent.on_arrival:
        // _arrived = true;
        if (!_isMultipleStop) {
          await Future.delayed(Duration(seconds: 2));
          await directions.finishNavigation();
        } else {}
        break;
      case MapBoxEvent.navigation_finished:
      case MapBoxEvent.navigation_cancelled:
        // _routeBuilt = false;
        // _isNavigating = false;
        /*
        if (_reroute) {
          _reroute = false;
          launchMapBox(_center, LatLng(_coordinates.lat, _coordinates.lng),
              _title.split(' ').first);
        }
        */
        break;
      case MapBoxEvent.user_off_route:
        // _reroute = true;
        // if (_reroute &&
        //     DateTime.now().difference(_rerouteTime.toDate()).inSeconds > 15) {
        //   _rerouteTime = Timestamp.now();
        //   _reroute = false;
        //   print('User Off Route');
        //   launchMapBox(_center, LatLng(_coordinates.lat, _coordinates.lng),
        //       _title.split(' ').first);
        // }
        break;
      default:
        break;
    }
    //refresh UI
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    initRun = false;

    // Mapbox
    directions = MapBoxNavigation(onRouteEvent: _onRouteEvent);

    // Set Variables
    setVariables();

    // Show Dialog
    _showBottomDialog();

    //Show Doc Page
    _showDocumentsPage();

    // Set Truck Icon
    getBytesFromAsset('assets/truck_marker.png', 60)
        .then((value) => truckIcon = BitmapDescriptor.fromBytes(value));

    // Add Markers
    addMarkers(Marker(
      markerId: MarkerId(_journey.journeyType == JourneyType.Pickup
          ? "pickUp"
          : _journey.journeyType == JourneyType.Dropoff
              ? "dropoff"
              : "stop"),
      position: LatLng(_coordinates.lat, _coordinates.lng),
      icon: BitmapDescriptor.defaultMarker,
    ));

    // Get Driver Current Location
    getGeoCurrentLocation(_journey).then((value) async {
      if (value == null) return;

      _currentLocation = value;
      _center = LatLng(_currentLocation.latitude, _currentLocation.longitude);

      // TODO: Change Times Method
      // Set Times
      /*
      if (_eta == null) {
        Job job = await _googleMapServices.getRouteTimes(
            _center,
            LatLng(_job.clientCoordinates.pickup.lat,
                _job.clientCoordinates.pickup.lng),
            LatLng(_job.clientCoordinates.pickup.lat,
                _job.clientCoordinates.pickup.lng),
            LatLng(_job.clientCoordinates.dropoff.lat,
                _job.clientCoordinates.dropoff.lng),
            _job);
        _job = job;
        _journey.eta = int.tryParse(_job.times[_journey.id].toString());
        _eta =
            _journey.eta != null ? double.parse(_journey.eta.toString()) : null;
      }
      */

      _googleMapServices
          .getRouteCoordinates(
              _center, LatLng(_coordinates.lat, _coordinates.lng))
          .then((value) {
        if (value == null) return;
        if (_showMap) createRoute(value);

        if (mounted)
          setState(() {
            addMarkers(Marker(
              markerId: MarkerId("current"),
              position: _center,
              icon: truckIcon,
            ));
          });
      });
    });

    // Set Listeners
    if (_showMap) _setListener();
    if (_showMap) _maybeStartFGS();

    if (_job.currentStop != null)
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (mounted)
          showWaitingDialog(
              context,
              stopsList.firstWhere((e) => e.type == _job.currentStop.stopType,
                  orElse: () => stopsList[0]),
              _job,
              _journey,
              currentDriver);
      });

    super.initState();
  }

  @override
  void dispose() async {
    WidgetsBinding.instance.removeObserver(this);
    _getPositionSubscription?.cancel();
    await ForegroundService?.stopForegroundService();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            _appBarTitle ?? '',
            style: Theme.of(context)
                .accentTextTheme
                .headline1
                .copyWith(color: Colors.white),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              icon: Icon(FontAwesomeIcons.headset),
              onPressed: () => setState(() {
                makePhoneCall(helpline);
              }),
            ),
          ],
        ),
        body: _center == null
            ? LoadingPage()
            : !_showMap
                ? GoogleMap(
                    initialCameraPosition: CameraPosition(
                      target: _center,
                      zoom: 15.0,
                    ),
                    onMapCreated: _onMapCreated,
                  )
                : Container(
                    child: Stack(
                      children: [
                        // Google Map View
                        Align(
                          alignment: Alignment.center,
                          child: GoogleMap(
                            trafficEnabled: true,
                            polylines: _polyLines,
                            markers: _markers,
                            onMapCreated: _onMapCreated,
                            initialCameraPosition: CameraPosition(
                              target: _center,
                              zoom: 15.0,
                              tilt: 30.0,
                            ),
                            myLocationEnabled: false,
                            myLocationButtonEnabled: false,
                            zoomGesturesEnabled: true,
                            zoomControlsEnabled: false,
                            tiltGesturesEnabled: true,
                            rotateGesturesEnabled: true,
                          ),
                        ),
                        // Top Field
                        Visibility(
                          visible: _showTopField,
                          child: Positioned(
                            right: 30,
                            left: 30,
                            top: 30,
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(4),
                                    border: Border.all(
                                      width: 1,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      // Left Side
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.all(5),
                                            child: Icon(
                                              FontAwesomeIcons.mapMarkerAlt,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Container(
                                            width: 200,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  _title.split(' ').first,
                                                  style: Theme.of(context)
                                                      .accentTextTheme
                                                      .bodyText2,
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  _coordinates.address ?? '',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 5,
                                                  style: Theme.of(context)
                                                      .accentTextTheme
                                                      .headline2,
                                                ),
                                                Text(
                                                  _coordinates
                                                          .formattedAddress ??
                                                      '',
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 5,
                                                  style: Theme.of(context)
                                                      .accentTextTheme
                                                      .headline2,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Text(
                                        '${remainingDistance.toStringAsFixed(2)} Km',
                                        style: Theme.of(context)
                                            .accentTextTheme
                                            .headline2
                                            .copyWith(
                                                color: Theme.of(context)
                                                    .primaryColor),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(vertical: 10),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(),
                                      RaisedButton.icon(
                                        color: Colors.red,
                                        padding: EdgeInsets.all(10),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(18.0),
                                        ),
                                        elevation: 5,
                                        icon: Image.asset(
                                          'assets/stops/stop.png',
                                          height: 20,
                                          width: 20,
                                        ),
                                        label: Text(
                                          'emergency_stop'.tr(),
                                          style: Theme.of(context)
                                              .accentTextTheme
                                              .headline1
                                              .copyWith(color: Colors.white),
                                        ),
                                        onPressed: () {
                                          showUnscheduledStops(context, _job,
                                              _journey, currentDriver);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // Bottom Menu
                        showBottomMenu(
                            context,
                            _coordinates,
                            _center,
                            _buttonFunction,
                            _docFunction,
                            _buttonText,
                            _buttonColor,
                            _title,
                            _moveCamera,
                            _job.orderNumber,
                            _gifUrl,
                            _journey)
                      ],
                    ),
                  ),
      ),
    );
  }
}
