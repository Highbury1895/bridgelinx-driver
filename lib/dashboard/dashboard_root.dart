import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/cubit/job_cubit.dart';
import 'package:bridgelinx_driver/dashboard/flow/camera_page.dart';
import 'package:bridgelinx_driver/dashboard/flow/cash_collect_page.dart';
import 'package:bridgelinx_driver/dashboard/dashboard_page.dart';
import 'package:bridgelinx_driver/dashboard/flow/job_complete_page.dart';
import 'package:bridgelinx_driver/dashboard/flow/map_page.dart';
import 'package:bridgelinx_driver/dashboard/flow/job_show_images.dart';
import 'package:bridgelinx_driver/dashboard/flow/map_page_base.dart';
import 'package:bridgelinx_driver/dashboard/jobs/jobs_list_page.dart';
import 'package:bridgelinx_driver/errors/error_page.dart';
import 'package:bridgelinx_driver/utils/fcm_subscription.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:bridgelinx_driver/utils/loading.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'flow/scheduled_stop_page.dart';
import 'jobs/job_details_page.dart';
import 'package:easy_localization/easy_localization.dart';

class DashBoardRoot extends StatefulWidget {
  @override
  _DashBoardRootState createState() => _DashBoardRootState();
}

class _DashBoardRootState extends State<DashBoardRoot> {
  @override
  void initState() {
    // Create Job
    // jobCubit.createJob();

    saveDeviceToken();

    FirebaseCrashlytics.instance.setUserIdentifier(
        FirebaseAuth.instance.currentUser.phoneNumber.toString());

    driverListener();

    super.initState();
  }

  driverListener() {
    Stream<DocumentSnapshot> driverQuery = loginCubit.driverListener();
    driverQuery.listen((data) async {
      Map snap = data.data();

      // Top Level Conditions
      if (currentJob != snap['currentJob'] && snap['currentJob'] == null) {
        if (currentDriver.driverType == DriverType.dynamic &&
            currentJob.driverStatus.status == DriverStatus.driverInit) return;
        if (dashDialog && mounted) {
          dashDialog = false;
          Navigator.pop(context);
        }
        jobCubit.getCurrentJob(snap['currentJob']);
      } else if (currentDriver != null &&
          (snap['driverType'].toString().toLowerCase() !=
                  currentDriver.driverType
                      .toString()
                      .toLowerCase()
                      .split('.')
                      .last ||
              double.parse(snap['cashInHand'].toString()) !=
                  double.parse(currentDriver.cashInHand.toString()) ||
              double.parse(snap['cashLimit'].toString()) !=
                  double.parse(currentDriver.cashLimit.toString()) ||
              snap['driverBlocked'] != currentDriver.driverBlocked)) {
        if (currentJob != null) return;

        if (dashDialog && mounted) {
          dashDialog = false;
          Navigator.pop(context);
        }

        // Refresh Driver
        bool flag = await saveCurrentDriver(null);
        if (flag) {
          jobCubit.dashboardPage();
          loginCubit.checkLogin();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<JobCubit, JobState>(
        cubit: jobCubit,
        builder: (context, state) {
          if (state is JobInitial)
            return DashboardPage(
              job: currentJob,
              driverInfo: currentDriver,
              jobId: state.jobId,
            );
          else if (state is JobExitBase)
            return JobMapsPage(
              job: state.job,
              showMap: false,
              showTopField: false,
              buttonColor: Theme.of(context).primaryColor,
              buttonText: '',
              coordinates: state.journey.toCoordinates,
              buttonFunction: () {
                jobCubit.moveToPickup(state.job, state.journey);
              },
              docFunction: null,
              title: null,
              appBarTitle: 'exit_'.tr(args: ['base'.tr()]),
              dialogType: DialogType.Start,
              journey: state.journey,
            );
          else if (state is JobMovePickup)
            return JobMapsPage(
              job: state.job,
              showMap: true,
              showTopField: true,
              buttonColor: Theme.of(context).primaryColor,
              buttonText: 'check_in'.tr(),
              coordinates: state.journey.toCoordinates,
              buttonFunction: () {
                jobCubit.startLoading(state.job, state.journey);
              },
              docFunction: null,
              title: 'pickup_from'.tr(),
              appBarTitle: 'go_to_pickup'.tr(),
              dialogType: DialogType.None,
              journey: state.journey,
            );
          else if (state is JobStartLoading)
            return JobMapsPage(
              job: state.job,
              showMap: true,
              showTopField: false,
              buttonColor: Color(0xFF00C78B),
              buttonText: 'start_loading'.tr(),
              coordinates: state.journey.toCoordinates,
              buttonFunction: () {
                jobCubit.takePicPickupCheckIn(state.job, state.journey);
              },
              docFunction: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => JobShowImagesPage(
                          job: state.job,
                          journey: state.journey,
                          // TODO: Update images page
                        )));
              },
              title: 'you_have_arrived_at_location'.tr(),
              appBarTitle: 'start_loading'.tr(),
              dialogType: DialogType.None,
              journey: state.journey,
            );
          else if (state is JobTakePicPickupCheckIn)
            return CameraPage(
              title: 'checkin_pickup_image'.tr(),
              job: state.job,
              imageName: jobImageLabels[0],
              func: (Job job) {
                jobCubit.setLoaders(job, state.journey);
              },
              journey: state.journey,
            );
          else if (state is JobSetLoaders) {
            return JobMapsPage(
              job: state.job,
              showMap: false,
              showTopField: false,
              buttonColor: Theme.of(context).primaryColor,
              buttonText: '',
              coordinates: state.journey.toCoordinates,
              buttonFunction: null,
              docFunction: null,
              title: '',
              appBarTitle: 'set_loaders'.tr(),
              dialogType: DialogType.Loaders,
              journey: state.journey,
            );
          } else if (state is JobLoadingComplete) {
            return JobMapsPage(
              job: state.job,
              showMap: true,
              showTopField: false,
              buttonColor: Color(0xFF00C78B),
              buttonText: 'complete_loading'.tr(),
              coordinates: state.journey.toCoordinates,
              buttonFunction: () {
                jobCubit.takePicPickup(state.job, state.journey);
              },
              docFunction: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => JobShowImagesPage(
                          // TODO: Update images class
                          job: state.job,
                          journey: state.journey,
                        )));
              },
              title: 'pickup_from'.tr(),
              appBarTitle: 'loading_complete'.tr(),
              dialogType: DialogType.None,
              gifUrl: 'assets/loading.gif',
              journey: state.journey,
            );
          } else if (state is JobTakePicPickup)
            return CameraPage(
              title: 'take_loading_complete_image'.tr(),
              job: state.job,
              imageName: jobImageLabels[1],
              func: (Job job) {
                if (job.priceInfo.paymentTypeJourney == state.journey.toRef)
                  // TODO: Handle payment flow
                  jobCubit.notifyCash(job, state.journey);
                else
                  jobCubit.exitPickup(job, state.journey);
              },
              journey: state.journey,
            );

          // Notify Cash Page

          else if (state is JobExitPickup)
            return JobMapsPage(
              job: state.job,
              showMap: false,
              showTopField: false,
              buttonColor: Theme.of(context).primaryColor,
              buttonText: '',
              coordinates: state.journey.toCoordinates,
              buttonFunction: null,
              docFunction: null,
              title: null,
              appBarTitle: 'exit_'.tr(args: ['pickup'.tr()]),
              dialogType: DialogType.ExitPick,
              journey: state.journey,
            );
          else if (state is JobTakePicPickupGatePass)
            return CameraPage(
              title: 'take_gatepass_image'.tr(),
              job: state.job,
              imageName: jobImageLabels[2],
              func: (Job job) {
                if (job.priceInfo.paymentTypeJourney == state.journey.toRef)
                  jobCubit.collectCash(job, state.journey);
                else
                  jobCubit.nextJourney(job, state.journey);
              },
              journey: state.journey,
            );

          // Collect Cash Page

          else if (state is JobMoveDrop)
            return JobMapsPage(
              job: state.job,
              showMap: true,
              showTopField: true,
              buttonColor: Theme.of(context).primaryColor,
              buttonText: 'check_in'.tr(),
              coordinates: state.journey.toCoordinates,
              buttonFunction: () {
                jobCubit.startUnloading(state.job, state.journey);
              },
              docFunction: null,
              title: 'dropoff_to'.tr(),
              appBarTitle: 'go_to_dropoff'.tr(),
              dialogType: DialogType.None,
              journey: state.journey,
            );
          else if (state is JobUnloading)
            return JobMapsPage(
              job: state.job,
              showMap: true,
              showTopField: false,
              buttonColor: Color(0xFF00C78B),
              buttonText: 'start_unloading'.tr(),
              coordinates: state.journey.toCoordinates,
              buttonFunction: () {
                jobCubit.takePicDropoffCheckIn(state.job, state.journey);
              },
              docFunction: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => JobShowImagesPage(
                          //TODO: Update images class
                          job: state.job,
                          journey: state.journey,
                        )));
              },
              title: 'you_have_arrived_at_location'.tr(),
              appBarTitle: 'start_unloading'.tr(),
              dialogType: DialogType.None,
              journey: state.journey,
            );
          else if (state is JobTakePicDropoffCheckIn)
            return CameraPage(
              title: 'take_checkin_dropoff_image'.tr(),
              job: state.job,
              imageName: jobImageLabels[3],
              func: (Job job) {
                jobCubit.setUnloaders(job, state.journey);
              },
              journey: state.journey,
            );
          else if (state is JobSetUnloaders) {
            return JobMapsPage(
              job: state.job,
              showMap: false,
              showTopField: false,
              buttonColor: Theme.of(context).primaryColor,
              buttonText: '',
              coordinates: state.journey.toCoordinates,
              buttonFunction: null,
              docFunction: null,
              title: '',
              appBarTitle: 'set_unloaders'.tr(),
              dialogType: DialogType.UnLoaders,
              journey: state.journey,
            );
          } else if (state is JobUnloadingComplete) {
            return JobMapsPage(
              job: state.job,
              showMap: true,
              showTopField: false,
              buttonColor: Color(0xFF00C78B),
              buttonText: 'complete_unloading'.tr(),
              coordinates: state.journey.toCoordinates,
              buttonFunction: () {
                jobCubit.takePicDropoff(state.job, state.journey);
              },
              docFunction: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => JobShowImagesPage(
                          job: state.job,
                          journey: state.journey,
                        )));
              },
              title: 'dropoff_to'.tr(),
              appBarTitle: 'unloading_complete'.tr(),
              dialogType: DialogType.None,
              gifUrl: 'assets/unloading.gif',
              journey: state.journey,
            );
          } else if (state is JobTakePicDropoff)
            return CameraPage(
              title: 'take_unloading_picture'.tr(),
              job: state.job,
              imageName: jobImageLabels[4],
              func: (Job job) {
                if (job.priceInfo.paymentTypeJourney == state.journey.toRef)
                  jobCubit.notifyCash(job, state.journey);
                else
                  jobCubit.exitDropoff(job, state.journey);
              },
              journey: state.journey,
            );

          // Notify Cash Page

          else if (state is JobExitDropOff)
            return JobMapsPage(
              job: state.job,
              showMap: false,
              showTopField: false,
              buttonColor: Theme.of(context).primaryColor,
              buttonText: '',
              coordinates: state.journey.toCoordinates,
              buttonFunction: null,
              docFunction: null,
              title: null,
              appBarTitle: 'exit_'.tr(args: ['dropoff'.tr()]),
              dialogType: DialogType.ExitDrop,
              journey: state.journey,
            );
          else if (state is JobTakePicDropoffCertificate)
            return CameraPage(
              title: 'take_receiving_image'.tr(),
              job: state.job,
              imageName: jobImageLabels[5],
              func: (Job job) {
                if (job.priceInfo.paymentTypeJourney == state.journey.toRef)
                  jobCubit.collectCash(job, state.journey);
                else
                  jobCubit.nextJourney(job, state.journey);
              },
              journey: state.journey,
            );

          //Stop
          else if (state is JobMoveStop)
            return JobMapsPage(
              job: state.job,
              showMap: true,
              showTopField: true,
              buttonColor: Theme.of(context).primaryColor,
              buttonText: 'check_in'.tr(),
              coordinates: state.journey.toStop.coordinates,
              buttonFunction: () {
                jobCubit.reachedStop(state.job, state.journey);
              },
              docFunction: null,
              title: 'stop_to'.tr(),
              appBarTitle: 'go_to_stop'.tr(),
              dialogType: DialogType.None,
              journey: state.journey,
            );
          else if (state is JobReachedStop)
            return ScheduledStopPage(
              buttonText: 'exit_'.tr(args: ['stop_point'.tr()]),
              title: 'stop_point'.tr(),
              job: state.job,
              journey: state.journey,
            );
          else if (state is JobExitStop)
            return JobMapsPage(
              job: state.job,
              showMap: false,
              showTopField: false,
              buttonColor: Theme.of(context).primaryColor,
              buttonText: '',
              coordinates: state.journey.toStop.coordinates,
              buttonFunction: null,
              docFunction: null,
              title: null,
              appBarTitle: 'exit_'.tr(args: ['stop'.tr()]),
              dialogType: DialogType.ExitStop,
              journey: state.journey,
            );

          // Collect Cash Page

          else if (state is JobComplete)
            return JobCompletePage(
              job: state.job,
            );
          else if (state is JobMoveBase)
            return BaseMapsPage(
              buttonColor: Theme.of(context).buttonColor,
              buttonFunction: () {
                jobCubit.reachedBase(state.job);
              },
              buttonText: 'checkin_base'.tr(),
              title: 'go_to_base'.tr(),
              coordinates: Coordinates(
                  lat: double.parse(state.base.lat),
                  lng: double.parse(state.base.lng),
                  address: state.base.name,
                  contactName: null),
            );
          else if (state is JobCollectCash)
            return CashCollectPage(
              job: state.job,
              coordinates: state.journey.toCoordinates,
              buttonText: state.buttonText,
              journey: state.journey,
            );
          else if (state is JobList)
            return JobsListPage(
              jobs: state.jobs,
            );
          else if (state is JobDetails)
            return JobDetailsPage(job: state.job);
          else if (state is JobLoadingPage)
            return LoadingPage();
          else if (state is JobError)
            return ErrorPage(
              job: state.job,
              title: state.title,
              desc: state.desc,
              imagePath: state.imagePath,
              journey: state.journey,
            );
          else
            return DashboardPage(job: currentJob, driverInfo: currentDriver);
        },
        listener: (context, state) {});
  }
}
