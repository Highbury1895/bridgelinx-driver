import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:bridgelinx_driver/utils/loading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:easy_localization/easy_localization.dart';

class JobsListPage extends StatefulWidget {
  final List<String> jobs;

  const JobsListPage({Key key, this.jobs}) : super(key: key);
  @override
  _JobsListPageState createState() => _JobsListPageState();
}

class _JobsListPageState extends State<JobsListPage> {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  List<QueryDocumentSnapshot> jobObjs = []; // stores fetched products
  bool isLoading = false; // track if products fetching
  bool hasMore = true; // flag for more products available or not
  int documentLimit = 10; // documents to be fetched per request
  QueryDocumentSnapshot
      lastDocument; // flag for last document from where next 10 records to be fetched
  ScrollController _scrollController = ScrollController();

  int _lastDocumentInt = 0;
  List<String> _jobs = [];
  List<String> _jobsList = [];

  getProductsOld() async {
    if (!hasMore) {
      // print('No More Jobs');
      return;
    }
    if (isLoading) {
      return;
    }
    if (mounted)
      setState(() {
        isLoading = true;
      });

    QuerySnapshot querySnapshot;
    if (lastDocument == null) {
      querySnapshot = await firestore
          .collection('jobs')
          .where('driverId', isEqualTo: currentDriver.driverId)
          .orderBy('orderNumber', descending: true)
          .limit(documentLimit)
          .get();
    } else {
      querySnapshot = await firestore
          .collection('jobs')
          .where('driverId', isEqualTo: currentDriver.driverId)
          .orderBy('orderNumber', descending: true)
          .startAfterDocument(lastDocument)
          .limit(documentLimit)
          .get();
      // print(1);
    }

    if (querySnapshot.docs.length < documentLimit) {
      hasMore = false;
    }

    if (querySnapshot.docs.length > 0) lastDocument = querySnapshot.docs.last;
    jobObjs.addAll(querySnapshot.docs);
    if (mounted)
      setState(() {
        isLoading = false;
      });
  }

  getProducts() async {
    if (!hasMore || _jobs == null || _jobs.length <= 0) {
      // print('No More Jobs');
      return;
    }
    if (isLoading) {
      return;
    }
    if (mounted)
      setState(() {
        isLoading = true;
      });

    QuerySnapshot querySnapshot;
    if ((_jobs.length - _lastDocumentInt) <= documentLimit) {
      _jobsList = _jobs.getRange(_lastDocumentInt, _jobs.length).toList();
      _lastDocumentInt = _jobs.length - 1;
    } else {
      _jobsList = _jobs
          .getRange(_lastDocumentInt, _lastDocumentInt + documentLimit)
          .toList();
      _lastDocumentInt = (_lastDocumentInt + documentLimit) - 1;
    }

    try {
      querySnapshot = await firestore
          .collection('jobs')
          .where('_id', whereIn: _jobsList)
          .orderBy('orderNumber', descending: true)
          .get();
    } catch (e) {
      print(e.toString());
    }

    if (_lastDocumentInt >= (_jobs.length - 1)) {
      hasMore = false;
    }

    jobObjs.addAll(querySnapshot.docs);
    if (mounted)
      setState(() {
        isLoading = false;
      });
  }

  @override
  void initState() {
    _jobs = widget.jobs.reversed.toList();
    _jobs.removeWhere((element) => element == null);

    getProducts();
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      double delta = MediaQuery.of(context).size.height * 0.20;
      if (maxScroll - currentScroll <= delta) {
        getProducts();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'order_history'.tr(),
          style: Theme.of(context)
              .accentTextTheme
              .headline1
              .copyWith(color: Colors.white),
        ),
        centerTitle: true,
        leading: FlatButton(
          padding: EdgeInsets.all(8),
          onPressed: () {
            jobCubit.dashboardPage();
          },
          child: Center(
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(FontAwesomeIcons.headset),
            onPressed: () => setState(() {
              makePhoneCall(helpline);
            }),
          ),
        ],
      ),
      body: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Container(
          child: Column(children: [
            Expanded(
              child: jobObjs.length == 0 && !isLoading
                  ? Center(
                      child: Text('no_orders_completed'.tr()),
                    )
                  : jobObjs.length == 0 && isLoading
                      ? LoadingPage()
                      : Padding(
                          padding: EdgeInsets.all(15),
                          child: ListView.builder(
                            controller: _scrollController,
                            itemCount: jobObjs.length,
                            itemBuilder: (context, index) {
                              final _job = jobObjs[index]
                                              .data()['clientAppVersion'] ==
                                          null ||
                                      int.tryParse(jobObjs[index]
                                              .data()['clientAppVersion']
                                              .toString()) <
                                          journeyVersion
                                  ? Job.fromJson(jobObjs[index].data())
                                  : Job.fromJsonJourney(jobObjs[index].data());

                              return InkWell(
                                onTap: () {
                                  jobCubit.jobDetailsPage(_job);
                                },
                                splashColor: Theme.of(context).primaryColor,
                                child: Container(
                                  margin: EdgeInsets.only(top: 20),
                                  width: double.maxFinite,
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    elevation: 5,
                                    child: Container(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 25, horizontal: 20),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Text(
                                                      'order_no'.tr(args: [
                                                        _job.orderNumber
                                                            .toString()
                                                      ]),
                                                      style: Theme.of(context)
                                                          .accentTextTheme
                                                          .headline4
                                                          .copyWith(
                                                              fontSize: 20),
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Text(
                                                      DateFormat(
                                                              'dd-MM-yyyy hh:mm')
                                                          .format(
                                                        DateTime
                                                            .fromMillisecondsSinceEpoch(
                                                          int.parse(
                                                            (_job.endTime
                                                                        .millisecondsSinceEpoch ??
                                                                    _job.startTime
                                                                        .millisecondsSinceEpoch)
                                                                .toString(),
                                                          ),
                                                        ),
                                                      ),
                                                      style: Theme.of(context)
                                                          .accentTextTheme
                                                          .headline1,
                                                    ),
                                                  ],
                                                ),
                                                Text(
                                                  'Rs'.tr(args: [
                                                    _job.businessCode == null
                                                        ? (_job.priceInfo
                                                                        .finalAmount ==
                                                                    null
                                                                ? _job.priceInfo
                                                                    .estimatedAmount
                                                                : _job.priceInfo
                                                                            .estimatedAmount <
                                                                        _job.priceInfo
                                                                            .finalAmount
                                                                    ? _job
                                                                        .priceInfo
                                                                        .estimatedAmount
                                                                    : _job
                                                                        .priceInfo
                                                                        .finalAmount)
                                                            .toInt()
                                                            .toString()
                                                        : '0'
                                                  ]),
                                                  style: Theme.of(context)
                                                      .accentTextTheme
                                                      .headline5
                                                      .copyWith(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.all(5),
                                            width: double.maxFinite,
                                            decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(5),
                                                    bottomRight:
                                                        Radius.circular(5))),
                                            child: Center(
                                                child: Text(
                                              'view_details'.tr(),
                                              style: Theme.of(context)
                                                  .accentTextTheme
                                                  .bodyText1
                                                  .copyWith(
                                                      color: Colors.white),
                                            )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
            ),
            isLoading && jobObjs.length > 0
                ? Center(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      height: 50,
                      width: 50,
                      child: CircularProgressIndicator(),
                    ),
                  )
                : Container(),
            Container(
              padding: EdgeInsets.all(30),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                color: Color(0xFF00C78B),
                onPressed: () {
                  jobCubit.dashboardPage();
                },
                child: Center(
                  child: Container(
                    padding: EdgeInsets.all(15),
                    child: Text(
                      'back'.tr(),
                      style: Theme.of(context)
                          .accentTextTheme
                          .headline1
                          .copyWith(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
