import 'package:bridgelinx_driver/utils/size_config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:easy_localization/easy_localization.dart';
import '../utils/globals.dart';

class CodePage extends StatefulWidget {
  final int number;

  const CodePage({Key key, this.number}) : super(key: key);

  @override
  _CodePageState createState() => _CodePageState();
}

class _CodePageState extends State<CodePage> {
  TextEditingController _codeController = TextEditingController();
  FirebaseAuth _auth = FirebaseAuth.instance;

  String _verificationId;
  int _resendToken;
  DateTime timeCodeSent;
  // StreamController<ErrorAnimationType> errorController = StreamController<ErrorAnimationType>();

  Future<void> _loginUser(String phoneNumber, BuildContext context) async {
    timeCodeSent = DateTime.now();
    _auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: (PhoneAuthCredential credential) async {
        final UserCredential result =
            await _auth.signInWithCredential(credential);

        final User firebaseUser = result.user;

        if (firebaseUser != null) {
          loginCubit.goToDashboard(firebaseUser);
        } else {
          Fluttertoast.showToast(msg: "An error occurred".tr());
        }
      },
      verificationFailed: (FirebaseAuthException exception) {
        print(exception);
        Fluttertoast.showToast(msg: exception.toString());
      },
      codeSent: (String verificationId, int resendToken) async {
        _verificationId = verificationId;
        _resendToken = resendToken;

        timeCodeSent = DateTime.now();

        // _loginManually(verificationId, resendToken);
      },
      timeout: Duration(seconds: 60),
      codeAutoRetrievalTimeout: (String verificationId) {
        print('codeAutoRetrievalTimeout Error');
      },
    );
  }

  Future<void> _loginManually(String verificationId, int resendToken) async {
    String smsCode = _codeController.text;
    if (smsCode.length < 6) {
      Fluttertoast.showToast(msg: "invalid_code".tr());
      return;
    }

    PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(
        verificationId: verificationId, smsCode: smsCode);

    final UserCredential result =
        await _auth.signInWithCredential(phoneAuthCredential);

    final User firebaseUser = result.user;

    if (firebaseUser != null) {
      loginCubit.goToDashboard(firebaseUser);
    } else {
      Fluttertoast.showToast(msg: "wrong_code_entered".tr());
    }
  }

  @override
  void initState() {
    _loginUser('+92${widget.number.toString().trim()}', context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'create_account'.tr(),
          style: Theme.of(context)
              .accentTextTheme
              .headline1
              .copyWith(color: Colors.black),
        ),
        centerTitle: true,
        automaticallyImplyLeading: true,
        leading: IconButton(
            icon: Icon(
              FontAwesomeIcons.arrowLeft,
              color: Colors.black,
              size: 16,
            ),
            onPressed: () => loginCubit.goToLogin()),
      ),
      body: Stack(
        children: [
          Positioned.fill(
            child: Container(
              margin: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                            margin: EdgeInsets.all(10),
                            child: Text(
                              'enter_verification_code'.tr(),
                              style: Theme.of(context)
                                  .accentTextTheme
                                  .headline1
                                  .copyWith(color: Colors.black),
                            )),
                        Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: PinCodeTextField(
                            textInputType:
                                TextInputType.numberWithOptions(decimal: false),
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            length: 6,
                            obsecureText: false,
                            animationType: AnimationType.fade,
                            pinTheme: PinTheme(
                              shape: PinCodeFieldShape.underline,
                              borderRadius: BorderRadius.circular(5),
                              borderWidth: 1,
                              fieldHeight: 50,
                              fieldWidth: 50,
                              selectedColor: Theme.of(context).primaryColor,
                              activeColor: Colors.green,
                              disabledColor: Colors.grey,
                              inactiveColor: Colors.black38,
                              activeFillColor: Colors.white,
                              inactiveFillColor: Colors.white,
                              selectedFillColor: Colors.white,
                            ),
                            animationDuration: Duration(milliseconds: 300),
                            backgroundColor: Colors.white,
                            enableActiveFill: true,
                            // errorAnimationController: errorController,
                            controller: _codeController,
                            onCompleted: (v) {
                              // print("Completed");
                            },
                            onChanged: (val) {},
                            // onChanged: (value) {
                            //   print(value);
                            //   setState(() {
                            //     currentText = value;
                            //   });
                            // },
                            beforeTextPaste: (text) {
                              // print("Allowing to paste $text");
                              //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                              //but you can show anything you want here, like your pop up saying wrong paste format or etc
                              return true;
                            },
                            appContext: context,
                          ),
                        ),
                      ]),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      GestureDetector(
                        onTap: () {
                          DateTime now = DateTime.now();
                          int diff = now.difference(timeCodeSent).inSeconds;
                          diff > 60
                              ? _loginUser(
                                  '+92${widget.number.toString().trim()}',
                                  context)
                              : Fluttertoast.showToast(
                                  msg: 'please_wait'.tr(args: ['60']));
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            'resend_code'.tr(),
                            style: Theme.of(context).accentTextTheme.bodyText2,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () => setState(() {
                              makePhoneCall(helpline);
                            }),
                            child: Container(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                'need_help'.tr(),
                                style:
                                    Theme.of(context).accentTextTheme.bodyText2,
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => setState(() {
                              makePhoneCall(helpline);
                            }),
                            child: Container(
                              child: Text('call_us'.tr(),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .bodyText2
                                      .copyWith(color: Color(0xff00C2CB))),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        margin: EdgeInsets.all(5),
                        child: MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          padding: EdgeInsets.all(10),
                          onPressed: () =>
                              _loginManually(_verificationId, _resendToken),
                          color: Theme.of(context).buttonColor,
                          child: Center(
                            child: Text(
                              'next'.tr(),
                              style: Theme.of(context).accentTextTheme.button,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Center(
                          child: Text(
                            'we_will_use_your_phone'.tr(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).accentTextTheme.bodyText2,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class NumberPage extends StatefulWidget {
  @override
  _NumberPageState createState() => _NumberPageState();
}

class _NumberPageState extends State<NumberPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _phoneController = TextEditingController();

  void goToCode() {
    final form = _formKey.currentState;
    if (form.validate()) {
      loginCubit.goToLoginCode(int.parse(_phoneController.text));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: Text(
            'create_account'.tr(),
            style: Theme.of(context)
                .accentTextTheme
                .headline1
                .copyWith(color: Colors.black),
          ),
          centerTitle: true,
          automaticallyImplyLeading: true,
          leading: IconButton(
              icon: Icon(
                FontAwesomeIcons.arrowLeft,
                color: Colors.black,
                size: 16,
              ),
              onPressed: () => loginCubit.goToLoginInit()),
        ),
        body: Stack(
          children: [
            Positioned.fill(
              child: Container(
                height: SizeConfig.screenHeight,
                padding: EdgeInsets.all(15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                            margin: EdgeInsets.all(10),
                            child: Text(
                              'whats_your_phone_number'.tr(),
                              style: Theme.of(context)
                                  .accentTextTheme
                                  .headline1
                                  .copyWith(color: Colors.black),
                            )),
                        Container(
                          margin: EdgeInsets.all(5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    margin: EdgeInsets.symmetric(vertical: 5),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 5, vertical: 15),
                                    decoration: BoxDecoration(
                                        // borderRadius: BorderRadius.circular(5),
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.black38,
                                                width: 1))),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          'assets/pakistan_img.png',
                                          height: 20,
                                          width: 20,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          '+92',
                                          style: Theme.of(context)
                                              .accentTextTheme
                                              .headline2,
                                        ),
                                      ],
                                    ),
                                  )),
                              Expanded(
                                  flex: 3,
                                  child: Form(
                                    key: _formKey,
                                    child: Container(
                                      margin: EdgeInsets.symmetric(vertical: 5),
                                      padding: EdgeInsets.all(5),
                                      child: TextFormField(
                                        validator: (value) {
                                          if (value == null ||
                                              !value.startsWith('3') ||
                                              value.length < 10)
                                            return 'please_enter_valid_phone'.tr();
                                          else
                                            return null;
                                        },
                                        controller: _phoneController,
                                        maxLength: 10,
                                        decoration: InputDecoration(
                                          counterText: '',
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.blue, width: 1.0),
                                          ),
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.black38,
                                                width: 1.0),
                                          ),
                                          hintText: "phone_number".tr(),
                                          hintStyle: Theme.of(context)
                                              .accentTextTheme
                                              .headline2,
                                        ),
                                        keyboardType:
                                            TextInputType.numberWithOptions(
                                                decimal: false),
                                        inputFormatters: [
                                          FilteringTextInputFormatter.digitsOnly
                                        ],
                                      ),
                                    ),
                                  ))
                            ],
                          ),
                        )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          margin: EdgeInsets.all(5),
                          child: MaterialButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            padding: EdgeInsets.all(15),
                            onPressed: goToCode,
                            color: Theme.of(context).buttonColor,
                            child: Center(
                              child: Text(
                                'next'.tr(),
                                style: Theme.of(context).accentTextTheme.button,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          child: Center(
                            child: Text(
                              'we_will_use_your_phone'.tr(),
                              textAlign: TextAlign.center,
                              style:
                                  Theme.of(context).accentTextTheme.bodyText2,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
