import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:math' show cos, sqrt, asin;
import 'globals.dart';

const apiKey = "AIzaSyCQvfbYZc9HKiJj6CLnA2PTn2j-WG7Ryn8";

class GoogleMapsServices {
  double calculateDistance(LatLng l1, LatLng l2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((l2.latitude - l1.latitude) * p) / 2 +
        c(l1.latitude * p) *
            c(l2.latitude * p) *
            (1 - c((l2.longitude - l1.longitude) * p)) /
            2;
    final value = 12742 * asin(sqrt(a));
    return value;
  }

  Future<String> getRouteCoordinates(LatLng l1, LatLng l2) async {
    int _connected = await checkInternet();
    if (_connected != 2) return null;
    String url =
        "https://maps.googleapis.com/maps/api/directions/json?origin=${l1.latitude},${l1.longitude}&destination=${l2.latitude},${l2.longitude}&key=$apiKey";
    http.Response response = await http.get(url);
    Map values = jsonDecode(response.body);
    // print(response.body);
    return values["routes"][0]["overview_polyline"]["points"];
  }

  Future<Map<String, dynamic>> getDistanceMatrix(
      List<Coordinates> coordinates, List<Journey> journeys) async {
    String url =
        'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=';
    for (Coordinates c in coordinates)
      url += '${c.lat.toString()},${c.lng.toString()}|';
    url += '&destinations=';
    for (Coordinates c in coordinates)
      url += '${c.lat.toString()},${c.lng.toString()}|';
    url += '&key=$apiKey';

    http.Response response = await http.get(url);

    Map values = jsonDecode(response.body);

    Map<String, dynamic> journeyMapDistance = {
      "total": {"estimated": 0, "actual": 0}
    };
    Map<String, dynamic> journeyMapTime = {
      "total": {"estimated": 0, "actual": 0}
    };
    for (Journey j in journeys) {
      journeyMapDistance[j.id] = {"estimated": 0, "actual": 0, "remaining": 0};
      journeyMapTime[j.id] = {"estimated": 0, "actual": 0, "remaining": 0};
    }

    Map<String, dynamic> timesMap = journeyMapTime;
    Map<String, dynamic> distanceMap = journeyMapDistance;

    Map<String, dynamic> map = {};
    int distance = 0;
    int time = 0;

    for (int i = 0; i < coordinates.length - 1; i++) {
      String journey = journeys[i].id;

      time += values["rows"][i]["elements"][i + 1]['duration']['value'];
      timesMap[journey]["estimated"] =
          values["rows"][i]["elements"][i + 1]['duration']['value'];
      timesMap["total"]["estimated"] = time;

      distance += values["rows"][i]["elements"][i + 1]['distance']['value'];
      distanceMap[journey]["estimated"] =
          values["rows"][i]["elements"][i + 1]['distance']['value'];
      distanceMap["total"]["estimated"] = distance;
    }

    map["distances"] = distanceMap;
    map["times"] = timesMap;

    return map;
  }

  Future<int> getRouteDistance(List<Coordinates> coordinates) async {
    String url =
        'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=';
    for (Coordinates c in coordinates)
      url += '${c.lat.toString()},${c.lng.toString()}|';
    url += '&destinations=';
    for (Coordinates c in coordinates)
      url += '${c.lat.toString()},${c.lng.toString()}|';
    url += '&key=$apiKey';

    // print('URL: $url');

    http.Response response = await http.get(url);
    Map values = jsonDecode(response.body);
    int distance = 0;
    for (int i = 0; i < coordinates.length - 1; i++)
      distance += values["rows"][i]["elements"][i + 1]['distance']['value'];

    // print('Distance: $distance');

    return distance;
  }

  Future<Job> getRouteTimes(
      LatLng org1, LatLng org2, LatLng dest1, LatLng dest2, Job job) async {
    String url =
        "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${org1.latitude},${org1.longitude}|${org2.latitude},${org2.longitude}&destinations=${dest1.latitude},${dest1.longitude}|${dest2.latitude},${dest2.longitude}&key=$apiKey";
    http.Response response = await http.get(url);
    Map values = jsonDecode(response.body);
    int value1 = (values["rows"][0]["elements"][0]["duration"]["value"]);
    int value2 = (values["rows"][1]["elements"][1]["duration"]["value"]);

    job.times['BaseToPickup'] = value1;
    job.times['PickupToDropoff'] = value2;

    jobCubit.updateTimes(job);

    // await saveBaseToPickup(value1);
    // await savePickupToDropoff(value2);

    return job;
  }

  Future<Job> getRouteTimesJourney(
      LatLng org1, LatLng dest1, Job job, String journeyId) async {
    String url =
        "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${org1.latitude},${org1.longitude}&destinations=${dest1.latitude},${dest1.longitude}&key=$apiKey";
    http.Response response = await http.get(url);
    Map values = jsonDecode(response.body);
    int value1 = (values["rows"][0]["elements"][0]["duration"]["value"]);

    job.times[journeyId] = value1;

    jobCubit.updateTimes(job);

    return job;
  }

  List<LatLng> convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }

  List decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
    do {
      var shift = 0;
      int result = 0;
      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);
    for (var i = 2; i < lList.length; i++) lList[i] += lList[i - 2];
    // print(lList.toString());
    return lList;
  }
}
