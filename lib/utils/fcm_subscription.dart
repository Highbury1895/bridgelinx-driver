import 'dart:async';
import 'dart:io';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final FirebaseFirestore _db = FirebaseFirestore.instance;
final FirebaseMessaging _fcm = FirebaseMessaging();
StreamSubscription iosSubscription;

class Subscip {
  static Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) {
    print('background method $message');
    /*
    if (message.containsKey('data') && message['data']['jobId'] != null) {
      if (currentJob != null && currentDriver.driverType == DriverType.dynamic)
        return Future.value(null);
      showNotification('New Order', 'You have received a new order');
      jobNotifTime = DateTime.now();
      jobCubit.getCurrentJob(message['data']['jobId']);
    }
    */

    return Future.value(null);
  }
}

void initNotifs() {
  if (Platform.isIOS) {
    iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
      // save the token  OR subscribe to a topic here
    });
    _fcm.requestNotificationPermissions(IosNotificationSettings());
  }

  _fcm.configure(
    onMessage: (Map<String, dynamic> message) async {
      print("onMessage: $message");
      if (message.containsKey('data') && message['data']['jobId'] != null) {
        if (currentJob != null) return;
        showNotification('New Order', 'You have received a new order');
        jobNotifTime = DateTime.now();
        jobCubit.getCurrentJob(message['data']['jobId']);
      }
    },
    onBackgroundMessage: Subscip.myBackgroundMessageHandler,
    onLaunch: (Map<String, dynamic> message) async {
      // print("onLaunch: $message");
      if (message.containsKey('data') && message['data']['jobId'] != null) {
        if (currentJob != null) return;
        currentJobId = message['data']['jobId'];
        // jobCubit.getCurrentJob(message['data']['jobId']);
      }
    },
    onResume: (Map<String, dynamic> message) async {
      // print("onResume: $message");
      if (message.containsKey('data') && message['data']['jobId'] != null) {
        if (currentJob != null) return;
        jobNotifTime = DateTime.now();
        jobCubit.getCurrentJob(message['data']['jobId']);
      }
    },
  );

  _fcm.subscribeToTopic('jobs');
}

subscribeToTpc(String topic) {
  _fcm.subscribeToTopic(topic).then((onValue) {
    return onValue;
  });
}

// Get the token, save it to the database for current user
Future<String> saveDeviceToken() async {
  // Get the current user
  String uid = _auth.currentUser.uid;

  // Get the token for this device
  String fcmToken = await _fcm.getToken();

  // Save it to Firestore
  if (fcmToken != null) {
    DocumentReference tokens = await _db
        .collection('drivers')
        .where('profileInfo', isEqualTo: uid)
        .get()
        .then((value) => value.docs.first.reference);
    tokens.update({'notifToken': fcmToken, 'versionName': appVersion, 'versionCode': appVersionCode});
    return fcmToken;
  } else {
    return '';
  }
}
