import 'dart:io';
import 'dart:typed_data';
import 'package:app_settings/app_settings.dart';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/DTO/journey.dart';
import 'package:bridgelinx_driver/Repos/job_repo.dart';
import 'package:bridgelinx_driver/cubit/job_cubit.dart';
import 'package:bridgelinx_driver/cubit/login_cubit.dart';
import 'package:bridgelinx_driver/dashboard/flow/map_page.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:device_info/device_info.dart';
// import 'package:camera/camera.dart';
import 'package:flutter_better_camera/camera.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_mapbox_navigation/library.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';
import '../DTO/driver.dart';
import '../Repos/driver_repo.dart';
import 'package:connectivity/connectivity.dart';
import 'package:easy_localization/easy_localization.dart';
import 'dart:ui' as ui;
import 'package:geolocator/geolocator.dart' as geo;

// Enums
enum FlavorType { Dev, Prod }

// Bloc
final loginCubit = new LoginCubit(DriverRepo(), FirebaseAuth.instance);
final jobCubit = JobCubit(JobRepo());

// Variables

FlavorType appFlavor;
String appVersion;
int appVersionCode;

Driver currentDriver;
Job currentJob;
String currentJobId;
Journey currentJourney;
String currentJourneyId;

Coordinates currentCoordinates;

String helpline = '+923109993626';

List<CameraDescription> cameras = [];

DateTime jobNotifTime;

bool dashDialog = false;
bool jobDialog = false;

MapBoxNavigation directions;

List<String> errors = ['location_off', 'internet_off'];
List<String> journeys = ['BaseToPickup', 'PickupToDropoff', 'DropoffToBase'];
List<String> jobImageLabels = [
  'loadingStarted',
  'loadingCompleted',
  'gatepass',
  'unloadingStarted',
  'unloadingCompleted',
  'receiving'
];

int journeyVersion = 34;

// Functions

// Check Internet

Future<bool> checkConnected() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    return true;
  }
  return false;
}

Future<int> checkInternet() async {
  bool connected = await checkConnected();
  if (connected) {
    try {
      await FirebaseFirestore.instance
          .runTransaction((Transaction tx) => null)
          .timeout(Duration(seconds: 5));
      return 2;
    } catch (_) {
      return 1;
    }
  } else {
    return 0;
  }
}

// Check Internet End

// Get Current Location

Future<LocationData> getCurrentLocation() async {
  // Not Used
  final _location = Location();
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  _serviceEnabled = await _location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await _location.requestService();
    if (!_serviceEnabled) {
      return null;
    }
  }

  _permissionGranted = await _location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await _location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return null;
    }
  }

  // Location Settings
  _location.changeSettings(
      interval: 5000, accuracy: LocationAccuracy.balanced, distanceFilter: 0);

  _locationData = await _location.getLocation();
  return _locationData;
}

Future<geo.Position> getGeoCurrentLocation(Journey journey) async {
  bool _serviceEnabled = await geo.Geolocator.isLocationServiceEnabled();

  if (!_serviceEnabled && jobCubit != null) {
    Fluttertoast.showToast(msg: 'please_turn_on_location_services'.tr());
    jobCubit.state is JobError
        ? DoNothingAction()
        : jobCubit.showError(
            currentJob,
            errors[0].tr(),
            'please_turn_on_location_services'.tr(),
            'assets/no_location.png',
            journey);
    return null;
  }

  geo.LocationPermission _permission = await geo.Geolocator.checkPermission();

  if (_permission != geo.LocationPermission.always) {
    _permission = await geo.Geolocator.requestPermission();
    getGeoCurrentLocation(journey);
    return null;
  }

  geo.Position _position = await geo.Geolocator.getCurrentPosition(
    desiredAccuracy: geo.LocationAccuracy.bestForNavigation,
  );

  currentCoordinates =
      Coordinates(lat: _position.latitude, lng: _position.longitude);

  return _position;
}

// Get Current Location End

// Location Permission

void checkLocationPermission(BuildContext context, bool policyDialog) async {
  geo.LocationPermission _permission = await geo.Geolocator.checkPermission();

  if (_permission != geo.LocationPermission.always) {
    if (policyDialog)
      _permission = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => locationPolicyDialog(context),
      ).then((value) async {
        if (Platform.isAndroid) {
          var androidInfo = await DeviceInfoPlugin().androidInfo;
          if (androidInfo.version.sdkInt >= 30)
            return AppSettings.openLocationSettings().then((value) async {
              return _permission = await geo.Geolocator.checkPermission();
            });
          else
            return _permission = await geo.Geolocator.requestPermission();
        } else {
          return _permission = await geo.Geolocator.requestPermission();
        }
      });
    else {
      if (Platform.isAndroid) {
        var androidInfo = await DeviceInfoPlugin().androidInfo;
        if (androidInfo.version.sdkInt >= 30)
          AppSettings.openLocationSettings().then((value) async {
            _permission = await geo.Geolocator.checkPermission();
          });
        else
          _permission = await geo.Geolocator.requestPermission();
      } else {
        _permission = await geo.Geolocator.requestPermission();
      }
    }

    if (_permission != geo.LocationPermission.always) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => wrongPermissionDialog(context),
      ).then((value) async {
        if (_permission == geo.LocationPermission.deniedForever)
          await AppSettings.openLocationSettings().then((value) async {
            checkLocationPermission(context, false);
          });
        else
          checkLocationPermission(context, false);
      });
    }
  }
}

Dialog locationPolicyDialog(BuildContext context) {
  String msg1 = "bridgelinx_driver_requires_location_in_background".tr();
  String msg2 = "please_select_location_permission".tr();
  String msg3 = "when_prompted".tr();
  String btnName = "'Allow all the time'";

  return Dialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
    elevation: 2,
    child: Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8), topRight: Radius.circular(8))),
            padding: EdgeInsets.all(30),
            child: Center(
              child: Icon(
                FontAwesomeIcons.searchLocation,
                size: 40,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                RichText(
                  text: TextSpan(
                      style: Theme.of(context).accentTextTheme.bodyText1,
                      children: [
                        TextSpan(text: msg1),
                        TextSpan(text: msg2),
                        TextSpan(
                            text: btnName,
                            style: Theme.of(context)
                                .accentTextTheme
                                .bodyText1
                                .copyWith(fontWeight: FontWeight.bold)),
                        TextSpan(text: msg3),
                      ]),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(),
                    FlatButton(
                      padding: EdgeInsets.all(10),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Center(
                        child: Text(
                          'okay'.tr(),
                          style: Theme.of(context)
                              .accentTextTheme
                              .button
                              .copyWith(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

Dialog wrongPermissionDialog(BuildContext context) {
  return Dialog(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
    elevation: 2,
    child: Container(
      padding: EdgeInsets.all(15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.all(5),
            child: Text(
              'wrong_permission_selected'.tr(),
              style: Theme.of(context).accentTextTheme.headline5,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.max,
              children: [
                Image.asset(
                  'assets/permissions_image.jpg',
                  height: 220,
                  width: 250,
                  fit: BoxFit.contain,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'please_select_option'.tr(args: ['Allow all the time']),
                  style: Theme.of(context).accentTextTheme.headline1,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(),
                    FlatButton(
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      padding: EdgeInsets.all(10),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'okay'.tr(),
                        style: Theme.of(context)
                            .accentTextTheme
                            .headline1
                            .copyWith(color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

// Location Permission End

Future<void> makePhoneCall(String number) async {
  if (await canLaunch('tel://$number')) {
    // await launch('tel://$number');
    Fluttertoast.showToast(msg: 'calling'.tr());
    await FlutterPhoneDirectCaller.callNumber(number);
  } else {
    throw 'Could not launch $number';
  }
}

// Maps Start
Future<void> launchGoogleMaps(LatLng origin, LatLng destination) async {
  String url =
      "https://www.google.com/maps/dir/?api=1&origin=${origin.latitude},${origin.longitude}&destination=${destination.latitude},${destination.longitude}&travelmode=driving&dir_action=navigate";
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch Maps';
  }
}

void launchMapBox(LatLng origin, LatLng destination, String destTitle) async {
  final _origin = WayPoint(
      name: "Current Location",
      latitude: origin.latitude,
      longitude: origin.longitude);
  final _stop = WayPoint(
      name: destTitle,
      latitude: destination.latitude,
      longitude: destination.longitude);

  var wayPoints = List<WayPoint>();
  wayPoints.add(_origin);
  wayPoints.add(_stop);

  await directions.startNavigation(
      wayPoints: wayPoints,
      options: MapBoxOptions(
          enableRefresh: false,
          alternatives: false,
          voiceInstructionsEnabled: true,
          mode: MapBoxNavigationMode.drivingWithTraffic,
          simulateRoute: false,
          language: "en",
          units: VoiceUnits.metric));
}

Future<Uint8List> getBytesFromAsset(String path, int width) async {
  ByteData data = await rootBundle.load(path);
  ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
      targetWidth: width);
  ui.FrameInfo fi = await codec.getNextFrame();
  return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
      .buffer
      .asUint8List();
}
// Maps End

// Local Notifications Start
void initializeNotifications() {
  // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
  var initializationSettingsAndroid = new AndroidInitializationSettings(
      'org_thebus_foregroundserviceplugin_notificationicon');
  var initializationSettingsIOS =
      new IOSInitializationSettings(onDidReceiveLocalNotification: null);
  var initializationSettings = new InitializationSettings(
      initializationSettingsAndroid, initializationSettingsIOS);
  flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: null);
}

showNotification(String title, String body) async {
  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'your channel id', 'your channel name', 'your channel description',
      importance: Importance.Max,
      priority: Priority.High,
      ticker: 'ticker',
      playSound: true,
      autoCancel: true,
      icon: 'org_thebus_foregroundserviceplugin_notificationicon');
  var iOSPlatformChannelSpecifics = IOSNotificationDetails();
  var platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
      0, title, body, platformChannelSpecifics);

  FlutterRingtonePlayer.play(
    android: AndroidSounds.notification,
    ios: IosSounds.glass,
    looping: false, // Android only - API >= 28
    volume: 1, // Android only - API >= 28
    asAlarm: true, // Android only - all APIs
  );
}
// Local Notifications End

// Toast Start
showToast(String title, String body) async {
  /*
  Widget toast = Container(
    padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(25.0),
      color: Color(0xff1C75BC),
    ),
    child: Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          Icons.check,
          color: Colors.white,
          size: 20,
        ),
        SizedBox(
          width: 12.0,
        ),
        Text(
          title,
          style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.w600,
              fontFamily: 'Avenir'),
        ),
      ],
    ),
  );

  fToast.showToast(
    child: toast,
    gravity: ToastGravity.TOP,
    toastDuration: Duration(seconds: 3),
  );
  */

  Fluttertoast.showToast(
    msg: title,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.TOP,
    timeInSecForIosWeb: 1,
    backgroundColor: Color(0xff1C75BC),
    textColor: Colors.white,
    fontSize: 20.0,
  );

  FlutterRingtonePlayer.play(
    android: AndroidSounds.notification,
    ios: IosSounds.glass,
    looping: false, // Android only - API >= 28
    volume: 1, // Android only - API >= 28
    asAlarm: true, // Android only - all APIs
  );
}
// Toast End

// UI
// Show Modal Dialog at start
showBottomDialog(
    BuildContext context, DialogType type, Job job, Journey journey) {
  jobDialog = true;
  showModalBottomSheet(
      isDismissible: false,
      enableDrag: false,
      context: context,
      builder: (BuildContext bc) {
        return WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: Container(
            padding: EdgeInsets.all(30),
            child: new Wrap(
              alignment: WrapAlignment.start,
              crossAxisAlignment: WrapCrossAlignment.start,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(bottom: 15),
                    child: Text(
                      'continue_exit'.tr(args: [
                        type == DialogType.Start
                            ? 'base'.tr()
                            : type == DialogType.ExitPick
                                ? 'pickup_location'.tr()
                                : type == DialogType.ExitDrop
                                    ? 'dropoff_location'.tr()
                                    : type == DialogType.ExitStop
                                        ? 'stop_location'.tr()
                                        : ''
                      ]),
                      style: Theme.of(context).accentTextTheme.headline5,
                    )),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      // margin: EdgeInsets.only(top: 15),
                      child: FlatButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          color: Colors.blue,
                          onPressed: () {
                            Navigator.pop(context);
                            if (type == DialogType.Start)
                              jobCubit.moveToPickup(job, journey);
                            else if (type == DialogType.ExitPick)
                              jobCubit.takePicPickupGatePass(job, journey);
                            else if (type == DialogType.ExitDrop)
                              jobCubit.takePicDropoffCertificate(job, journey);
                            else if (type == DialogType.ExitStop)
                              jobCubit.nextJourney(job, journey);
                          },
                          child: Center(
                            child: Container(
                              padding: EdgeInsets.all(15),
                              child: Text(
                                  'exit_'.tr(args: [
                                    type == DialogType.Start
                                        ? 'base'.tr()
                                        : type == DialogType.ExitPick
                                            ? 'pickup_location'.tr()
                                            : type == DialogType.ExitDrop
                                                ? 'dropoff_location'.tr()
                                                : type == DialogType.ExitStop
                                                    ? 'stop_location'.tr()
                                                    : ''
                                  ]),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline1
                                      .copyWith(
                                        color: Colors.white,
                                      )),
                            ),
                          )),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      }).then((value) {
    jobDialog = false;
  });
}

// Show Modal Dialog at start
showLoaderDialog(
    BuildContext context, DialogType type, Job job, Journey journey) {
  jobDialog = true;
  int loaders = 1;
  showModalBottomSheet(
      isDismissible: false,
      enableDrag: false,
      context: context,
      builder: (BuildContext bc) {
        return WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: StatefulBuilder(
            builder: (BuildContext context, StateSetter setSheetState) =>
                Container(
              padding: EdgeInsets.all(30),
              child: new Wrap(
                alignment: WrapAlignment.start,
                crossAxisAlignment: WrapCrossAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Text(
                      'select_number_of'.tr(args: [
                        type == DialogType.Loaders
                            ? 'loaders'.tr()
                            : 'unloaders'.tr()
                      ]),
                      style: Theme.of(context).accentTextTheme.headline5,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ButtonTheme(
                          height: 40,
                          minWidth: 40,
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            color: Colors.blueGrey[100],
                            onPressed: () {
                              if (loaders <= 0) return;
                              setSheetState(() {
                                loaders--;
                              });
                            },
                            child: Center(
                              child: Icon(FontAwesomeIcons.minus,
                                  color: Colors.grey, size: 15),
                            ),
                          ),
                        ),
                        Text(
                          loaders.toString(),
                          style: Theme.of(context).accentTextTheme.headline4,
                        ),
                        ButtonTheme(
                          height: 40,
                          minWidth: 40,
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            color: Colors.blueGrey[100],
                            onPressed: () {
                              setSheetState(() {
                                loaders++;
                              });
                            },
                            child: Center(
                              child: Icon(FontAwesomeIcons.plus,
                                  color: Colors.grey, size: 15),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            color: Colors.blue,
                            onPressed: () async {
                              Navigator.pop(context);
                              if (type == DialogType.Loaders) {
                                // job.numberOfLoaders = loaders;
                                if (job.numberOfLoadersJourney == null)
                                  job.numberOfLoadersJourney = {
                                    journey.toRef: loaders
                                  };
                                else
                                  job.numberOfLoadersJourney[journey.toRef] =
                                      loaders;
                                await saveCurrentJob(job);
                                jobCubit.completeLoading(job, journey);
                              } else if (type == DialogType.UnLoaders) {
                                // job.numberOfUnloaders = loaders;
                                if (job.numberOfUnloadersJourney == null)
                                  job.numberOfUnloadersJourney = {
                                    journey.toRef: loaders
                                  };
                                else
                                  job.numberOfUnloadersJourney[journey.toRef] =
                                      loaders;
                                await saveCurrentJob(job);
                                jobCubit.completeUnloading(job, journey);
                              } else
                                print('Something went wrong');
                            },
                            child: Center(
                              child: Container(
                                padding: EdgeInsets.all(15),
                                child: Text(
                                  'next'.tr(),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline1
                                      .copyWith(
                                        color: Colors.white,
                                      ),
                                ),
                              ),
                            )),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      }).then((value) {
    jobDialog = false;
  });
}

Widget showBottomMenu(
    BuildContext context,
    Coordinates coordinates,
    LatLng center,
    Function func,
    Function docsFunc,
    String buttonText,
    Color buttonColor,
    String title,
    Function moveCamera,
    int orderNumber,
    String gifUrl,
    Journey journey) {
  bool _navLoading = false;

  return Positioned(
    bottom: 0,
    right: 0,
    left: 0,
    child: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          // Top Buttons
          Container(
            color: Colors.transparent,
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ButtonTheme(
                  height: 40,
                  minWidth: 40,
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    color: Theme.of(context).primaryColorDark,
                    onPressed: moveCamera,
                    child: Center(
                      child: Icon(Icons.my_location,
                          color: Colors.white, size: 20),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                StatefulBuilder(
                  builder: (contextButton, setButtonState) => FlatButton.icon(
                    padding: EdgeInsets.all(10),
                    color: Theme.of(context).primaryColorDark,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black54,
                    onPressed: _navLoading
                        ? null
                        : () async {
                            if (contextButton != null)
                              setButtonState(() {
                                _navLoading = true;
                              });

                            // Check Internet
                            int _connected = await checkInternet();

                            if (contextButton != null)
                              setButtonState(() {
                                _navLoading = false;
                              });

                            if (_connected == 2)
                              try {
                                launchMapBox(
                                    center,
                                    LatLng(coordinates.lat, coordinates.lng),
                                    title.split(' ').first);
                              } catch (e) {
                                print(e);
                              }
                            else
                              Fluttertoast.showToast(msg: 'internet_off'.tr());
                          },
                    icon: _navLoading
                        ? SizedBox(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(Colors.white),
                            ))
                        : Icon(
                            FontAwesomeIcons.map,
                            color: Colors.white,
                            size: 20,
                          ),
                    label: Text(
                      'navigation'.tr(),
                      style: Theme.of(context)
                          .accentTextTheme
                          .headline1
                          .copyWith(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
          // Bottom Container
          Container(
            padding: EdgeInsets.all(25),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                // Gif
                gifUrl == null
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(bottom: 15),
                        child: Image.asset(
                          gifUrl,
                          height: 150.0,
                          width: 150.0,
                        ),
                      ),
                // Title
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        title ?? '',
                        style: Theme.of(context).accentTextTheme.headline5,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        orderNumber == null ||
                                (context.locale ==
                                        EasyLocalization.of(context)
                                            .supportedLocales[0]
                                    ? title.length > 15
                                    : title.length > 25)
                            ? ''
                            : 'order_no'.tr(args: [orderNumber.toString()]),
                        style: Theme.of(context).accentTextTheme.headline1,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                // Bottom Row
                coordinates.contactName == null
                    ? journey != null && journey.toStop != null
                        ? Container(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Container(),
                                Text(
                                  'checkin_time'.tr(args: [
                                    DateFormat.jm()
                                        .format(
                                            DateTime.fromMillisecondsSinceEpoch(
                                                int.parse(journey.toStop
                                                    .reachTime['estimated']
                                                    .toString())))
                                        .toString()
                                  ]),
                                  style: Theme.of(context)
                                      .accentTextTheme
                                      .headline5
                                      .copyWith(
                                          color: Colors.red, fontSize: 18),
                                ),
                              ],
                            ),
                          )
                        : Container()
                    : Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            // User
                            Wrap(
                              crossAxisAlignment: WrapCrossAlignment.center,
                              children: [
                                Container(
                                  child: Text(
                                    coordinates.contactName,
                                    style: Theme.of(context)
                                        .accentTextTheme
                                        .headline1,
                                  ),
                                )
                              ],
                            ),
                            // Buttons
                            Wrap(
                              alignment: WrapAlignment.end,
                              children: [
                                ButtonTheme(
                                  height: 40,
                                  minWidth: 40,
                                  child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5)),
                                    color: Theme.of(context).primaryColorDark,
                                    onPressed: () {
                                      try {
                                        makePhoneCall(coordinates.contactPhone);
                                      } catch (e) {
                                        print(e.toString());
                                      }
                                    },
                                    child: Center(
                                      child: Icon(FontAwesomeIcons.phoneAlt,
                                          color: Colors.white, size: 20),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Visibility(
                                  visible: docsFunc != null,
                                  child: ButtonTheme(
                                    height: 40,
                                    minWidth: 40,
                                    child: FlatButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      color: Colors.blueGrey[50],
                                      onPressed: docsFunc,
                                      child: Center(
                                        child: Icon(
                                          FontAwesomeIcons.stickyNote,
                                          color: Colors.grey,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  // margin: EdgeInsets.symmetric(horizontal: 15),
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    color: buttonColor,
                    disabledColor: Color(0xFFEEEEEE),
                    onPressed: func,
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(15),
                            child: Center(
                              child: Text(
                                buttonText,
                                style: Theme.of(context)
                                    .accentTextTheme
                                    .headline1
                                    .copyWith(
                                        color: func == null
                                            ? Color(0xFF757575)
                                            : Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
