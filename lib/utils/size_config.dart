/*
Important class to help and support responsiveness in the app,
it gets width and height by Media Query and then used to calculate other important pixels.
this class also contains some color values that are used throughout the app for
custom styling.
 */

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData; //initializer
  static double screenWidth; //Width of screen
  static double screenHeight; //Height of Phone's screen
  static double blockSizeHorizontal; // 1/100th value of screen's width
  static double blockSizeVertical;// 1/100th value of screen's height
  static double _safeAreaHorizontal; // Padding on both side (left-right) for safe area. notches and iPhoneX and above
  static double _safeAreaVertical; // Padding on both (top-bottom) for safe area vertically.
  static double safeBlockHorizontal; // 1/100th of screen width after subtracting safe-area.
  static double safeBlockVertical; // 1/100th of screen height after subtracting safe-area.
  static Color backgroundColour; // Color of base canvas in every screen of application.

  void init(BuildContext context){
    _mediaQueryData = MediaQuery.of(context);
    backgroundColour = const Color(0xff3b48ad);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height -  AppBar().preferredSize.height;
    blockSizeHorizontal = screenWidth/100;
    blockSizeVertical = screenHeight/100;
    _safeAreaHorizontal = _mediaQueryData.padding.left +
        _mediaQueryData.padding.right;
    _safeAreaVertical = _mediaQueryData.padding.top +
        _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal)/100;
    safeBlockVertical = (screenHeight - _safeAreaVertical)/100;
  }
}