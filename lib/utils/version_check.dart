import 'dart:io';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

class VersionCheck {
  static const APP_STORE_URL = null;
  static const PLAY_STORE_URL =
      'https://play.google.com/store/apps/details?id=com.weoveri.bridgelinx_driver';

  static versionCheck(context) async {
    //Get Current installed version of app
    final PackageInfo info = await PackageInfo.fromPlatform();
    double currentVersion =
        double.parse(info.version.trim().replaceAll(".", ""));

    //Get Latest version info from firebase config
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    try {
      // Using default duration to force fetching from remote server.
      await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      await remoteConfig.activateFetched();
      double minVersion = double.parse(remoteConfig
          .getString('force_update_driver_min')
          .trim()
          .replaceAll(".", ""));
      double newVersion = double.parse(remoteConfig
          .getString('force_update_driver_current_version')
          .trim()
          .replaceAll(".", ""));
      if (newVersion > currentVersion) {
        bool _skip = minVersion <= currentVersion;
        _showVersionDialog(context, _skip);
      }
    } on FetchThrottledException catch (exception) {
      // Fetch throttled.
      print(exception);
    } catch (exception) {
      print('Unable to fetch remote config. Cached or default values will be '
          'used');
    }
  }

  static _showVersionDialog(context, bool skip) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String title = "new_update_available".tr();
        String message =
            "newer_version_of_app_available".tr();
        String btnLabel = "update_now".tr();
        String btnLabelCancel = "later".tr();
        return Platform.isIOS
            ? WillPopScope(
                onWillPop: () async => false,
                child: new CupertinoAlertDialog(
                  title: Text(title),
                  content: Text(message),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(btnLabel),
                      onPressed: () => _launchURL(APP_STORE_URL),
                    ),
                    FlatButton(
                      child: Text(btnLabelCancel),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ],
                ),
              )
            : WillPopScope(
                onWillPop: () async => false,
                child: new AlertDialog(
                  title: Text(title),
                  content: Text(message),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(btnLabel),
                      onPressed: () => _launchURL(PLAY_STORE_URL),
                    ),
                    Visibility(
                      visible: skip,
                      child: FlatButton(
                        child: Text(btnLabelCancel),
                        onPressed: () => Navigator.pop(context),
                      ),
                    ),
                  ],
                ),
              );
      },
    );
  }

  static _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
