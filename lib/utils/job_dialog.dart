import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:bridgelinx_driver/utils/shared_preferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:easy_localization/easy_localization.dart';

class Consts {
  Consts._();

  static const double padding = 15.0;
  static const double avatarRadius = 50.0;
}

class JobDialog extends StatefulWidget {
  final String title, description, buttonText;
  final Image image;
  final Job job;

  JobDialog({
    Key key,
    @required this.title,
    this.description,
    this.buttonText,
    this.image,
    this.job,
  }) : super(key: key);

  @override
  _JobDialogState createState() => _JobDialogState();
}

class _JobDialogState extends State<JobDialog> {
  List<Coordinates> pickupCoordinates = List.from([]);
  List<Coordinates> dropoffCoordinates = List.from([]);

  getClientCoordinates(Job job) {
    Map<String, dynamic> pickupMap = job.clientCoordinatesJourney.pickup;
    Map<String, dynamic> dropoffMap = job.clientCoordinatesJourney.dropoff;

    pickupCoordinates
        .addAll(pickupMap.values.map((e) => Coordinates.fromJson(e)).toList());
    dropoffCoordinates
        .addAll(dropoffMap.values.map((e) => Coordinates.fromJson(e)).toList());
  }

  @override
  void initState() {
    getClientCoordinates(widget.job);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: Consts.padding + Consts.avatarRadius,
              bottom: Consts.padding,
              left: Consts.padding,
              right: Consts.padding,
            ),
            // margin: EdgeInsets.only(top: Consts.avatarRadius),
            decoration: new BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(Consts.padding),
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10.0,
                  offset: const Offset(0.0, 10.0),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                // SizedBox(height: 20.0),
                Expanded(
                  flex: 2,
                  child: Center(
                    child: Text(widget.title,
                        style: Theme.of(context)
                            .accentTextTheme
                            .headline4
                            .copyWith(color: Theme.of(context).primaryColor)),
                  ),
                ),
                SizedBox(height: 25.0),
                // Job Details
                Expanded(
                  flex: 12,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        // Order Number
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(),
                              Text(
                                'order_no'.tr(
                                    args: [widget.job.orderNumber.toString()]),
                                style:
                                    Theme.of(context).accentTextTheme.headline1,
                              )
                            ],
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(4),
                              topRight: Radius.circular(4),
                              bottomLeft: Radius.circular(4),
                              bottomRight: Radius.circular(4),
                            ),
                            border: Border.all(
                              width: 1,
                              color: Colors.grey,
                            ),
                          ),
                          child: Column(
                            children: [
                              // Pickup Details
                              Container(
                                padding: EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                    width: 1,
                                    color: Colors.grey,
                                  )),
                                ),
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: pickupCoordinates.length,
                                  physics: ScrollPhysics(),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    final _coordinates =
                                        pickupCoordinates[index];
                                    return Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.all(5),
                                          child: Icon(
                                            FontAwesomeIcons.mapMarkerAlt,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Container(
                                          width: 200,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'pickup'.tr(),
                                                style: Theme.of(context)
                                                    .accentTextTheme
                                                    .bodyText2,
                                              ),
                                              Text(
                                                _coordinates.formattedAddress,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 5,
                                                style: Theme.of(context)
                                                    .accentTextTheme
                                                    .headline2,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ),

                              // Dropoff Details
                              Container(
                                  padding: EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                      width: 1,
                                      color: Colors.grey,
                                    )),
                                  ),
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: dropoffCoordinates.length,
                                    physics: ScrollPhysics(),
                                    itemBuilder: (context, index) {
                                      final _coordinates =
                                          dropoffCoordinates[index];
                                      return Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.all(5),
                                            child: Icon(
                                                FontAwesomeIcons.mapMarkerAlt,
                                                color: Colors.red),
                                          ),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          Container(
                                            width: 200,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  'dropoff'.tr(),
                                                  style: Theme.of(context)
                                                      .accentTextTheme
                                                      .bodyText2,
                                                ),
                                                Text(
                                                  _coordinates.formattedAddress,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 5,
                                                  style: Theme.of(context)
                                                      .accentTextTheme
                                                      .headline2,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      );
                                    },
                                  )),
                              // Estimated Price
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(5),
                                      child: Icon(FontAwesomeIcons.moneyBill,
                                          color: Colors.green),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Container(
                                      width: 200,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'estimated_price'.tr(),
                                            style: Theme.of(context)
                                                .accentTextTheme
                                                .bodyText2,
                                          ),
                                          Text(
                                            'Rs'.tr(args: [
                                              (widget.job.priceInfo
                                                              .estimatedAmount ==
                                                          null
                                                      ? 0.0
                                                      : (0.85 *
                                                          widget.job.priceInfo
                                                              .estimatedAmount))
                                                  .toInt()
                                                  .toString()
                                            ]),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 5,
                                            style: Theme.of(context)
                                                .accentTextTheme
                                                .headline2,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 25.0),
                Expanded(
                  flex: 2,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                            margin: EdgeInsets.all(10),
                            child: FlatButton(
                              color: Colors.green,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                                clearJobsData();

                                currentDriver.driverType == DriverType.dynamic
                                    ? jobCubit.applyForJob(widget.job, context)
                                    : jobCubit.exitBase(widget.job);
                              },
                              child: Container(
                                padding: EdgeInsets.all(15),
                                child: Center(
                                  child: Icon(
                                    Icons.check,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        currentDriver.driverType == DriverType.dynamic
                            ? Expanded(
                                child: Container(
                                  margin: EdgeInsets.all(5),
                                  child: FlatButton(
                                    color: Colors.red,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    onPressed: () async {
                                      jobCubit.cancelJob(widget.job.jobId,
                                          currentDriver.driverId);
                                      jobNotifTime = null;
                                      dashDialog = false;
                                      Navigator.of(context).pop();
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(15),
                                      child: Center(
                                        child: Icon(
                                          Icons.close,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          // Positioned(
          //   left: Consts.padding,
          //   right: Consts.padding,
          //   child: CircleAvatar(
          //     backgroundColor: Colors.blueGrey[100],
          //     radius: Consts.avatarRadius,
          //     child: Center(
          //       child: Icon(
          //         FontAwesomeIcons.userAlt,
          //         color: Colors.grey,
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
