import 'dart:convert';
import 'package:bridgelinx_driver/DTO/driver.dart';
import 'package:bridgelinx_driver/DTO/job.dart';
import 'package:bridgelinx_driver/utils/globals.dart';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences prefs;

String distances = "distances";
String gatePass = "gatepass";
String locale = "locale";
String lastJob = "lastjob";
String currentJobKey = "currentJob";
String currentDriverKey = "currentDriver";

// Distances
Future<bool> saveDistances(List<Coordinates> list) async {
  List<String> listStr = list.map((e) => json.encode(e.toJson())).toList();
  return await prefs.setStringList(distances, listStr);
}

Future<bool> addDistances(Coordinates distance) async {
  List<Coordinates> list = List.from([]);

  if (prefs.containsKey(distances)) {
    list = await getDistances();
  }

  list.add(distance);
  return await saveDistances(list);
}

Future<List<Coordinates>> getDistances() async {
  List<String> listStr = prefs.getStringList(distances);
  return listStr != null
      ? listStr.map((e) => Coordinates.fromJson((json.decode(e)))).toList()
      : null;
}

Future<bool> clearDistances() async {
  return prefs.remove(distances);
}

// Gatepass
Future<bool> saveGatepass(String url) async {
  return await prefs.setString(gatePass, url);
}

Future<String> getGatepass() async {
  return prefs.getString(gatePass);
}

// Last Job
Future<Job> getLastJob() async {
  if (!prefs.containsKey(lastJob))
    return null;
  else
    return Job.fromJsonJourney(json.decode(prefs.getString(lastJob)));
}

Future<bool> saveLastJob(Job job) async {
  return await prefs.setString(lastJob, json.encode(job.toJson()));
}

// Last Job
Job getCurrentJobSP() {
  if (!prefs.containsKey(currentJobKey))
    return null;
  else
    return Job.fromJsonJourney(json.decode(prefs.getString(currentJobKey)));
}

Future<bool> saveCurrentJob(Job job) async {
  currentJob = job;
  if (job == null) {
    prefs.remove(currentJobKey);
    return true;
  } else
    return await prefs.setString(currentJobKey, json.encode(job.toJson()));
}

bool isCurrentJob() {
  return prefs.containsKey(currentJobKey);
}

Driver getCurrentDriverSP() {
  if (!prefs.containsKey(currentDriverKey))
    return null;
  else
    return Driver.fromJson(json.decode(prefs.getString(currentDriverKey)));
}

Future<bool> saveCurrentDriver(Driver driver) async {
  currentDriver = driver;
  if (driver == null) {
    prefs.remove(currentDriverKey);
    return true;
  } else
    return await prefs.setString(
        currentDriverKey, json.encode(driver.toJsonSP()));
}

bool isCurrentDriver() {
  return prefs.containsKey(currentDriverKey);
}

//Locale
Future<bool> saveLocale(String _locale) async {
  return await prefs.setString(locale, _locale);
}

String getLocale() {
  if (!prefs.containsKey(locale))
    return 'en';
  else
    return prefs.getString(locale);
}

// Clear
Future<bool> clearJobsData() async {
  return prefs.remove(distances).then((value) {
    return prefs.remove(gatePass).then((value) {
      return prefs.remove(lastJob);
    });
  });
}

Future<bool> clearAllPrefs() async {
  return prefs.clear();
}
